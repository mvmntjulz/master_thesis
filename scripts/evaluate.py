import os, sys
from os import path as osp
import json
import numpy as np
from PIL import Image
import argparse

import torch.nn as nn
import torch.nn.functional as F
import torch
from torchvision import transforms as transforms
from torchvision import models as models 
from sklearn.neighbors import NearestNeighbors
from tqdm import tqdm

dir_main = osp.abspath(osp.join('..'))
assert(osp.exists(dir_main))
sys.path.append(osp.join(dir_main, 'models', 'vgg16'))
sys.path.append(osp.join(dir_main, 'models', 'res'))
sys.path.append(osp.join(dir_main, 'util'))

from vgg16 import VGG16_Block4, VGG16_Dilated, VGG16_Conv4_MultiScale5, VGG16_Conv4_Relu_MultiScale5, VGG16_Conv4_S16_Dilated
from res import Model as ResNet

# HARD-CODED SETTINGS (CHANGE THESE, IF ENV CHANGES):
#==================================================================================
json_path = osp.join(dir_main, "datasets", "brueghel", "brueghel_one_each.json")
dir_dataset = osp.join(dir_main, "datasets", "brueghel")
dir_images = "BrueghelVal"
global_query_scale = 36
res_pre_weights = osp.join(dir_main, "weights", "res", "resnet18.pth")
res_art_weights = osp.join(dir_main, "weights", "res", "brueghelModel.pth")

#==================================================================================

## Arguments Parser
parser = argparse.ArgumentParser(description="Evaluation for feature retrieval tasks")
parser.add_argument('--net', type=str, default="vgg_py", metavar='X', help='which model to evaluate')
parser.add_argument('--exp_name', type=str, default='test', metavar='x', help='name of experiment')
parser.add_argument('--epoch', type=int, default=1, metavar='x', help='epoch of trained model')
parser.add_argument('--stride', type=int, default=16, metavar='X', help='network stride')

parser.add_argument('--d_id', type=int, default=-1, metavar='x', help='set gpu device id')



# for trained (by our method) models
class CorrNet(nn.Module):
    def __init__(self):
        super(CorrNet, self).__init__()

        weight_name = "weights_" + "_".join(args.net.split("_")[:-1]) + "_epoch" + str(args.epoch) + ".pt"
        weights_dir = osp.join(dir_main, "train", args.exp_name, weight_name)
        
        if args.net == "res_fine":
            self.feat = ResNet(args.d_id, weights_dir, "resnet18")

        if args.net == "vgg_py_fine":
            self.feat = models.vgg16(pretrained=False) 
            self.feat = nn.Sequential(*list(self.feat.features.children())[:-7])
            weights = torch.load(weights_dir, map_location="cuda:0")
            self.load_state_dict(weights)
        

def forward(model, img):
    if args.net == "vgg_py_fine":
        feat = model.feat(img)
    if args.net == "vgg_py": 
        feat = model(img)
    if args.net == "res_fine":
        feat = model.feat.forward(img)
    if args.net == "res_pre" or args.net == "res_art":
        feat = model.forward(img)
    feat = F.normalize(feat, p=2, dim=1)

    return feat


def load_gt():
    with open(json_path) as json_file:
        data = json.load(json_file)
    return data


def resize_image(featMax, w, h):
    ratio = w/h 
    if ratio < 1:
        featW = featMax
        featH = max(round(ratio * featW),13)
    else:
        featH = featMax
        featW = max(round(featH/ratio), 13)

    return featW, featH


def transform(img):
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) 
    compose_list = [transforms.ToTensor(), normalize]
    trans = transforms.Compose(compose_list)
    return trans(img)


def img2feat(img, model, return_featH=False):
    featW, featH = resize_image(global_query_scale, img.size[0], img.size[1])
    img = img.resize((featH * args.stride, featW * args.stride))
    img = transform(img).unsqueeze(0)
    if args.d_id != -1:
        img = img.cuda()
    feat = forward(model, img) 
    
    if return_featH:
        return feat, featH
    else:
        return feat


def retrieval(model, query_feats, data):
    nbr_neighbors = 10 # TODO: How many?
    image_list = sorted(os.listdir(osp.join(dir_dataset, dir_images)))
    posW = np.ndarray(shape=(len(image_list), query_feats.shape[0], nbr_neighbors), dtype=np.int32)
    posH = np.ndarray(shape=(len(image_list), query_feats.shape[0], nbr_neighbors), dtype=np.int32)
    scores = np.ndarray(shape=(len(image_list), query_feats.shape[0], nbr_neighbors), dtype=np.float32)
    
    print("Retrieval...")
    for i, image_name in tqdm(enumerate(image_list)):
        img = Image.open(osp.join(dir_dataset, dir_images, image_name)) 
        img = img.convert("RGB")
        feat, featH = img2feat(img, model, return_featH=True)

        dists, indices = nn_search(feat, query_feats, nbr_neighbors)
        posW[i] = indices // featH
        posH[i] = indices % featH
        scores[i] = dists
    
    return posW, posH, scores #TODO: CONTINUE HERE (note: biggest per image != biggest overall)


def nn_search(feat, query_feats, nbr_neighbors):
    feat_flat = np.transpose(np.squeeze(feat.data.cpu().numpy()), [1, 2, 0])
    feat_flat = np.reshape(feat_flat, (feat_flat.shape[0] * feat_flat.shape[1], -1))

    nbrs = NearestNeighbors(n_neighbors=1, algorithm="auto", metric="l2", n_jobs=-1).fit(feat_flat)
    indices, dists = nbrs.kneighbors(query_feats, n_neighbors=nbr_neighbors)

    return indices, dists 


def bbx2center(bbx):
    dx = bbx[2] - bbx[0]
    dy = bbx[3] - bbx[1]
    return bbx[0] + dx // 2, bbx[1] + dy // 2


def create_query_feats(model, data):
    queries = np.empty(shape=(0, feat_dim), dtype=np.float64)
    # TODO: maybe save bbx size, so errors can depend on them?

    for key, cands in data.items(): #key: class
        for ii, cand in enumerate(cands): #cand: query images + gt for given class
            query = cand["query"]

            img = Image.open(osp.join(dir_dataset, dir_images, query[0])) 
            img = img.convert("RGB")

            # TODO: resizing/scaling ? 

            featW, featH = resize_image(global_query_scale, img.size[0], img.size[1])
            bW, bH = bbx2center(query[1])
            ratioW, ratioH = bW/img.size[0], bH/img.size[1]
            featW_pos, featH_pos = round(featW * ratioH), round(featH * ratioW)

            feat = img2feat(img, model)
            feat_pos = feat[:, :, featW_pos, featH_pos]
            queries = np.append(queries, feat_pos.data.cpu().numpy(), axis=0)
    
    return queries


def map():
    #perform mAP on eval results
    pass


def load_model():
    if args.net == "vgg_py_fine" or args.net == "res_fine":
        model = CorrNet()
    if args.net == "vgg_py": 
        model = models.vgg16(pretrained=True) 
        model = nn.Sequential(*list(model.features.children())[:-7])
    if args.net == "res_pre": 
        model = ResNet(args.d_id, res_pre_weights, "resnet18")
    if args.net == "res_art":
        model = ResNet(args.d_id, res_art_weights, "resnet18")

    return model.cuda() if args.d_id != -1 else model


if __name__ == '__main__':
    args = parser.parse_args()
    if args.d_id != -1:
        if torch.cuda.is_available():
            print("GPU: {}".format(args.d_id))
            torch.cuda.set_device(args.d_id)

    feature_dim_256 = ["res_art", "res_pre", "res_fine"]
    feature_dim_512 = ["vgg_py", "vgg_py_fine"]
    if args.net in feature_dim_256: feat_dim = 256
    if args.net in feature_dim_512: feat_dim = 512 

    data = load_gt()
    model = load_model() 
    query_feats = create_query_feats(model, data)
    retrieval(model, query_feats, data)
