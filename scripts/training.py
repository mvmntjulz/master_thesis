from __future__ import division

# add dir to PYTHONPATH
import os, sys
import os.path as osp
dir_main = osp.abspath(osp.join('..'))
assert(osp.exists(dir_main))
sys.path.append(osp.join(dir_main, 'models', 'googlenet'))
sys.path.append(osp.join(dir_main, 'models', 'vgg16'))
sys.path.append(osp.join(dir_main, 'models', 'res'))
sys.path.append(osp.join(dir_main, 'util'))

import numpy as np
import math
import argparse
import time
import json
from cv2 import resize, imread, getRotationMatrix2D, warpAffine
import torch.nn as nn
import torch.nn.functional as F
import torch
from torch.utils.data import DataLoader
from datasets import Brueghel

from vgg16 import VGG16_Block4, VGG16_Dilated, VGG16_Conv4_MultiScale5, VGG16_Conv4_Relu_MultiScale5, VGG16_Conv4_S16_Dilated
from res import Model as ResNet
from dirs_util import create_dir, check_dir
from pdist import pdist
from argsort import argsort as my_argsort

import GPUtil as gpu 
from inspect import currentframe, getframeinfo
from torchvision import transforms as transforms
from torchvision import models as models 
from PIL import Image, ImageDraw
from sklearn.neighbors import NearestNeighbors
import csv

#RAdam import
from radam import RAdam

## Arguments Parser
parser = argparse.ArgumentParser(description='train models for matching tasks with GT correspondences')
parser.add_argument('--net', type=str, default="vgg_py", metavar='X', help='which model to train')
parser.add_argument('--pre', type=str, default="imagenet", metavar='X', help='specifies the pretrained type')
parser.add_argument('--set', type=str, default="brueghel", metavar='X', help='dataset to train on')
parser.add_argument('--corr_dir', type=str, default="corrs_train", metavar='X', help='name of corrs dir in dataset dir (or: which corrs to use)')
parser.add_argument('--scaling', dest='scaling', help='do gt correspondences contain scaling info and should they be used', action='store_true')
parser.add_argument('--nn', type=int, default=20, metavar='x', help='number of total negatives per positive corr')
parser.add_argument('--hnn', type=int, default=0, metavar='x', help='number of hard negatives (hnn <= nn)')
parser.add_argument('--hnr', type=int, default=4, metavar='x', help='hard negative radius in feature dimension')
parser.add_argument('--hn_dir', default="l2r", choices=["l2r", "r2l", "both"], help='hard negative direction')
parser.add_argument('--hn_scale', dest='hn_scale', help='search hard negatives over all scales and mean', action='store_true')
parser.add_argument('--hn_scale_gl', dest='hn_scale_gl', help='same as hn_scale, but allowed hn from every scale', action='store_true')
parser.add_argument('--pos_scale', dest='pos_scale', help='scale positive corrs according to best hn_scale', action='store_true')
parser.add_argument('--pos_shift', dest='pos_shift', help='shift positive corrs along scale list', action='store_true')
parser.add_argument('--bs', type=int, default=1, metavar='x', help='batch size')
parser.add_argument('--loss', default="contrastive", choices=["contrastive", "triplet", "ntXent"], help='which loss function to utilize')
parser.add_argument('--loss_th', type=float, default=-1.0, metavar='x', help='threshold for loss-function')
parser.add_argument('--dist', default="l2", choices=["l2", "cos"], help='which distance function to utilize')
parser.add_argument('--lr', type=float, default=1e-4, metavar='x', help='learning rate')
parser.add_argument('--opt', default="adam", choices=["adam", "radam"], help='which optimizer to use')
parser.add_argument('--beta', type=float, default=0.9, metavar='x', help='first value for the betas of the Adam optimizer')
parser.add_argument('--shuffle', dest='shuffle', help='whether to shuffle the image', action='store_true')
parser.add_argument('--mirror', dest='mirror', help='whether to mirror all image pairs', action='store_true')
parser.add_argument('--hue', type=float, default=0.0, metavar='x', help='if and how many hue jittering should be applied')
parser.add_argument('--hue_chance', type=float, default=0.0, metavar='x', help='probability of hue jittering for each image')
parser.add_argument('--gray_chance', type=float, default=0.0, metavar='x', help='probability of gray-scale augmentation for each image')
parser.add_argument('--rota', type=int, default=0, metavar='x', help='maximum angle (degree) to randomly rotate images')
parser.add_argument('--rota_chance', type=float, default=0.0, metavar='x', help='chance for image pair to be rotated')
parser.add_argument('--crop', type=int, default=0, metavar='x', help="Maxmimum pixel difference between original and cropped images per dimension")
parser.add_argument('--crop_chance', type=float, default=0.0, metavar='x', help="chance for image pair to be cropped")
parser.add_argument('--stride', type=int, default=16, metavar='X', help='network stride')
parser.add_argument('--head', type=int, default=0, metavar='x', help='projection dimension of the last head layer')
parser.add_argument('--img_dim', type=int, default=224, metavar='X', help='img size when scaling is off')
parser.add_argument('--dil', type=int, default=1, metavar='X', help='dilation level for vgg_dil')
parser.add_argument('--epoch', type=int, default=1, metavar='X', help='train X epochs')
parser.add_argument('--save', type=int, default=1, metavar='X', help='save the model every X epochs ')
parser.add_argument('--d_id', type=int, default=-1, metavar='x', help='set gpu device id')
parser.add_argument('--exp_name', type=str, default='test', metavar='x', help='name of experiment')
args = parser.parse_args()
print(args)

cfg = dict()

# model:
if "vgg" in args.net:
    cfg['init_model'] = 'vgg16'
elif "res" in args.net:
    cfg['init_model'] = 'res'
else:
    cfg["init_model"] = "googlenet"

# init weights:
if args.net == "vgg_ms5":
    cfg['init_weights'] = 'vgg16_caffe_conv4_relu_ms5.pt'
elif args.net == "res":
    cfg['init_weights'] = 'resnet18.pth' 
else:
    cfg['init_weights'] = 'vgg16_caffe_conv4.pt'

# normalize
if args.pre == "imagenet":
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) 
if args.pre == "caffe":
    normalize = transforms.Normalize(mean=[123., 117., 104.], std=[1., 1., 1.]) 

# other usefule globals
scales = [80, 74, 68, 60, 49, 36, 20]
#scales = [68, 60, 49]
l2Dist = nn.PairwiseDistance(p=2)
cfg['loss_margin'] = 1  # loss margin for negative sample
cfg['rnd_seed'] = 7
torch.manual_seed(cfg['rnd_seed'])
global zero_counter; zero_counter = 0
global aug_counter; aug_counter = 0
global hn_list; hn_list = list() 
global DEBUG; DEBUG = False

## file configuration
cfg['dir_main'] = check_dir(dir_main)
cfg['dir_dataset'] = check_dir(osp.join(cfg["dir_main"], "datasets", args.set))
cfg['dir_init_weights'] = check_dir(osp.join(cfg['dir_main'], 'weights', cfg['init_model'], cfg['init_weights']))
cfg['dir_output'] = create_dir(osp.join(cfg["dir_main"], "train"))  # path output is saved


def gpu_load(text=""):
    if DEBUG:
        cf = currentframe()
        print(f"\n\nLoC: {cf.f_back.f_lineno} ({text})")
        gpu.showUtilization()


def transform(img, size=None):
    compose_list = [transforms.ToTensor(), normalize]
    if args.hue and np.random.rand() < args.hue_chance:
        compose_list.insert(0, transforms.ColorJitter(hue=args.hue))
    if np.random.rand() < args.gray_chance:
        compose_list.insert(0, transforms.Grayscale(num_output_channels=3))
    trans = transforms.Compose(compose_list)
    return trans(img)


class CorrNet(nn.Module):
    def __init__(self, cfg):
        super(CorrNet, self).__init__()
        self.cfg = cfg
        if args.net == "res":
            self.feat = ResNet(args.d_id, cfg["dir_init_weights"], "resnet18")
            in_channels = 256
        if args.net == "vgg_py":
            self.feat = models.vgg16(pretrained=True) 
            self.feat = nn.Sequential(*list(self.feat.features.children())[:-7])
            in_channels = 512 
        if args.head:
            self.head_conv1 = nn.Conv2d(in_channels, args.head, kernel_size=1)
            self.head_relu1 = nn.ReLU()
            self.head_conv2 = nn.Conv2d(args.head, args.head, kernel_size=1)
            self.head_relu2 = nn.ReLU()

    def forward(self, img):
        if args.net == "res":
            feat = self.feat.forward(img)
        if args.net == "vgg_py":
            feat = self.feat(img)
        if args.head:
            feat = self.head_conv1(feat)
            feat = self.head_relu1(feat)
            feat = self.head_conv2(feat)
            feat = self.head_relu2(feat)

        feat = F.normalize(feat, p=2, dim=1) 

        return feat

    def get_dist(self, feat1, feat2, corrs):
        feat1_pos = torch.t(feat1[0][:, corrs[:, 0], corrs[:, 1]])
        feat2_pos = torch.t(feat2[0][:, corrs[:, 2], corrs[:, 3]])
        dist_list = list()
        dist_list.append(dist(feat1_pos, feat2_pos))
        return dist_list


def combine_hn(nbr_corr_pos, hn_l2r, hn_r2l):
    hn_comb = list()
    for i in range(nbr_corr_pos):
        hn_comb.append(torch.cat((hn_l2r[i][:args.hnn//2], hn_r2l[i][:args.hnn//2]), 0)) 

    return hn_comb


def replace_with_hn(corr_pos, corr_neg, feat1, feat2):
    if args.hn_dir == "both":
        hn_l2r = generate_hard_neg(corr_pos, feat1, feat2, direction="l2r")
        hn_r2l = generate_hard_neg(corr_pos[:,[2,3,0,1]], feat2, feat1, direction="r2l")
        hn = combine_hn(corr_pos.shape[0], hn_l2r, hn_r2l)
    if args.hn_dir == "l2r":
        hn = generate_hard_neg(corr_pos, feat1, feat2, direction="l2r")
    if args.hn_dir == "r2l":
        hn = generate_hard_neg(corr_pos[:,[2,3,0,1]], feat2, feat1, direction="r2l")

    if not hn:
        return corr_neg

    corr_neg_np = corr_neg.data.cpu().numpy()
    corr_np = corr_pos.data.cpu().numpy()

    for i in range(corr_pos.shape[0]):
        feat_idx = np.argwhere((corr_neg_np[:,0] == corr_np[i,0]) & (corr_neg_np[:,1] == corr_np[i,1]))[:,0]
        nbr_rn = feat_idx.shape[0]
        nbr_hn = hn[i].shape[0]

        if nbr_rn >= nbr_hn:
            corr_neg[feat_idx[:nbr_hn]] = hn[i]
        else:
            corr_neg[feat_idx] = hn[i][:nbr_rn]

    return corr_neg


def dist(feat1, feat2):
    if args.dist == "l2":
        return l2Dist(feat1, feat2) 
    elif args.dist == "cos":
        return F.cosine_similarity(feat1, feat2)


def contrastive_loss(dist_pos, dist_neg):
    if args.dist == "cos":
        dist_pos = 1.0 - dist_pos
        dist_neg = 1.0 - dist_neg

    loss = torch.sum(dist_pos ** 2) + torch.sum(F.relu(args.loss_th - dist_neg) ** 2)
    return 0.5 * loss / (dist_pos.size(0) + dist_neg.size(0))


def ntXent_loss(dist_pos, dist_neg):
    # here loss_th := tau
    dist_pos = dist_pos.mean()
    loss = torch.log(torch.sum(torch.exp(dist_neg / args.loss_th))) - dist_pos / args.loss_th
    return loss 


def triplet_loss(dist_pos, dist_neg):
    dist_pos = dist_pos.mean()
    dist_neg = dist_neg.mean()
    loss = torch.clamp(dist_neg + args.loss_th - 1, min=0) + torch.clamp(args.loss_th - dist_pos, min=0)
    return loss 


def generate_hard_neg(corr_pos, feat1, feat2, direction):
    feat_h = feat2.shape[3]

    feat1_pos = feat1[0][:, corr_pos[:,0], corr_pos[:,1]]
    feat1_pos = np.transpose(feat1_pos.data.cpu().numpy(), [1,0])

    feat2_flat = np.transpose(np.squeeze(feat2.data.cpu().numpy()), [1, 2, 0])
    feat2_flat = np.reshape(feat2_flat, (feat2_flat.shape[0] * feat2_flat.shape[1], -1))
    nbr_neighbors = min(feat2_flat.shape[0], 10 * args.hnn)

    # cpu numpy knn: 
    nbrs = NearestNeighbors(n_neighbors=1, algorithm="auto", metric="l2", n_jobs=-1).fit(feat2_flat)
    pts_knn = nbrs.kneighbors(feat1_pos, n_neighbors=nbr_neighbors, return_distance=False)
    hn = list()

    for i in range(corr_pos.shape[0]):
        curr_knn = pts_knn[i].reshape((1,nbr_neighbors))
        curr_knn = np.concatenate((curr_knn // feat_h, curr_knn % feat_h), axis=0).T

        curr_knn = torch.from_numpy(curr_knn).float().cuda() if args.d_id != -1 else torch.from_numpy(curr_knn).float()

        feat2_pos = corr_pos[i,2:].repeat(nbr_neighbors, 1).float()
        mask = l2Dist(feat2_pos, curr_knn) >= args.hnr
        if not mask.any():
            continue
        curr_knn = curr_knn[mask][:min(args.hnn, mask.shape[0])] 
        feat1_neg = corr_pos[i,:2].repeat(curr_knn.shape[0], 1).float()
        if direction == "l2r":
            hn.append(torch.cat((feat1_neg, curr_knn), 1).long())
        elif direction == "r2l":
            hn.append(torch.cat((curr_knn, feat1_neg), 1).long())
       
        # count how many hn are directly on a positive corr
        hn_mask = l2Dist(feat2_pos[:args.hnn], curr_knn) <= 1.0
        close_hn = hn_mask.sum().item() 
        global hn_list
        hn_list.append(close_hn)

    return hn


def generate_rnd_neg(corr_pos): 
    corr_neg_res = np.empty(shape=(0,4), dtype=np.float64)

    for corr in corr_pos:
        if args.scaling:
            rnd_negX = np.random.randint(0, (corr[6])-1, size=(args.nn, 1))
            rnd_negY = np.random.randint(0, (corr[7])-1, size=(args.nn, 1))
            rnd_neg = np.hstack((rnd_negX, rnd_negY))
            corr_neg = get_valid_neg(rnd_neg, corr, 4)
        else:
            rnd_neg = np.random.randint(0, args.img_dim-1, size=(corr_pos.shape[0] * args.nn, 2))
            corr_neg = get_valid_neg(rnd_neg, corr_pos, 2)

        corr_neg_res = np.append(corr_neg_res, corr_neg, axis=0)

    return torch.from_numpy(corr_neg_res).to(torch.long)


def get_valid_neg(rnd_neg, corr, ref_idx):
    #TODO: CONTINUE HERE: THIS MIGHT NEED SOME WORK with more than one corr_pos
    pos_tile = np.tile(corr, (args.nn, 1))
    valid_idx = np.linalg.norm(rnd_neg - pos_tile[:, ref_idx:ref_idx+2], axis=1) > args.hnr
    corr_neg = np.concatenate((pos_tile[valid_idx, 0:2], rnd_neg[valid_idx]), axis=1)

    return corr_neg 


def info_all_scales(img1_name, img2_name, corr_pos):
    corr_pos = corr_pos.float()
    orig_scale_img1 = max(corr_pos[0][2], corr_pos[0][3]).item()
    orig_scale_img2 = max(corr_pos[0][6], corr_pos[0][7]).item()
   
    corr_pos_scales = np.ndarray(shape=(corr_pos.shape[0], len(scales), 8), dtype=np.int32)

    for i, scale in enumerate(scales):
        ratio1 = scale / orig_scale_img1
        ratio2 = scale / orig_scale_img2
        
        scaled_corr = torch.cat((corr_pos[:, :4] * ratio1, corr_pos[:,4:] * ratio2), 1)
        corr_pos_scales[:, i] = torch.round(scaled_corr)
    
    return corr_pos_scales


def img_resize(img, corrW, corrH):
    return img.resize((corrH * args.stride, corrW * args.stride))


def img2tensor(img):
    return transform(img).unsqueeze(0) 


def img_load(img_name):
    img = Image.open(osp.join(cfg["dir_dataset"], "images", img_name)) 
    return img.convert("RGB")


def forward_setup(img1_name, img2_name, corr_pos):
    corr_neg = generate_rnd_neg(corr_pos)

    img1 = img_load(img1_name[0])
    img2 = img_load(img2_name[0])
  
    img1 = img_resize(img1, corr_pos[0][2], corr_pos[0][3])
    img2 = img_resize(img2, corr_pos[0][6], corr_pos[0][7])

    if args.rota or args.crop:
        img1, img2, corr_pos, corr_neg = aug_pipe(img1, img2, corr_pos[:, [0,1,4,5]], corr_neg)
    
    corr_pos_scales = None
    if args.hn_scale or args.hn_scale_gl or args.pos_shift:
        corr_pos_scales = info_all_scales(img1_name, img2_name, corr_pos)

    img1 = img2tensor(img1) 
    img2 = img2tensor(img2)
   
    if args.d_id != -1 and corr_pos is not None and corr_neg is not None:
        img1, img2 = img1.cuda(), img2.cuda() 
        corr_pos, corr_neg = corr_pos.cuda(), corr_neg.cuda()
    
    return img1, img2, corr_pos, corr_neg, corr_pos_scales 


def aug_pipe(img1, img2, corr_pos, corr_neg):
    #TODO: aug_crop()...

    if np.random.rand() < args.rota_chance:
        img1, img2, corr_pos, corr_neg = aug_rotate(img1, img2, corr_pos, corr_neg)

    return img1, img2, corr_pos, corr_neg


def aug_crop(img1, img2, corr_pos, corr_neg):
    pass


def aug_rotate(img1, img2, corr_pos, corr_neg):
    corr_pos, corr_neg = corr_pos.float(), corr_neg.float()

    center_img1 = [img1.size[1] / (2 * args.stride), img1.size[0] / (2 * args.stride)]
    center_img2 = [img2.size[1] / (2 * args.stride), img2.size[0] / (2 * args.stride)]
    angle1, angle2 = args.rota * np.random.uniform(-1, 1, 1)[0], args.rota * np.random.uniform(-1, 1,1)[0]
    cos1, cos2 = np.cos(np.deg2rad(angle1)), np.cos(np.deg2rad(angle2))
    sin1, sin2 = np.sin(np.deg2rad(angle1)), np.sin(np.deg2rad(angle2))

    corr_pos = rotate_corr(corr_pos, center_img1, center_img2, cos1, cos2, sin1, sin2, img1, img2)
    corr_neg = rotate_corr(corr_neg, center_img1, center_img2, cos1, cos2, sin1, sin2, img1, img2)

    img1 = transforms.functional.rotate(img1, angle1)
    img2 = transforms.functional.rotate(img2, angle2)

    return img1, img2, corr_pos, corr_neg


def rotate_corr(corr, center1, center2, cos1, cos2, sin1, sin2, img1, img2):
    corr[:,0] = (corr[:,0] - center1[0]) * cos1 - (corr[:,1] - center1[1]) * sin1 + center1[0]
    corr[:,1] = (corr[:,0] - center1[0]) * sin1 + (corr[:,1] - center1[1]) * cos1 + center1[1]
    corr[:,2] = (corr[:,2] - center2[0]) * cos2 - (corr[:,3] - center2[1]) * sin2 + center2[0]
    corr[:,3] = (corr[:,2] - center2[0]) * sin2 + (corr[:,3] - center2[1]) * cos2 + center2[1]

    #check all new corrs if valid:
    if is_rota_valid(img1, corr[:,:2]) and is_rota_valid(img2, corr[:,2:]):
        return corr.round().to(torch.int64)
    else:
        return None


def is_rota_valid(img, corr):
    check_w = corr[:,0] < img.size[1] // args.stride 
    check_h = corr[:,1] < img.size[0] // args.stride
    
    if (corr > 0).all() and check_w.all() and check_h.all():
        return True
    else:
        return False


def step(loss, optimizer):
    if (loss.data.cpu().numpy() > 0).any():
        loss = loss.mean()
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        return loss.data.cpu().item()
    else:
        global zero_counter
        zero_counter += 1
        return None


def get_dist_scale_hn(img1_name, img2_name, corr_pos_scales):
    dist_min_list = list()
    scale_idx_list = list()
    for i in range(corr_pos_scales.shape[0]): # for every corr-pair:
        dist_neg_list = list()
        dist_neg_data = list()
        for j in range(corr_pos_scales[i].shape[0]): # for every scale:
            corr_pos_scale = torch.from_numpy(corr_pos_scales[i,j]).unsqueeze(0).long()
            if args.d_id != -1:
                corr_pos_scale = corr_pos_scale.cuda()

            corr1W = corr_pos_scales[i, j, 2]
            corr1H = corr_pos_scales[i, j, 3]
            corr2W = corr_pos_scales[i, j, 6]
            corr2H = corr_pos_scales[i, j, 7]

            img1, img2 = img_load(img1_name[0]), img_load(img2_name[0])
            img1, img2 = img_resize(img1, corr1W, corr1H), img_resize(img2, corr2W, corr2H)
            img1, img2 = img2tensor(img1), img2tensor(img2)

            if args.d_id != -1:
                img1, img2 = img1.cuda(), img2.cuda()

            feat1, feat2 = model.forward(img1), model.forward(img2)
            if args.hn_dir == "both":
                hn_l2r = generate_hard_neg(corr_pos_scale[:, [0, 1, 4, 5]], feat1, feat2, "l2r")
                hn_r2l = generate_hard_neg(corr_pos_scale[:, [4, 5, 0, 1]], feat2, feat1, "r2l")
                hn = combine_hn(corr_pos_scale.shape[0], hn_l2r, hn_r2l)[0]
            else:
                hn = generate_hard_neg(corr_pos_scale[:, [0, 1, 4, 5]], feat1, feat2, args.hn_dir)[0]

            dist_neg = model.get_dist(feat1, feat2, hn)
            dist_neg_list += dist_neg
            if args.hn_scale_gl:
                dist_neg_data += [dist_neg[i].data.cpu().numpy() for i in range(len(dist_neg))]
    
        if args.hn_scale:
            dist_neg_means = [d.data.cpu().numpy().mean() for d in dist_neg_list]
            idx_min = dist_neg_means.index(min(dist_neg_means))
            dist_min_list.append(dist_neg_list[idx_min])
            scale_idx_list.append(idx_min)

        if args.hn_scale_gl:
            dist_neg_np = np.concatenate(dist_neg_data, axis=0)
            dist_neg_list = torch.cat(dist_neg_list, dim=0)[np.argsort(dist_neg_np)[:args.hnn]]
            dist_min_list.append(dist_neg_list)

    return [torch.cat(dist_min_list, dim=0)], scale_idx_list


def get_dist_scale_pos(img1_name, img2_name, corr_pos_scales, scale_idx_list, model):
    dist_pos_list = list()

    for i in range(corr_pos_scales.shape[0]):
        corr1W = corr_pos_scales[i, scale_idx_list[i], 2]
        corr1H = corr_pos_scales[i, scale_idx_list[i], 3]
        corr2W = corr_pos_scales[i, scale_idx_list[i], 6]
        corr2H = corr_pos_scales[i, scale_idx_list[i], 7]

        img1, img2 = img_load(img1_name[0]), img_load(img2_name[0])
        img1, img2 = img_resize(img1, corr1W, corr1H), img_resize(img2, corr2W, corr2H)
        img1, img2 = img2tensor(img1), img2tensor(img2)

        if args.d_id != -1:
            img1, img2 = img1.cuda(), img2.cuda()

        feat1, feat2 = model.forward(img1), model.forward(img2)
        curr_pos = corr_pos_scales[i, scale_idx_list[i], [0,1,4,5]]
        dist_pos = model.get_dist(feat1, feat2, np.expand_dims(curr_pos, axis=0))
        dist_pos_list.append(dist_pos[0])

    return dist_pos_list


def get_dist_shift_pos(img1_name, img2_name, corr_pos, corr_pos_scales, model):
    dist_pos_list = list() 
    sd = 1 #shifting-distance

    for i in range(corr_pos.shape[0]):
        scale1 = max(corr_pos[i, 2], corr_pos[i,3])
        scale2 = max(corr_pos[i, 6], corr_pos[i,7])
        scale1_idx = (corr_pos[i, 2:4] == scale1).nonzero()[0].item() + 2
        scale2_idx = (corr_pos[i, 6:8] == scale2).nonzero()[0].item() + 6
        
        idx1 = scales.index(scale1)
        idx2 = scales.index(scale2)
        
        b1 = idx1-(sd) if idx1 > sd else 0
        b2 = idx2-(sd) if idx2 > sd else 0
        e1 = idx1+(sd+1) 
        e2 = idx2+(sd+1) 
        scales1 = scales[b1:e1]
        scales2 = scales[b2:e2]

        for pair in zip(scales1, scales2):
            corr1 = corr_pos_scales[i, corr_pos_scales[0,:,scale1_idx] == pair[0]][0,:4]
            corr2 = corr_pos_scales[i, corr_pos_scales[0,:,scale2_idx] == pair[1]][0, 4:]
            corr_cat = np.concatenate((corr1, corr2), axis=0)

            img1, img2 = img_load(img1_name[0]), img_load(img2_name[0])
            img1 = img_resize(img1, corr_cat[2], corr_cat[3])
            img2 = img_resize(img2, corr_cat[6], corr_cat[7])
            img1, img2 = img2tensor(img1), img2tensor(img2)

            if args.d_id != -1:
                img1, img2 = img1.cuda(), img2.cuda()

            feat1, feat2 = model.forward(img1), model.forward(img2)
            dist_pos = model.get_dist(feat1, feat2, np.expand_dims(corr_cat[[0,1,4,5]], axis=0))
            dist_pos_list.append(dist_pos[0])

    return dist_pos_list


def iter_scaling(img1_name, img2_name, corr_pos, optimizer, model):
    #DEBUG ONLY:
    #corr_pos = corr_pos.repeat(2,1)
    img1_tensor, img2_tensor, corr_pos, corr_neg, corr_pos_scales = forward_setup(img1_name, img2_name, corr_pos)
    
    if corr_pos is None or corr_neg is None:
        global aug_counter
        aug_counter += 1
        return None, None
    
    # logical expression for methods, which produce pos/neg corrs 
    prod_pos = args.pos_scale or args.pos_shift
    prod_neg = args.hn_scale or args.hn_scale_gl

    if not prod_pos or not prod_neg:
        feat1, feat2 = model.forward(img1_tensor), model.forward(img2_tensor)
        if args.hnn:
            corr_neg = replace_with_hn(corr_pos[:, [0,1,4,5]], corr_neg, feat1, feat2)
        dist_pos = model.get_dist(feat1, feat2, corr_pos[:, [0,1,4,5]])
        dist_neg = model.get_dist(feat1, feat2, corr_neg)

    if args.hn_scale or args.hn_scale_gl:
        dist_neg, scale_idx_list = get_dist_scale_hn(img1_name, img2_name, corr_pos_scales)
    if args.pos_scale:
        dist_pos = get_dist_scale_pos(img1_name, img2_name, corr_pos_scales, scale_idx_list, model)
    elif args.pos_shift:
        dist_pos = get_dist_shift_pos(img1_name, img2_name, corr_pos, corr_pos_scales, model)

    return dist_pos, dist_neg


def iter_no_scaling(img1, img2, corr_pos, corr_neg, optimizer, model):
    if args.d_id != -1:
        img1, img2 = img1.cuda(), img2.cuda()
        corr_pos = corr_pos.cuda() 
        corr_neg = corr_neg.cuda() 

    loss = calc_loss(img1, img2, corr_pos, corr_neg, optimizer, model)


def do_epoch(epoch, dataloader, optimizer, model):
    loss_iter_list = list() 
    dist_pos_batch = list() 
    dist_neg_batch = list() 

    # Training Iterations
    for ii, pair in enumerate(dataloader):
        img1, img2, corr_pos = pair
        corr_pos = corr_pos[0].to(torch.int64)

        if args.scaling:
            dist_pos, dist_neg = iter_scaling(img1, img2, corr_pos, optimizer, model) 

            if dist_pos is None or dist_neg is None:
                print("dist_pos or dist_neg was None!!")
                continue
            
            loss_data = None
            dist_pos_batch += dist_pos
            dist_neg_batch += dist_neg

            if (ii+1) % args.bs == 0:
                print(f"ii: {ii}")
                dist_pos_batch = torch.cat(dist_pos_batch, dim=0) 
                dist_neg_batch = torch.cat(dist_neg_batch, dim=0) 

                if args.loss == "contrastive":
                    loss = contrastive_loss(dist_pos_batch, dist_neg_batch)  
                elif args.loss == "triplet":
                    loss = triplet_loss(dist_pos_batch, dist_neg_batch)
                elif args.loss == "ntXent":
                    loss = ntXent_loss(dist_pos_batch, dist_neg_batch)

                loss_data = step(loss, optimizer) 
                dist_pos_batch = list() #empty batches
                dist_neg_batch = list()

            if loss_data is not None:
                loss_iter_list.append(loss_data)
        else:
            #TODO: needs some work, ovsly
            iter_no_scaling(img1, img2, corr_pos, corr_neg, optimizer, model)

    return loss_iter_list


def save_epoch(epoch, t0, model, dir_temp, loss_epoch_list, loss_iter_list):
    t1 = time.time() - t0
    print(f"\nEpoch {epoch+1}: Loss {loss_epoch_list[-1]}")
    print(f"Escaped Time {t1 // 3600}, {t1 // 60 % 60}, {t1 % 60} ({zero_counter} times zero) ({aug_counter} times skipped)") 

    with open(osp.join(dir_temp, f"loss_epoch_{epoch+1}.txt"), 'wb') as f:                 
        np.savetxt(f, np.asarray(loss_epoch_list), fmt='%f')             

    with open(osp.join(dir_temp, f"loss_iter_{epoch+1}.txt"), 'wb') as f:                 
        np.savetxt(f, np.asarray(loss_iter_list), fmt='%f')             
    
    with open(osp.join(dir_temp, f"cfg_{args.net}.txt"), 'a', encoding='utf8') as f:
        f.write("\n\n" + f"Zero-counter: {zero_counter}, Aug-counter: {aug_counter}")

    with open(osp.join(dir_temp, f"hn_{epoch+1}.txt"), 'a', encoding='utf8') as f:
        global hn_list
        np.savetxt(f, np.asarray(hn_list), fmt='%f')             

    if (epoch + 1) % args.save == 0:
        print('Saving trained model to temp folder...')
        torch.save(model.state_dict(), osp.join(dir_temp, f"weights_{args.net}_epoch{epoch+1}.pt"))
    

def set_th():
    if args.loss_th == -1: # default, if no threshold is given
        if args.loss == "contrastive":
            args.loss_th = 1.0 
        if args.loss == "triplet":
            args.loss_th = 0.8 
        if args.loss == "ntXent":
            args.loss_th = 1.0


def training_setup():
    dir_temp = create_dir(osp.join(cfg['dir_output'], args.exp_name))

    with open(osp.join(dir_temp, f"cfg_{args.net}.txt"), 'w', encoding='utf8') as f:
        f.write(json.dumps(cfg))   
        f.write("\n\n" + str(vars(args)))

    if args.d_id != -1:
        if torch.cuda.is_available():
            print("GPU: {}".format(args.d_id))
            torch.cuda.set_device(args.d_id)
        else:
            print("No CUDA-Device available")
   
    dataset = Brueghel(cfg["dir_dataset"], args.corr_dir, args.img_dim, pretrained_type=args.pre, stride=args.stride, scaling=args.scaling, mirror=args.mirror, num_neg=args.nn) 
    dataloader = DataLoader(dataset, batch_size=1, shuffle=args.shuffle, num_workers=0)
    model = CorrNet(cfg).cuda() if args.d_id != -1 else CorrNet(cfg)
    model.train()
    if args.opt == "radam":
        optimizer = RAdam(model.parameters(), lr=args.lr, betas=(args.beta, 0.999))
    if args.opt == "adam":
        optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, betas=(args.beta, 0.999))
    optimizer.zero_grad()
    set_th()

    if args.dist == "l2":
        if args.loss == "ntXent" or args.loss == "triplet":
            # TODO: fix this issue, by making triplet and ntXent also work with l2
            print(f"WARNING: {args.loss} expects similarities, not distances!")

    return model, optimizer, dataloader, dir_temp


def check_hnn():
    if args.hnn > args.nn:
        args.nn = args.hnn
        print("set nn to hnn, since it was smaller")


if __name__ == '__main__':
    args = parser.parse_args()
    check_hnn()
    t0 = time.time()
    loss_epoch_list = list()
    model, optimizer, dataloader, dir_temp = training_setup()

    print("Begin Training...")
    for epoch in range(args.epoch):
        loss_iter_list = do_epoch(epoch, dataloader, optimizer, model) 
        loss_epoch_list.append(np.mean(loss_iter_list))
        save_epoch(epoch, t0, model, dir_temp, loss_epoch_list, loss_iter_list)

