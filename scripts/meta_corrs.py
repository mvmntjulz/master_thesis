import csv
import os
from os import path as osp

corr_path = osp.join("corrs")


scales = dict()

for i, corr in enumerate(sorted(os.listdir(corr_path))):
    if i % 1000 == 0 and i > 0: print(i)
    img1_name, img2_name = corr.split("-")[1] + ".jpg", corr.split("-")[2] + ".jpg"
    with open(osp.join(corr_path, corr), "r") as corr_file:
        corrs = [list(map(int, i[0].split(","))) for i in list(csv.reader(corr_file, delimiter= " "))]
        for pair in corrs:
            scale_pair = f"{pair[2]}_{pair[5]}"
            if scale_pair in scales:
                scales[scale_pair] +=1
            else:
                scales[scale_pair] = 1
scales = sorted(scales.items(), key=lambda kv: kv[1]) 
print(scales)
