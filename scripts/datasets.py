import csv
import os
from os import path as osp
import torch
from torch.utils.data import Dataset
from PIL import Image
import numpy as np
from torchvision import transforms as transforms
import pickle


class Brueghel(Dataset):
    def __init__(self, data_dir, corr_dir, img_dim, pretrained_type="imagenet", num_neg=40, stride=4, scaling=False, mirror=True):
        self.image_dir = osp.join(data_dir, "images")
        self.data_dir = data_dir
        self.corr_dir = osp.join(data_dir, corr_dir)
        self.pretrained_type = pretrained_type # caffe or imagenet
        self.pairs = self.create_pairs(mirror)
        self.img_dim = img_dim
        self.num_neg = num_neg
        self.stride = stride
        self.scaling = scaling
        self.feat_min = 13 # taken from Artminer code (margin * 2 + searchRegion + 1) = 13
        if self.pretrained_type == "imagenet":
            self.normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) 
        if self.pretrained_type == "caffe":
            self.normalize = transforms.Normalize(mean=[123., 117., 104.], std=[1., 1., 1.]) 

    def create_pairs(self, mirror):
        pairs = list()
        img_dict = pickle.load(open(osp.join(self.data_dir, "brueghel_int2name.p"), "rb"))
        sorted_img_list = sorted(os.listdir(self.corr_dir), key = lambda x: (int(x.split("-")[0]), int(x.split("-")[1])))
  
        for i, corr in enumerate(sorted_img_list):
            img1_name, img2_name = corr.split("-")[2], corr.split("-")[3][:-4]
            img1_name, img2_name = img_dict[int(img1_name)], img_dict[int(img2_name)]
            with open(osp.join(self.corr_dir, corr), "r") as corr_file:
                corrs = [list(map(int, i[0].split(","))) for i in list(csv.reader(corr_file, delimiter= " "))]
                
                corrs = np.asarray(corrs)
                pairs.append([img1_name, img2_name, corrs, 0]) # last number indicates flipped images

                # mirroring
                if mirror:
                    img1 = Image.open(osp.join(self.image_dir, img1_name))
                    img2 = Image.open(osp.join(self.image_dir, img2_name))
                    corrs_mirror = corrs.copy()
                    corrs_mirror[:, 0] = img1.size[0] - corrs[:, 0]
                    corrs_mirror[:, 3] = img2.size[0] - corrs[:, 3]
                    pairs.append([img1_name, img2_name, corrs_mirror, 1])

        return pairs     

    def __len__(self):
        return len(self.pairs) 

    def read_pair(self, idx):
        img1 = Image.open(osp.join(self.image_dir, self.pairs[idx][0]))
        img2 = Image.open(osp.join(self.image_dir, self.pairs[idx][1]))
        if self.pairs[idx][3]:
            img1, img2 = img1.transpose(Image.FLIP_LEFT_RIGHT), img2.transpose(Image.FLIP_LEFT_RIGHT)
        corr_pos = np.asarray(self.pairs[idx][2])
        if self.scaling:
            return img1, img2, corr_pos
        else:
            img1 = img1.convert("RGB")
            img2 = img2.convert("RGB")
            return img1, img2, corr_pos[:, [0,1,3,4]]

    def get_feat_dims(self, featMax, w, h):
        ratio = w/h 
        if ratio < 1:
            # image is higher than wide
            featW = featMax
            featH = max(round(ratio * featW), self.feat_min)
        else:
            # image is wider than high
            featH = featMax
            featW = max(round(featH/ratio), self.feat_min)
        return featW, featH
   
    # adds missing feature dim of both images to corrs
    def expand_corrs(self, img1, img2, corr_pos):
        [w1, h1], [w2, h2] = img1.size[:2], img2.size[:2]
        corr_pos = np.asarray(corr_pos, dtype=np.float64)
        expand_corrs = list()
        for corr in corr_pos:
            feat_w1, feat_h1 = self.get_feat_dims(corr[2], w1, h1)
            feat_w2, feat_h2 = self.get_feat_dims(corr[5], w2, h2)
            expand_corrs.append([corr[0], corr[1], feat_w1, feat_h1, corr[3], corr[4], feat_w2, feat_h2])
        expand_corrs = np.asarray(expand_corrs)
        return img1, img2, expand_corrs

    def resize_pair(self, img1, img2, corr_pos):
        [w1, h1], [w2, h2] = img1.size[:2], img2.size[:2]
        corr_pos = np.asarray(corr_pos, dtype=np.float64)
        if self.scaling:
            # scales: [80, 74, 68, 60, 49, 36, 20]
            scaled_corrs = list() 
            for corr in corr_pos:
                feat_w1, feat_h1 = self.get_feat_dims(corr[2], w1, h1)
                feat_w2, feat_h2 = self.get_feat_dims(corr[5], w2, h2)
                x1 = corr[0] / h1 * feat_w1 * self.stride
                y1 = corr[1] / w1 * feat_h1 * self.stride
                x2 = corr[3] / h2 * feat_w2 * self.stride
                y2 = corr[4] / w2 * feat_h2 * self.stride
                scaled_corrs.append([x1, y1, feat_w1, feat_h1, x2, y2, feat_w2, feat_h2])
            corr_pos = np.asarray(scaled_corrs)
        else:
            img1, img2 = img1.resize((self.img_dim, self.img_dim)), img2.resize((self.img_dim, self.img_dim))
            resize_ratio = np.array([self.img_dim/h1, self.img_dim/w1, self.img_dim/h2, self.img_dim/w2], dtype=float)
            corr_pos[:,0] *= resize_ratio[0]
            corr_pos[:,1] *= resize_ratio[1]
            corr_pos[:,2] *= resize_ratio[2]
            corr_pos[:,3] *= resize_ratio[3]
        return img1, img2, corr_pos
    
    def switch_pair(self, img1, img2, corr_pos):
        if np.random.randint(2):
            return img1, img2, corr_pos
        else:
            if self.scaling:
                i = np.argsort([4, 5, 6, 7, 0, 1, 2, 3])
            else:
                i = np.argsort([2, 3, 0, 1])

            return img2, img1, corr_pos[:, i]

    def transform(self, img):
        trans = transforms.Compose(
                [
                    transforms.ToTensor(),
                    self.normalize
                ])
        return trans(img)
    
    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        img1, img2, corr_pos = self.read_pair(idx) 
        img1, img2, corr_pos = self.expand_corrs(img1, img2, corr_pos)

        if self.scaling:
            # return only the image names for later resizing
            return self.pairs[idx][0], self.pairs[idx][1], corr_pos
        else:
            img1, img2 = self.transform(img1), self.transform(img2)
            return img1, img2, corr_pos
