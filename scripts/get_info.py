import pandas as pd
import os
from os import path as osp
import sys
import argparse
import numpy as np
from config import get_info as c

parser = argparse.ArgumentParser(description='Get information about pck performance')
parser.add_argument('--net', type=str, default='4a', metavar='x', help='which model should be used')
parser.add_argument('--exp', type=str, default='default', metavar='x', help='name of experiment')
parser.add_argument('--bbx', action="store_true", help='get results from bbx retrieval')
parser.add_argument('--t', type=int, default=45, metavar='x', help='threshold for new retrievals')
parser.add_argument('--x', dest="x", action="store_true", help='use expanded queries')
parser.add_argument('--per_class', dest="per_class", action="store_true", help='print per class info')
args = parser.parse_args()

c["dir_main"] = osp.join("..")

def get_info():
    if args.x:
        print("Using expanded Queries.")
        info_file = "info_x_" + args.t + ".csv"
    else:
        print("Using flipped Queries.")
        info_file = "info_" + str(args.t) + ".csv"
    info = pd.read_csv(osp.join(c["dir_main"], "exps", args.exp, args.net, info_file))
    print(args.exp, args.net)
    print("prec1:", "{:.2}".format(info["prec1"].mean()))
    print("prec5:", "{:.2}".format(info["prec5"].mean()))
    print("prec10:", "{:.2}".format(info["prec10"].mean()))
    print("prec20:", "{:.2}".format(info["prec20"].mean()))
    print("prec50:", "{:.2}".format(info["prec50"].mean()))
    print("prec100:", "{:.2}".format(info["prec100"].mean()))
    print("discards:", "{:.3}".format(info["nbr_discard"].mean()))

def get_info_grouped():
    info_file = "info_" + str(args.t) + ".csv"
    info = pd.read_csv(osp.join(c["dir_main"], "exps", args.exp, args.net, info_file))
    print(args.exp, args.net)
    info = info.groupby(["object"])

    for p in ["prec1", "prec5", "prec10", "prec20", "prec50", "prec100", "nbr_discard"]:
        print("\n")
        print(info[p].mean())

def get_mAP():
    df_query = pd.read_csv(c["df_query"], index_col=0, dtype={"object":str})
    nbr_q_per_class = df_query.groupby("object").count()
    info_file = f"info_{args.t}.txt" if not args.bbx else "info_bbx.txt"

    matrix = list()
    with open(osp.join(c["dir_main"], "exps", args.exp, args.net, info_file)) as f:
        for line in f:
            s_line = line.strip().replace(" ", "")[1:] # fist one is always the query itself
            matrix.append([int(x) for x in s_line])

    matrix = np.asarray(matrix)
    prec_matrix = list()

    for query in matrix:
        prec_query = list()
        for pos in range(len(query)):
            prec_at = sum(query[0:pos+1]) if query[pos] != 0 else 0 
            prec_query.append(prec_at/(pos + 1))
        prec_matrix.append(prec_query)

    aP = list()
    for i, line in enumerate(prec_matrix):
        q_class = df_query.iloc[i].object
        nbr_gt = nbr_q_per_class.loc[nbr_q_per_class.index == q_class].values[0][0]

        #These lines divide by the number of correct matches
        #nbr_correct = sum(matrix[i,:nbr_gt])
        #aP.append(sum(line[:nbr_gt])/nbr_correct) if nbr_correct > 0 else aP.append(0.0)

        #This line divides by the number of gt objects
        aP.append(sum(line[:nbr_gt])/nbr_gt)

    print("mAP:", sum(aP) / len(aP))

if args.bbx:
    get_mAP()
elif not args.per_class:
    get_info()
    get_mAP()
else:
    get_info_grouped()
