import matplotlib.pyplot as plt
import os
from os import path as osp
import argparse
import numpy as np


parser = argparse.ArgumentParser()
parser.add_argument("--epoch", type=int, help="number of epochs to plot")
parser.add_argument("--exp", type=str, help="experiement name of the training")
parser.add_argument('--type', default="loss", choices=["loss", "hn"], help='visualize loss or hn-count')
parser.add_argument('--step_size', type=int, help='mean over step_size iterations')

args = parser.parse_args()

dir_main = osp.join("..") 
train_dir = osp.join(dir_main, "train", args.exp) 
y_np = np.empty(shape=(0,0), dtype=np.float32)

for i in range(args.epoch):
    if args.type == "loss":
        save_file = osp.join(train_dir, f"loss_iter_{i+1}.txt")
    else:
        save_file = osp.join(train_dir, f"hn_{i+1}.txt")
    with open(osp.join(save_file)) as f:
        lines = f.readlines()
        y = [float(line.rstrip()) for line in lines]
        y = np.asarray(y)
        y_np = np.append(y_np, y)

cut = (y_np.shape[0] % args.step_size)
if cut:
    y_np = y_np[:-cut]
nbr_iter = y_np.shape[0]
x = np.arange(1, y_np.shape[0] + 1, args.step_size)
y_np = np.mean(y_np.reshape(-1, args.step_size), axis=1)
plt.plot(x, y_np)
plt.xlabel(f"iteration ({nbr_iter // args.epoch} per epoch)")
if args.type == "loss":
    plt.ylabel("loss")
else:
    plt.ylabel(f"hn-avg per {args.step_size}")
plt.show()
