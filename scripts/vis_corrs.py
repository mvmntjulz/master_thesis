import numpy as np
import os
import csv
import pickle
from os import path as osp
from PIL import Image, ImageDraw


stride = 16
feat_min = 13
save_every = 200
data_dir = "../../thesis/datasets/brueghel"
corr_dir = "../../cvi/ArtMiner/feature_learning/corrs_prepared/brueghel_train"
image_dir = "../../cvi/ArtMiner/data/Brueghel"
save_dir = "../vis_corrs"


def get_feat_dims(featMax, w, h):
    ratio = w/h 
    if ratio < 1:
        # image is higher than wide
        featW = featMax
        featH = max(round(ratio * featW), feat_min)
    else:
        # image is wider than high
        featH = featMax
        featW = max(round(featH/ratio), feat_min)
    return featW, featH


def vis_corrs(i, j, img1, img2, img1_name, img2_name, corrs):
    im1 = np.array(img1)
    im2 = np.array(img2)

    if im1.shape[0] != im2.shape[0]:
        if im1.shape[0] > im2.shape[0]:
            shape = (im1.shape[0] - im2.shape[0], im2.shape[1], 3)
            add_rows = np.zeros(shape, dtype=np.int32)
            im2 = np.vstack((im2, add_rows))
        else:
            shape = (im2.shape[0] - im1.shape[0], im1.shape[1], 3)
            add_rows = np.zeros(shape, dtype=np.int32)
            im1 = np.vstack((im1, add_rows))
    add_x = im1.shape[1]
    imC = np.hstack((im1, im2))
    imC = Image.fromarray(imC.astype(np.uint8)) 
    draw = ImageDraw.Draw(imC)

    for c in corrs:
        x1 = c[1] * stride
        y1 = c[0] * stride
        x2 = c[5] * stride + add_x
        y2 = c[4] * stride
        draw.line((x1, y1, x2, y2), fill=128, width=3)
        imC.save(osp.join(save_dir, f"{i}_{j}_{img1_name[:10]}_{img2_name[:10]}.jpg"))


img_dict = pickle.load(open(osp.join(data_dir, "brueghel_int2name.p"), "rb"))
sorted_corrs = sorted(os.listdir(corr_dir), key = lambda x: (int(x.split("-")[0]), int(x.split("-")[1])))
for file_name in sorted_corrs:
    i, j = file_name.split("-")[0], file_name.split("-")[1]
    if (int(i) * 200 + int(j)) % save_every != 0: 
        continue
    print(i, j)
    img1_name, img2_name = file_name.split("-")[2], file_name.split("-")[3][:-4]
    img1_name, img2_name = img_dict[int(img1_name)], img_dict[int(img2_name)]

    with open(osp.join(corr_dir, file_name), "r") as corr_file:
        corrs = [list(map(int, i[0].split(","))) for i in list(csv.reader(corr_file, delimiter= " "))]
        corrs = np.asarray(corrs, dtype=np.float64)

        img1 = Image.open(osp.join(image_dir, img1_name))
        img2 = Image.open(osp.join(image_dir, img2_name))
        
        img1 = img1.convert("RGB")
        img2 = img2.convert("RGB")

        [w1, h1], [w2, h2] = img1.size[:2], img2.size[:2]
        expand_corrs = list() 
        for corr in corrs:
            feat_w1, feat_h1 = get_feat_dims(corr[2], w1, h1)
            feat_w2, feat_h2 = get_feat_dims(corr[5], w1, h1)
            expand_corrs.append([corr[0], corr[1], feat_w1, feat_h1, corr[3], corr[4], feat_w2, feat_h2])
        img1 = img1.resize((int(feat_h1 * stride), int(feat_w1 * stride)))  
        img2 = img2.resize((int(feat_h2 * stride), int(feat_w2 * stride)))  
        vis_corrs(i, j, img1, img2, img1_name, img2_name, expand_corrs)

