from __future__ import division

# add dir to PYTHONPATH
import os, sys
import os.path as osp
dir_main = osp.abspath(osp.join('..'))
assert(osp.exists(dir_main))
sys.path.append(osp.join(dir_main, 'models', 'googlenet'))
sys.path.append(osp.join(dir_main, 'models', 'vgg16'))
sys.path.append(osp.join(dir_main, 'models', 'res'))
sys.path.append(osp.join(dir_main, 'util'))

import numpy as np
import argparse
import time
import json
from cv2 import resize, imread, getRotationMatrix2D, warpAffine
import torch.nn as nn
import torch.nn.functional as F
import torch
from torch.utils.data import DataLoader
from datasets import Brueghel

from vgg16 import VGG16_Block4, VGG16_Dilated, VGG16_Conv4_MultiScale5, VGG16_Conv4_Relu_MultiScale5, VGG16_Conv4_S16_Dilated
from res import Model as ResNet
from dirs_util import create_dir, check_dir
from pdist import pdist
from argsort import argsort as my_argsort

import GPUtil as gpu 
from inspect import currentframe, getframeinfo
from torchvision import transforms as transforms
from torchvision import models as models 
from PIL import Image, ImageDraw
from sklearn.neighbors import NearestNeighbors
import csv

## Arguments Parser
parser = argparse.ArgumentParser(description='test overfitted model with GT correspondences')
parser.add_argument('--net', type=str, default="vgg_py", metavar='X', help='which model to test')
parser.add_argument('--pre', type=str, default="imagenet", metavar='X', help='specifies the pretrained type')
parser.add_argument('--set', type=str, default="brueghel", metavar='X', help='dataset to test on')
parser.add_argument('--corr_dir', type=str, default="corrs_train", metavar='X', help='name of corrs dir in dataset dir (or: which corrs to use)')
parser.add_argument('--scaling', dest='scaling', help='whether to scaling images and corrs', action='store_true')
parser.add_argument('--stride', type=int, default=16, metavar='X', help='network stride')
parser.add_argument('--img_dim', type=int, default=224, metavar='X', help='img size when scaling is off')
parser.add_argument('--dil', type=int, default=1, metavar='X', help='dilation level for vgg_dil')
parser.add_argument('--d_id', type=int, default=-1, metavar='x', help='set gpu device id')
parser.add_argument('--exp_name', type=str, default='test', metavar='x', help='name of experiment')
parser.add_argument('--epoch', type=int, default=1, metavar='x', help='epoch of trained model')
args = parser.parse_args()
print(args)

cfg = dict()

# model:
if "vgg" in args.net:
    cfg['init_model'] = 'vgg16'
elif "res" in args.net:
    cfg['init_model'] = 'res'
else:
    cfg["init_model"] = "googlenet"

# init weights:
if args.net == "vgg_ms5":
    cfg['init_weights'] = 'vgg16_caffe_conv4_relu_ms5.pt'
elif args.net == "res":
    cfg['init_weights'] = 'resnet18.pth' 
else:
    cfg['init_weights'] = 'vgg16_caffe_conv4.pt'

# normalize
if args.pre == "imagenet":
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) 
if args.pre == "caffe":
    normalize = transforms.Normalize(mean=[123., 117., 104.], std=[1., 1., 1.]) 

l2Dist = nn.PairwiseDistance(p=2)

cfg['loss_margin'] = 1  # loss margin for negative sample
cfg['rnd_seed'] = 7
torch.manual_seed(cfg['rnd_seed'])

## file configuration
cfg['dir_main'] = check_dir(dir_main)
cfg['dir_dataset'] = check_dir(osp.join(cfg["dir_main"], "datasets", args.set))
cfg['dir_init_weights'] = check_dir(osp.join(cfg['dir_main'], 'weights', cfg['init_model'], cfg['init_weights']))
cfg['dir_trained_weights'] = osp.join(cfg["dir_main"], "train")


def transform(img, size=None):
    compose_list = [transforms.ToTensor(), normalize]
    trans = transforms.Compose(compose_list)
    return trans(img)


class CorrNet(nn.Module):
    def __init__(self, cfg):
        super(CorrNet, self).__init__()
        self.cfg = cfg

        #pretrained models:
        if args.net == "res":
            self.feat = ResNet(args.d_id, cfg["dir_init_weights"], "resnet18")
        elif args.net == "vgg_py":
             self.feat = models.vgg16(pretrained=True) 
             self.feat = nn.Sequential(*list(self.feat.features.children())[:-7])

        #train models:
        if args.net == "res_fine":
            self.feat = ResNet(args.d_id, osp.join(cfg["dir_trained_weights"], args.exp_name, f"weights_{args.net}_epoch{args.epoch}.pt"), "resnet18")
        elif args.net == "vgg_py_fine":
            self.feat = models.vgg16(pretrained=False) 
            self.feat = nn.Sequential(*list(self.feat.features.children())[:-7])
            weight_name = "weights_" + "_".join(args.net.split("_")[:-1]) + "_epoch" + str(args.epoch) + ".pt"
            weights_dir = osp.join(dir_main, "train", args.exp_name, weight_name)
            weights = torch.load(weights_dir, map_location="cuda:0")
            self.load_state_dict(weights)
    
    def forward(self, im1, im2, corr_pos):
        if args.net == "res":
            feat1, feat2 = self.feat.forward(im1), self.feat.forward(im2)
        else:
            feat1, feat2 = self.feat(im1), self.feat(im2)

        feat1, feat2 = F.normalize(feat1, p=2, dim=1), F.normalize(feat2, p=2, dim=1) 

        feat1_pos = feat1[0][:, corr_pos[:, 0], corr_pos[:, 1]]

        return feat1_pos, feat2


def forward_setup(img1_name, img2_name, corr_pos):

    img1 = Image.open(osp.join(cfg["dir_dataset"], "images", img1_name[0])) 
    img2 = Image.open(osp.join(cfg["dir_dataset"], "images", img2_name[0]))
   
    img1 = img1.convert("RGB")
    img2 = img2.convert("RGB")
    
    img1 = img1.resize((corr_pos[0][3] * args.stride, corr_pos[0][2] * args.stride))
    img2 = img2.resize((corr_pos[0][7] * args.stride, corr_pos[0][6] * args.stride))

    corr_pos = corr_pos[:, [0,1,4,5]] # don't need the scaling anymore
    
    
    img1 = transform(img1).unsqueeze(0)
    img2 = transform(img2).unsqueeze(0)
   
    if args.d_id != -1:
        img1, img2 = img1.cuda(), img2.cuda() 
        corr_pos = corr_pos.cuda()
    
    return img1, img2, corr_pos


def test_setup():

    if args.d_id != -1:
        if torch.cuda.is_available():
            print("GPU: {}".format(args.d_id))
            torch.cuda.set_device(args.d_id)
        else:
            print("No CUDA-Device available")
   
    dataset = Brueghel(cfg["dir_dataset"], args.corr_dir, args.img_dim, pretrained_type=args.pre, stride=args.stride, scaling=args.scaling, mirror=False, num_neg=20) 
    dataloader = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0)
    model = CorrNet(cfg).cuda() if args.d_id != -1 else CorrNet(cfg)
    model.eval()

    return model, dataloader


def overfit_test(model, dataloader):
    dists = np.ndarray(shape=len(dataloader), dtype=np.float32) 

    for ii, pair in enumerate(dataloader):
        img1_name, img2_name, corr_pos = pair
        corr_pos = corr_pos[0].to(torch.int64)
    
        img1, img2, corr_pos = forward_setup(img1_name, img2_name, corr_pos)
        feat1_pos, feat2 = model.forward(img1, img2, corr_pos)
        feat_h = feat2.shape[3]

        feat1_pos = np.transpose(feat1_pos.data.cpu().numpy(), [1,0])
        feat2_flat = np.transpose(np.squeeze(feat2.data.cpu().numpy()), [1, 2, 0])
        feat2_flat = np.reshape(feat2_flat, (feat2_flat.shape[0] * feat2_flat.shape[1], -1))
        
        nbrs = NearestNeighbors(n_neighbors=1, algorithm="auto", metric="l2", n_jobs=-1).fit(feat2_flat)
        pts_knn = nbrs.kneighbors(feat1_pos, n_neighbors=1, return_distance=False)
        
        curr_dist = np.ndarray(shape=corr_pos.shape[0], dtype=np.float32) 

        for i in range(corr_pos.shape[0]):
            curr_knn = pts_knn[i]
            curr_knn = np.concatenate((curr_knn // feat_h, curr_knn % feat_h), axis=0).T
            
            curr_knn = torch.from_numpy(curr_knn).float().cuda() if args.d_id != -1 else torch.from_numpy(curr_knn).float()

            feat2_pos = corr_pos[i,2:].repeat(1, 1).float()
            dist = l2Dist(feat2_pos, curr_knn)
            curr_dist[i] = dist.item()
        
        dists[ii] = np.mean(curr_dist)

    return dists 


def print_overfit_results(dists):
    print(f" Mean: {np.mean(dists)}")
    print(f" Std: {np.std(dists)}")


if __name__ == '__main__':
    args = parser.parse_args()
    t0 = time.time()
    loss_epoch_list = list()
    model, dataloader = test_setup()
    dists = overfit_test(model, dataloader)
    print_overfit_results(dists)

