from __future__ import division

# add dir to PYTHONPATH
import os, sys
import os.path as osp
dir_main = osp.abspath(osp.join('..'))
assert(osp.exists(dir_main))
sys.path.append(osp.join(dir_main, 'models', 'vgg16'))
sys.path.append(osp.join(dir_main, 'models', 'res'))
sys.path.append(osp.join(dir_main, 'util'))

import argparse
import numpy as np
import torch, torchvision
from torchvision import transforms
from cv2 import resize
from torch.autograd import Variable
import cv2
import torch.nn as nn
import torch.nn.functional as F

#from googlenet import GoogleNet_4a, GoogleNet_MultiScale_5, GoogleNet_Dilated
from vgg16 import VGG16_Conv4_Relu_MultiScale5, VGG16_Block4, VGG16_Dilated, VGG16_Conv4_S16_Dilated

sys.path.append(osp.join(dir_main, 'models', "res"))
from res import Model as ResNet

from dirs_util import create_dir, check_dir
import csv
import pandas as pd


parser = argparse.ArgumentParser(description='Creates features for query parts and all images in a dataset')
parser.add_argument('--net', type=str, default='vgg_4a', metavar='x', help='which model should be used')
parser.add_argument('--pre', type=str, default="imagenet", metavar='X', help='specifies the pretrained type')
parser.add_argument('--stride', type=int, default=4, metavar='x', help='network stride')
parser.add_argument('--set', type=str, default="brueghel", metavar='X', help='dataset to evaluate on')
parser.add_argument('--exp', type=str, default='default', metavar='x', help='name of experiment')
parser.add_argument('--img_size', type=int, default=224, metavar='x', help='set image size for scaling')
parser.add_argument('--val', dest="val", action="store_true", help='use validation set')
parser.add_argument('--d_id', type=int, default=0, metavar='x', help='set gpu device id')
parser.add_argument('--do', type=str, default="qf", metavar='x', help="create (q)ueries, (b)ounding-box queries or (f)eatures (example: --do qbf creates everything)")
parser.add_argument('--debug', dest="debug", action="store_true", help='debug mode on') 
args = parser.parse_args()

# load config
cfg = dict()
from config import create_features as c

DEBUG = args.debug
if DEBUG: print("DEBUG is ON")

if args.val:
    c["df_main"] = c["df_val"]
    c["df_query"] = c["df_val_query"]

## file configuration
cfg['dir_main'] = check_dir(dir_main)
cfg['dir_datasets'] = check_dir(osp.join("..", "datasets", args.set, "images"))
cfg["out_dir"] = args.net

feature_dim_128 = ["train", "ms5", "d1", "d3", "d5", "vgg_ms5"]
feature_dim_256 = ["res_art", "res_pre", "res_fine"]
feature_dim_512 = ["4a", "vgg_4a", "vgg_d1", "vgg_d3", "vgg_d5", "vgg_4aS16", "vgg_4aS16_fine"]

if args.net in feature_dim_128: cfg["feature_dim"] = 128 
if args.net in feature_dim_256: cfg["feature_dim"] = 256 
if args.net in feature_dim_512: cfg["feature_dim"] = 512 

#vgg model
if args.net == "vgg_4a" or args.net == "vgg_4aS16": cfg["weights"] = "vgg16_caffe_conv4.pt" 
if args.net == "vgg_4aS16_fine": print("insert weights for finetuned S16 here")
if args.net == "vgg_ms5": cfg["weights"] = osp.join(args.set, c["vgg_ms5_weights"]) 
if args.net == "vgg_d1": cfg["weights"] = osp.join(args.set, c["vgg_d1_weights"]) 
if args.net == "vgg_d3": cfg["weights"] = osp.join(args.set, c["vgg_d3_weights"]) 
if args.net == "vgg_d5": cfg["weights"] = osp.join(args.set, c["vgg_d5_weights"]) 

#resnet model
if args.net == "res_pre": cfg["weights"] = "resnet18.pth" 
if args.net == "res_art": cfg["weights"] = "brueghelModel.pth" 
if args.net == "res_fine": cfg["weights"] = osp.join(args.set, c["res_fine_weights"]) 

#network stride check
if "res" in args.net or "S16" in args.net:
    if args.img_size != 896: print("WARNING: net is res or vgg_4aS16 (fine), but img_dim ist not 896!")

cfg['rnd_seed'] = 7
cfg['use_gpu'] = True 
torch.manual_seed(cfg['rnd_seed'])

if "vgg" in args.net: cfg['dir_weights'] = check_dir(osp.join(cfg['dir_main'], "weights", "vgg16", cfg["weights"]))
if "res" in args.net: cfg["dir_weights"] = check_dir(osp.join(cfg["dir_main"], "weights", "res", cfg["weights"]))

if not osp.exists(osp.join(cfg["dir_main"], "exps", args.exp, cfg["out_dir"])):
    os.makedirs(osp.join(cfg["dir_main"], "exps", args.exp, cfg["out_dir"]))

class CorrNet(nn.Module):
    def __init__(self, cfg):
        super(CorrNet, self).__init__()
        self.cfg = cfg
        self.feat = GoogleNet_MultiScale_5()
        self.fully_conv = nn.Conv2d(512, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.load_state_dict(torch.load(cfg["dir_weights"]))

class VGGNet(nn.Module):
    def __init__(self, cfg):
        super(VGGNet, self).__init__()
        self.cfg = cfg
        if args.net == "vgg_ms5":
            self.fully_conv = nn.Conv2d(512, 128, kernel_size=1, stride=1, padding=0, dilation=1)
            self.feat = VGG16_Conv4_Relu_MultiScale5()
        if args.net == "vgg_4a":
            self.feat = VGG16_Block4()
        if args.net == "vgg_d1":
            self.feat = VGG16_Dilated(dilation=1)
        if args.net == "vgg_d3":
            self.feat = VGG16_Dilated(dilation=3)
        if args.net == "vgg_d5":
            self.feat = VGG16_Dilated(dilation=5)
        self.load_state_dict(torch.load(cfg["dir_weights"]))

class DilNet(nn.Module):
    def __init__(self, cfg, dil):
        super(DilNet, self).__init__()
        self.cfg = cfg
        self.feat = GoogleNet_Dilated(dilation=dil)
        self.fully_conv = nn.Conv2d(512, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.load_state_dict(torch.load(cfg["dir_weights"]))

def feat_match(img, cfg):
    img = resize(img, (args.img_size, args.img_size))

    if args.pre == "caffe":
        img = torch.from_numpy(np.transpose(img, [2, 0, 1]).astype('float32')).cuda()
    img = normalize(img).unsqueeze(0).cuda()

    cat_1 = ["vgg_ms5"]
    cat_2 = ["vgg_d1", "vgg_d3", "vgg_d5"]
    cat_3 = ["vgg_4a", "vgg_4aS16"]
    cat_4 = ["res_pre", "res_art", "res_fine"]

    if args.net in cat_1:
        feat = model.feat(img)
        feat = model.fully_conv(feat)
    if args.net in cat_2:
        feat = model.feat(img) 
    if args.net in cat_3:
        feat = model(img)
    if args.net in cat_4:
        feat = model.forward(img)

    #TODO:  nikolai frage
    feat = F.normalize(feat, p=2, dim=1)
    return feat

if torch.cuda.is_available():
    print("GPU: {}".format(args.d_id))
    torch.cuda.set_device(args.d_id)

if args.net == "vgg_4a":
    model = VGG16_Block4().cuda()
    model.load_state_dict(torch.load(cfg["dir_weights"]))
elif args.net == "vgg_4aS16":
    model = VGG16_Conv4_S16_Dilated().cuda()
    model.load_state_dict(torch.load(cfg["dir_weights"]))
elif args.net == "vgg_4aS16_fine":
    print("add here stuff...")
elif "vgg" in args.net: 
    model = VGGNet(cfg).cuda()
if "res" in args.net: 
    model = ResNet(args.d_id, cfg["dir_weights"], "resnet18").cuda()

model.eval()

if args.pre == "imagenet":
    normalize = transforms.Compose([transforms.ToTensor(), transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
if args.pre == "caffe":
    normalize = transforms.Compose([transforms.Normalize(mean=[123., 117., 104.], std=[1., 1., 1.])])

if "q" in args.do or args.do == "":
    features = np.empty(shape=[0, cfg["feature_dim"]])
    #LOAD DATAFRAME ======================================================
    df = pd.read_csv(c["df_query"], index_col=0, dtype={"object": str})
    j = 0
    for i, row in df.iterrows(): 
        print(j)
        j += 1
        im = cv2.imread(f"{cfg['dir_datasets']}/{row.img_name}")
        if im is None:
            sys.exit("imread returned None!")
        im_f = cv2.flip(im, 1)
        h, w = im.shape[0], im.shape[1]
        rX = row.midx / w 
        rY = row.midy / h 

        rX_f = (w - row.midx) / w 
        rY_f = (h - row.midy) / h 
        
        newX = int(args.img_size * rX / args.stride)
        newY = int(args.img_size * rY / args.stride)

        newX_f = int(args.img_size * rX_f / args.stride)
        newY_f = int(args.img_size * rY_f / args.stride)

        feat = feat_match(im, cfg).detach().cpu().numpy()
        patch_feat = feat[0, :, newY, newX]
        patch_feat = np.reshape(patch_feat, (1, cfg["feature_dim"]))
        features = np.append(features, patch_feat, axis=0)

        feat_f = feat_match(im_f, cfg).detach().cpu().numpy()
        patch_feat_f = feat_f[0, :, newY, newX]
        patch_feat_f = np.reshape(patch_feat_f, (1, cfg["feature_dim"]))
        features = np.append(features, patch_feat_f, axis=0)
            

    np.save(os.path.join(cfg["dir_main"], "exps", args.exp, cfg["out_dir"], "query_features"), features)
    print("Done saving query-features!")

if "b" in args.do:
    df = pd.read_csv(c["df_query"], index_col=0, dtype={"object": str})
    if not osp.exists(osp.join(cfg["dir_main"], "exps", args.exp, cfg["out_dir"], "bbx_queries")):
        os.makedirs(osp.join(cfg["dir_main"], "exps", args.exp, cfg["out_dir"], "bbx_queries"))

    j = 0
    for i, row in df.iterrows(): 
        if DEBUG and j == 2:
            break
        print(j)
        j += 1
        im = cv2.imread(f"{cfg['dir_datasets']}/{row.img_name}")
        if im is None:
            sys.exit("imread returned None!")
        h, w = im.shape[0], im.shape[1]
        rx1 = row.bbs_x1 / w # btween 0 and 1
        rx2 = row.bbs_x2 / w
        ry1 = row.bbs_y1 / h
        ry2 = row.bbs_y2 / h

        new_x1 = int(args.img_size * rx1 / args.stride)
        new_x2 = int(args.img_size * rx2 / args.stride)
        new_y1 = int(args.img_size * ry1 / args.stride)
        new_y2 = int(args.img_size * ry2 / args.stride)

        feat = feat_match(im, cfg).detach().cpu().numpy() 
        bbx_feature = feat[0, :, new_y1:new_y2, new_x1:new_x2]
        bbx_feature = np.reshape(bbx_feature, (cfg["feature_dim"], bbx_feature.shape[1] * bbx_feature.shape[2])).T
        np.save(osp.join(cfg["dir_main"], "exps", args.exp, cfg["out_dir"], "bbx_queries", f"{i}_{row.img_name}"), bbx_feature)

    print("Done saving bbx-query-features!")

if "f" in args.do or args.do == "":
    output_size = int(args.img_size / args.stride)
    if not osp.exists(osp.join(cfg["dir_main"], "exps", args.exp, cfg["out_dir"], "features")):
        os.makedirs(osp.join(cfg["dir_main"], "exps", args.exp, cfg["out_dir"], "features"))

    df_unique = pd.read_csv(c["df_main"], index_col=0, dtype={"object": str})
    df_unique = df_unique.img_name.unique()

    for i, image in enumerate(df_unique):
        if i % 100 == 0 and i != 0:
            print(i)
        im = cv2.imread(os.path.join(cfg["dir_datasets"], f"{image}"))
        if im is None:
            sys.exit("imread returned None!")
        feat = feat_match(im, cfg).detach().cpu().numpy()
        feat = feat[0,:,:,:].transpose(1,2,0).reshape(output_size * output_size, cfg["feature_dim"])
        np.save(osp.join(cfg["dir_main"], "exps", args.exp, cfg["out_dir"], "features", image), feat)

