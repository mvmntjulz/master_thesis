import itertools
import cv2
import os
from os import path as osp
import sys
import numpy as np
import pandas as pd
import argparse
from matplotlib import pyplot as plt

parser = argparse.ArgumentParser(description='visualize retrieval results')
parser.add_argument('--net', type=str, default='4a', metavar='x', help='which model should be used')
parser.add_argument('--exp', type=str, default='default', metavar='x', help='name of experiment')
parser.add_argument('--set', type=str, default="brueghel", metavar='x', help='dataset to visualize')
parser.add_argument('--bbx', action="store_true", help='visualize bbx-retrieval results')
#parser.add_argument('--t', type=int, default=45, metavar='x', help='threshold for new retrievals')
args = parser.parse_args()

# load config
from config import visualize as c
c["dir_main"] = osp.join("..")


c['dir_datasets'] = osp.join(c["dir_main"], "datasets", args.set, "images")
w = c["grid_size"][0] # Width of the matrix (nb of images)
h = c["grid_size"][1] # Height of the matrix (nb of images)
n = w*h

if not osp.exists(osp.join(c["dir_main"], "exps", args.exp, args.net, "vis")):
    os.makedirs(osp.join(c["dir_main"], "exps", args.exp, args.net, "vis"))

def create_img_list(qd):
    df_info = pd.read_csv(os.path.join(qd, "info.csv"), index_col=0)
    imgs = list()

    for _, row in df_info.iterrows():
        img = cv2.imread(os.path.join(c["dir_datasets"], row["img_name"]))
        if not args.bbx:
            if row["gt_y"] != -1:
                cv2.circle(img, (row["gt_x"], row["gt_y"]), 15, (255, 0, 0), 3)
                if row["tp"]:
                    cv2.circle(img, (row["pred_x"], row["pred_y"]), 15, (0, 255, 0), 3)
                else:
                    cv2.circle(img, (row["pred_x"], row["pred_y"]), 15, (0, 0, 255), 3)
        else:
            GREEN = [0,255,0]
            RED = [0,0,255]
            img = cv2.copyMakeBorder(img, 10,10,10,10,cv2.BORDER_CONSTANT, value=GREEN if row.tp else RED)

        imgs.append(img) 
    return imgs

def visualize():
    query_dirs = [x[0] for x in os.walk(os.path.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep"))][1:]
    if not len(query_dirs):
        print("No retrievals at given path!")
        return 0

    for i, qd in enumerate(query_dirs):
        print(f"{i}/{len(query_dirs)}")
        imgs = create_img_list(qd)
        img_h, img_w, img_c = (250, 250, imgs[0].shape[2]) 

        mat_x = img_w * w + c["margin"] * (w - 1)
        mat_y = img_h * h + c["margin"] * (h - 1)

        imgmatrix = np.zeros((mat_y, mat_x, img_c),np.uint8)
        imgmatrix.fill(255)
        positions = itertools.product(range(h), range(w))

        for (y_i, x_i), img in zip(positions, imgs):
            x = x_i * (img_w + c["margin"])
            y = y_i * (img_h + c["margin"])
            img = cv2.resize(img, (img_w, img_h))
            imgmatrix[y:y+img_h, x:x+img_w, :] = img

        cv2.imwrite(os.path.join(c["dir_main"], "exps", args.exp, args.net, "vis", f"{i}.jpg"), imgmatrix)
    print("Done visualizing retrievals!")

visualize()
