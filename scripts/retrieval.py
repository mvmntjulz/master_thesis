import sys
import os
import os.path as osp
dir_main = osp.abspath(osp.join('..'))
sys.path.append(osp.join(dir_main, 'util'))

import numpy as np
import faiss
import copy
import matplotlib.pyplot as plt
import csv
import cv2
from dirs_util import create_dir, check_dir
import pandas as pd
import math
import pdb
import argparse

parser = argparse.ArgumentParser(description='train VGG16 dilation with GT correspondences')
parser.add_argument('--net', type=str, default='4a', metavar='x', help='which model should be used')
parser.add_argument('--stride', type=int, default=4, metavar='x', help='network stride')
parser.add_argument('--set', type=str, default="brueghel", metavar='X', help='dataset to evaluate on')
parser.add_argument('--exp', type=str, default='default', metavar='x', help='name of experiment')
parser.add_argument('--img_size', type=int, default=224, metavar='x', help='set image size for scaling')
parser.add_argument('--bbx', action="store_true",help='do bbx or single feature retrieval')
parser.add_argument('--nn_faiss', type=int, default=1000, metavar='x', help='how many nn for a given query-box should be searched with the faiss index')
parser.add_argument('--nn_eva', type=int, default=100, metavar='x', help='how many nn for a given query in evaluation')
parser.add_argument('--val', dest="val", action="store_true", help='use validation set')
parser.add_argument('--po', dest="po", action="store_true", help='allow for part only evaluation')
parser.add_argument('--t', type=int, default=45, metavar='x', help='threshold for new retrievals')
parser.add_argument('--create_idx', dest="create_idx", action="store_true", help='create index (instead of reading)')
parser.add_argument('--pca', dest="pca", action="store_true", help='only perform pca for 512-dim models')
parser.add_argument('--per_img', dest="per_img", action="store_true", help='retrieval per image')
parser.add_argument('--alpha', type=float, default=0.5, metavar='x', help='factory for distance in pck')
parser.add_argument('--debug', dest="debug", action="store_true", help='debug mode on')
args = parser.parse_args()

# Load config file
from config import retrieval as c

DEBUG = args.debug
if DEBUG: print("DEBUG is ON")

k = args.nn_eva * 20 
t_test = c["t_test"] 

print("USING FLIPPED QUERIES IS ON") if c["flipped_queries"] else print("USING FLIPPED QUERIES IS OFF")

feature_dim_128 = ["train", "ms5", "d1", "d3", "d5", "vgg_ms5"]
feature_dim_256 = ["res_art", "res_pre", "res_fine"]
feature_dim_512 = ["4a", "vgg_4a", "vgg_d1", "vgg_d3", "vgg_d5", "vgg_4aS16", "vgg_4aS16_fine"]

if args.net in feature_dim_128: c["feature_dim"] = 128 
if args.net in feature_dim_256: c["feature_dim"] = 256 
if args.net in feature_dim_512: c["feature_dim"] = 512 

c['dir_main'] = check_dir(dir_main)
c['dir_datasets'] = check_dir(osp.join("..", "datasets", args.set, "images"))

#network stride check
if "res" in args.net or "S16" in args.net:
    if args.img_size != 896: print("WARNING: net is res or vgg_4aS16 (fine), but img_dim ist not 896!")

if args.val:
    c['df_main'] = c["df_val"]
    c['df_query'] = c["df_val_query"]

if not args.bbx:
    if c["flipped_queries"]:
        quer = np.load(osp.join(c["dir_main"], "exps", args.exp, args.net, "query_features.npy")).astype("float32")
    else:
        quer = np.load(osp.join(c["dir_main"], "exps", args.exp, args.net, "query_features_x5.npy")).astype("float32")
    if DEBUG:
        quer = quer[0:20, :]
    print(f"Using {quer.shape[0]} queries with {quer.shape[1]} dimensions.")

print("======CFG======")
print(c)
print(args)
print("======CFG======")

if not osp.exists(osp.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep")):
    os.makedirs(osp.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep"))


def save_config():
    with open(osp.join(c["dir_main"], "exps", args.exp, args.net, "config.txt"), "w") as f:
        f.write(str(c) + "\n" + str(args))

def do_pca():
    if "res" in args.net:
        samples = np.empty(shape=[0, 256]).astype("float32")
    else: 
        samples = np.empty(shape=[0, 512]).astype("float32")
    for i, f in enumerate(os.listdir(os.path.join(c["dir_main"], "exps", args.exp, args.net, "features"))):
        if i < 100:
            feature = np.load(os.path.join(c["dir_main"], "exps", args.exp, args.net, "features", f)).astype("float32")
            samples = np.append(samples, feature, axis=0)
        else:
            break
    if "res" in args.net:
        mat = faiss.PCAMatrix(256, 128)
    else:
        mat = faiss.PCAMatrix(512, 128)
    mat.train(samples)
    faiss.write_VectorTransform(mat, f"mat_{args.net}.pca")
    return mat 


def save_index(index):
    print("Saving index to file...")
    faiss.write_index(index, os.path.join(c["dir_main"], "exps", args.exp, args.net, "index.idx"))


def read_index():
    print(f"Reading index from file...")
    return faiss.read_index(os.path.join(c["dir_main"], "exps", args.exp, args.net, "index.idx"))


def build_index(mat):
    index = faiss.IndexFlatL2(128)
    print("Building index...")
    df_unique = pd.read_csv(c["df_main"], index_col=0)
    df_unique = df_unique.img_name.unique()

    for i, img_name in enumerate(df_unique):
        if i % 1000 == 0 and i != 0:
            print(i)
        if i == 300 and DEBUG:
            break
        feature = np.load(osp.join(c["dir_main"], "exps", args.exp, args.net, "features", img_name + ".npy")).astype("float32")
        if args.net == "4a" or args.net == "d5_4a" or args.net == "vgg_4a" or args.net == "vgg_d1" or args.net == "vgg_d3" or args.net == "vgg_d5" or "res" in args.net or "vgg_4aS16" in args.net:
            feature = mat.apply_py(np.ascontiguousarray(feature))
        index.add(np.ascontiguousarray(feature))
    return index


def our_faiss(k=None, quer=None):
    mat = None
    if args.net == "4a" or args.net == "d5_4a" or args.net == "vgg_4a" or args.net == "vgg_d1" or args.net == "vgg_d3" or args.net == "vgg_d5" or "res" in args.net or "vgg_4aS16" in args.net:
        mat = faiss.read_VectorTransform(f"mat_{args.net}.pca")
        if not args.bbx:
            quer = mat.apply_py(np.ascontiguousarray(quer))

    if args.create_idx:
        index = build_index(mat) 
        save_index(index)
    else:
        index = read_index()
    if not args.bbx:
        print("NN-Search...")
        D, I = index.search(quer,k)
        return D, I
    else:
        return index


def index_2_patch(index):
    output_size = int(args.img_size / args.stride)
    ft_no = index % (output_size * output_size)
    im_no = int(index / (output_size * output_size))
    return im_no, ft_no


def feat_no_2_pix_coords(ft_no, shape):
    output_size = int(args.img_size / args.stride)
    fx = ft_no % output_size 
    fy = int(ft_no / output_size)
    rx = fx * args.stride / args.img_size
    ry = fy * args.stride / args.img_size
    h, w = shape[0], shape[1]
    x = int(w * rx) 
    y = int(h * ry) 
    return x, y


def save_query_image(row, q_nbr):
    q_im = cv2.imread(f"{c['dir_datasets']}/{row[1][0]}")
    cv2.circle(q_im, (row[1]["midx"], row[1]["midy"]), 10, (255,0,0), 3)
    cv2.imwrite(os.path.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep", str(q_nbr), f"{row[1][0]}"), q_im)


def create_retrieval_list(q_nbr, I, D, k, df_main, df_main_unique):
    retrieval_list = list()

    for j in range(k):
        idx = I[q_nbr, j]
        dist = D[q_nbr, j]
        img_n, ft_n = index_2_patch(idx)
        img_name = df_main_unique[img_n] 
        neighbor_img = cv2.imread(f"{c['dir_datasets']}/{img_name}")
        pix = feat_no_2_pix_coords(ft_n, neighbor_img.shape) 
        retrieval_list.append([img_name, pix[0], pix[1], dist]) 
    return retrieval_list

"""
def get_part_from_pos(img_name, pos, df_main):
    df_part = df_main.loc[df_main.img_name == img_name]
    df_part = df_part.loc[df_part.part != "object"] 
    cands = list()

    for cand in df_part.iterrows():
        # bbx = x1, y1, x2, y2
        bbx = (cand[1][7],cand[1][8], cand[1][9], cand[1][10])
        if is_in_box(bbx, pos):
            cands.append((cand, get_diag(bbx)))
    return None if not cands else min(cands, key = lambda t: t[1])[0]

def get_diag(bbx):
    return math.sqrt((bbx[2]- bbx[0])**2 + (bbx[3] - bbx[1])**2)

def is_in_box(bbx, pos):
    return True if pos[0] > bbx[0] and pos[0] < bbx[2] and pos[1] > bbx[1] and pos[1] < bbx[3] else False
"""

def filter_for_similars(cl, row, df_main):
    l = 0 
    nbr_discard = 0
    retrievals = list()
    found_images = list()

    for i in cl:
        if l == args.nn_eva:
            break
        if not args.per_img:
            in_same_image = [retr for retr in retrievals if retr[0] == i[0] 
                    and is_close((retr[1], retr[2]), (i[1], i[2]))]
            
            if not in_same_image:
                retrievals.append(i)
                l += 1
            else:
                nbr_discard += 1
        else:
            if i[0] in found_images:
                nbr_discard += 1
            else:
                retrievals.append(i)
                found_images.append(i[0])
                l += 1
    return retrievals, nbr_discard

def is_close(p1, p2):
    return abs(p1[0] - p2[0]) < args.t and abs(p1[1] - p2[1]) < args.t 


def evaluate(q_nbr, cl, row, df_main):
    precision_at = dict() 
    precision = 0
    found_idx = list()
    df_info = pd.DataFrame(data=None, index=[], columns=["img_name", "gt_x", "gt_y", "pred_x", "pred_y", "dist", "bbs_x1", "bbs_y1", "bbs_x2", "bbs_y2", "tp"])
    txt_info = np.zeros(shape=[len(cl)]).astype("int8")

    for n, i in enumerate(cl):
        #neighbor_img = cv2.imread(f"{c['dir_datasets']}/{i[0]}")
        df_img = df_main.loc[df_main.img_name == i[0]]
       
        if args.po:
            df_part = df_img.loc[df_img.part == row[1]["part"]]
        else:
            df_object = df_img.loc[df_img["object"] == row[1]["object"]] 
            df_part = df_object.loc[df_object.part == row[1]["part"]]

            if df_object.empty: # is object in image?
                df_info.loc[n] = [i[0], -1, -1, i[1], i[2], -1, -1, -1, -1, -1, 0]
                add_prec(precision_at, precision, n)
                continue

        if df_part.empty: # is part in object?
            df_info.loc[n] = [i[0], -1, -1, i[1], i[2], -1, -1, -1, -1, -1, 0]
            add_prec(precision_at, precision, n)
            continue

        closest_part, closest_idx, distance, tp = pck(df_part, i)
        # TODO: Add correct retreival acception for expandend queries if exp. retrievals wont get themselves as first neighbor
        if tp and closest_idx not in found_idx:
                precision += 1
                found_idx.append(closest_idx) 
        else:
            tp = False

        txt_info[n] = 1 if tp else 0

        df_info.loc[n] = [i[0], 
                closest_part["midx"], closest_part["midy"], 
                i[1], i[2], 
                distance, 
                closest_part["bbs_x1"], 
                closest_part["bbs_y1"], 
                closest_part["bbs_x2"], 
                closest_part["bbs_y2"],
                1 if tp else 0]
        add_prec(precision_at, precision, n)
    return df_info, txt_info, precision_at


def pck(df_part, i):
    distance = 1e10
    closest_part = df_part.values[0]
    closest_idx = 0
    for idx, part in df_part.iterrows():
        current_dist = math.sqrt((i[1] - part.midx)**2 + (i[2] - part.midy)**2)
        if current_dist < distance:
            distance = current_dist
            closest_part = part
            closest_idx = idx
    diag = math.sqrt((closest_part["bbs_x1"] - closest_part["bbs_x2"])**2 + (closest_part["bbs_y1"] - closest_part["bbs_y2"])**2)
    tp = True if diag * args.alpha > distance else False
    return closest_part, closest_idx, distance, tp


def add_prec(precision_at, precision, n):
    at = [1, 5, 10, 20, 50, 100]
    if n in at:
        precision_at[n] = max(0, precision - 1) / float(n)
    #if c["flipped_queries"]: # using flipped queries
    #    if n in at:
    #        precision_at[n] = max(0, precision -1) / float(n)
    #elif n+1 in at: # using expanded queries
    #    precision_at[n+1] = max(0, precision) / float(n+1)


def retrieval(I, D, k):
    df_query = pd.read_csv(c["df_query"], index_col=0, dtype={"object": str})  
    df_main = pd.read_csv(c["df_main"], index_col=0, dtype={"object": str})
    df_main_unique = pd.unique(df_main["img_name"].values) 
    q_nbr = -1 
    precisions = list()
    txt_info_global = np.empty(shape=[0, args.nn_eva]).astype("int32")
    df_info_global = pd.DataFrame(data=None, index=[], columns=["object",
                                                                "part",
                                                                "prec1", "prec5",
                                                                "prec10", "prec20",
                                                                "prec50", "prec100", "nbr_discard"])
    for row in df_query.iterrows():
        #l = 1
        q_nbr += 1
        if q_nbr == 200 and t_test:
            break
        #detections = dict()

        if DEBUG and q_nbr == 5:
            break
        
        if not os.path.exists(os.path.join(os.path.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep", f"{q_nbr}"))):
            os.makedirs(os.path.join(os.path.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep", f"{q_nbr}")))

        save_query_image(row, q_nbr)
        if c["flipped_queries"]: 
            original_list = create_retrieval_list(q_nbr*2, I, D, k, df_main, df_main_unique)
            flipped_list = create_retrieval_list(q_nbr*2 + 1, I, D, k, df_main, df_main_unique)
            combined_list = sorted(original_list + flipped_list, key=lambda tup: tup[3])
        else:
            original_list = create_retrieval_list(q_nbr, I, D, k, df_main, df_main_unique)
            combined_list = sorted(original_list, key=lambda tup: tup[3])

        combined_list, nbr_discard = filter_for_similars(combined_list, row, df_main)
        df_info, txt_info, precision_at = evaluate(q_nbr, combined_list, row, df_main)

        txt_info_global = np.append(txt_info_global, np.reshape(txt_info, (1, args.nn_eva)), axis=0)

        csv_file = os.path.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep", str(q_nbr), "info.csv")
        df_info.to_csv(csv_file)
        np.savetxt(osp.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep", str(q_nbr), "info.txt"), txt_info, fmt='%s')
        
        precisions.append(precision_at)
        print(f"q:{q_nbr}, {precision_at}", nbr_discard)

        df_info_global.loc[q_nbr] = [row[1]["object"], row[1]["part"],
                                     precisions[q_nbr][1],
                                     precisions[q_nbr][5],
                                     precisions[q_nbr][10],
                                     #precisions[q_nbr][20],
                                     #precisions[q_nbr][50],
                                     #precisions[q_nbr][100],
                                     0,
                                     0,
                                     0,
                                     nbr_discard]

    if c["flipped_queries"]: 
        file_name = "info_" + str(args.t) + ".csv"
        txt_name = "info_" + str(args.t) + ".txt"
    else:
        file_name = "info" + "_x_" + str(args.t) + ".csv"
        txt_name = "info" + "_x_" + str(args.t) + ".txt"
    csv_file = os.path.join(c["dir_main"], "exps", args.exp, args.net, file_name)
    df_info_global.to_csv(csv_file)
    np.savetxt(osp.join(c["dir_main"], "exps", args.exp, args.net, txt_name), txt_info_global, fmt='%s')

def bbx_retrieval(index):
    output_size = int(args.img_size / args.stride)
    df_query = pd.read_csv(c["df_query"], index_col=0, dtype={"object": str})  
    df_main = pd.read_csv(c["df_main"], index_col=0, dtype={"object": str})
    df_main_unique = pd.unique(df_main["img_name"].values) 
    precision_global = np.zeros(shape=(df_query.shape[0], df_main_unique.shape[0])).astype("int32")
    nbr_q_per_class = df_query.groupby("object").count()

    if args.net == "4a" or args.net == "d5_4a" or args.net == "vgg_4a" or args.net == "vgg_d1" or args.net == "vgg_d3" or args.net == "vgg_d5" or "res" in args.net:
        mat = faiss.read_VectorTransform(f"mat_{args.net}.pca")

    for idx, q_row in df_query.iterrows():
        print(idx + 1, "/", df_query.shape[0])
        df_info = pd.DataFrame(data=None, index=[], columns=["img_name", "counts", "tp"])
        bbx_features = np.load(osp.join(c["dir_main"], "exps", args.exp, args.net, "bbx_queries", f"{idx}_{q_row.img_name}.npy")).astype("float32")
        if not osp.exists(osp.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep", str(idx))):
            os.makedirs(os.path.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep", str(idx)))

        # apply pca to get to 128 feature dim
        if args.net == "4a" or args.net == "d5_4a" or args.net == "vgg_4a" or args.net == "vgg_d1" or args.net == "vgg_d3" or args.net == "vgg_d5" or "res" in args.net:
            bbx_features = mat.apply_py(np.ascontiguousarray(bbx_features))
        
        D, I = index.search(np.ascontiguousarray(bbx_features), args.nn_faiss)
        I_img = np.floor(I / (output_size * output_size)).astype("int32")
                    
        # check if first hit is always the query image
        #check_first_hit(q_row.img_name, df_main_unique, I_img)

        I_img = uniques_per_row(q_row, I_img, nbr_q_per_class) 
        values, counts = np.unique(I_img, return_counts=True)
        
        # delete the -1 entry if present
        ind = np.argwhere(values == -1)
        if not len(ind):
            print("Warning: values had no entry = -1")
        else:
            values = np.delete(values, ind)
            counts = np.delete(counts, ind)

        #nn_ind = counts.argsort()[-args.nn_eva-1:][::-1]
        nn_ind = counts.argsort()[::-1]
        counts = counts[nn_ind]
        values = values[nn_ind]
        
        prec_row = bbx_evaluate(df_main, idx, q_row, df_main_unique[values])
        precision_global[idx, :prec_row.shape[0]] = prec_row

        df_info["img_name"] = df_main_unique[values]
        df_info["counts"] = counts
        df_info["tp"] = precision_global[idx,: prec_row.shape[0]]
        df_info.to_csv(osp.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep", str(idx), "info.csv"))

    np.savetxt(osp.join(c["dir_main"], "exps", args.exp, args.net, "info_bbx.txt"), precision_global, fmt='%i')


def check_first_hit(q_img_name, df_main_unique, I_img):
    for img_index in I_img[:, 0]:
        if q_img_name != df_main_unique[img_index]:
            print("Warning: First retrieval was not query image itself!")
            return False
    return True

def uniques_per_row(q_row, a, nbr_q_per_class):
    temp = np.full(a.shape, -1,  dtype="int32") 
    nbr_gt = nbr_q_per_class.loc[nbr_q_per_class.index == q_row.object].values[0][0]

    for i, row in enumerate(a):
        unique_row = np.unique(row)
        if len(unique_row) < nbr_gt:
            print(f"Error: Did not find enough unique images in faiss index! {len(unique_row)} / {nbr_gt}")
            sys.exit()
        temp[i,:len(unique_row)] = unique_row
    return temp 

def bbx_evaluate(df_main, idx, row, retrievals):
    query_img = cv2.imread(f"{c['dir_datasets']}/{row.img_name}")
    cv2.rectangle(query_img, (row.bbs_x1, row.bbs_y1), (row.bbs_x2, row.bbs_y2), (255,0,0), 2)
    cv2.imwrite(osp.join(c["dir_main"], "exps", args.exp, args.net, "retr_sep", str(idx), f"{row.img_name}"), query_img)
    precision_local = np.zeros(shape=retrievals.shape)

    for i, retr in enumerate(retrievals):
        retr_images = df_main.loc[df_main.img_name == retr]
        precision_local[i] = 1 if row.object in retr_images.object.values else 0 
    return precision_local
        

if args.pca:
    do_pca()
else:
    save_config()
    if args.bbx:
        index = our_faiss()
        bbx_retrieval(index)
    else:
        D, I = our_faiss(k, quer)
        retrieval(I, D, k)
