
Artminer Training Settings:
     In each Epoch,
        1. 2000 2X2 features are utilized to search candidate regions;
        2. we validate on the outermost part in 10X10 region;
        3. We train on 4 corners in the 12X12 region for the top 200 pairs;
        4. Batch size is 4, thus each epoch we do 50 update.
        We search to match in 7 scales, the max dimensions in the feature maps are:
            [80 74 68 60 49 36 20]

Query-Image scales: [68, 60, 49, 40, 36] während training. Wird pro query rnd ausgewählt.

Procedure:
    1. RandomQueryFeat: get 2000 random 2x2 query-patches
    2. RetrievalRes: for each of the 2000 query patches get top 10 images, scales and positons that match the query patch.
    3. TrainPairs: Until 2000: Pick random query and for that query 2 of its top 10 image region matches. Rank dem by votes (ValidRegion "10x10"region"). Take the top 200 of those 2000 pairs.
    4. For each epoch: 
        - permute those 200 pairs into 50 batches with size 4 (200/50)
        For each Batch:
            For each pair:
                - calculate pos-similarity by matching the pos (4 corners) in feat1 feature rectangles (3x3) in feat2 (for each of the 4 corners individually). Top1 is taken as distance.
                - calculate neg_similarity by matching the pos in feat1 to every feature in feat2. mean(Top10) is taken as distance. (repreat with feat1 = feat2, feat2 = feat1). Take the greater one as result
                - thus, "We select the negatives as the set of top matches to P1 in P2’s image." makes sence now... 
                - triplet-loss 

