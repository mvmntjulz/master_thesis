#!/bin/sh
EXP_NAME=$1
TRAIN_NET=$2     
EVA_NET=$3     
CORR_DIR=$4

LOSS=$5
DIST=$6
LR=$7
OPT=$8
LOSS_TH=$9

NN="${10}"
HNN="${11}"
HNR="${12}"
HN_DIR="${13}"

HUE="${14}"
HUE_C="${15}"
GRAY_C="${16}"

BS="${17}"
BETA="${18}"
HEAD="${19}"
D_ID="${20}"

EPOCH="${21}"

# CAN BE EMPTY, HAVE TO BE LAST:
HN_SCALE="${22:-""}"
HN_SCALE_GL="${23:-""}"
POS_SCALE="${24:-""}"
POS_SHIFT="${25:-""}"
EVA_GRAY="${26:-""}"
EVA_HEAD="${27:-""}"

#FURTHER EVA_PARA
JSON="../data/brueghelVal.json"
SEARCH_DIR="../data/Brueghel"
EVA_TYPE="val"
NBPRED="1000"

if [[ $E_NET == *"_fine"* ]]; then
    cd /export/home/jstern/thesis/scripts
    conda deactivate
    conda activate server
    python training.py --epoch $EPOCH --scaling --net $TRAIN_NET --corr_dir $CORR_DIR --d_id $D_ID --exp_name $EXP_NAME --loss $LOSS --loss_th $LOSS_TH --dist $DIST --nn $NN --hnn $HNN --hnr $HNR --bs $BS --lr $LR --opt $OPT --hn_dir $HN_DIR --beta $BETA --hue $HUE --hue_chance $HUE_C --gray_chance $GRAY_C --head $HEAD $HN_SCALE $HN_SCALE_GL $POS_SCALE $POS_SHIFT
fi

cd /export/home/jstern/cvi/ArtMiner/eva_art
conda deactivate
conda activate artminer

python retrieval.py --net $EVA_NET --d_id $D_ID --exp $EXP_NAME --searchDir $SEARCH_DIR  --eva_type $EVA_TYPE --labelJson $JSON --epoch $EPOCH --margin 5 --cuda --cropSize 0 --nbPred $NBPRED --nbDraw 0 --visualDir vis --queryFeatMax 8 --IoUThreshold 0.3 --detTxt det.txt --detJson det.json --detMAP mAP.json --head $HEAD $EVA_HEAD $EVA_GRAY 

conda deactivate
cd ~/thesis/exps/
