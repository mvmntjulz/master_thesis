#!/bin/sh
D_ID=$1

T_NET="res"     # "vgg_py"
E_NET="res_fine"  # "vgg_py_fine" | "res_untrained" | "res" ("*_fine" => train AND eva)
CD="res_60k"        # "corrs_train_shift" (40k)

L="contrastive"     # "triplet"
D="l2"              # "cos" 
LR="1e-6"
O="adam"            # "radam" 
TH=-1

NN=20
HNN=20
HNR=4
HND="both"           # "r2l" | "both"

HUE="0.0"           # hue value
HC="0.0"            # prob for hue-aug on image
GC="0.0"            # prob for gray-aug on image

BS=1                # batch-size
B="0.5"             # first beta value for adam optimizer (0.5 in artminer training)
H="0"               # dimension of head layer (0:= no head layer)

E="1"               # number of epochs to train

HNS=""              # "--hn_scale" for hard negative scaling over all scales during training
HNSG=""             # "--hn_scale_gl" for global hard negative scaling
PSC=""              # "--pos_scale" scale positive corrs according to best hn-scale
PSH=""              # "--pos_shift" shift positive corrs along the scale list
EG=""               # "--gray" use gray in evaluation 
EH=""               # "--eva_head" use_head in evaluation

. ./train_eva.sh "exp_name" $T_NET $E_NET $CD $L $D $LR $O $TH $NN $HNN $HNR $HND $HUE $HC $GC $BS $B $H $D_ID $E "$HNS" "$HNSG" "$PSC" "$PSH" "$EG" "$EH"
