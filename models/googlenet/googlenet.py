from __future__ import division
import torch
import torch.nn as nn
import torch.nn.functional as F


# pytorch implementation of GoogleNet
class LRN(nn.Module):
    def __init__(self, local_size=1, alpha=1.0, beta=0.75, ACROSS_CHANNELS=True):
        super(LRN, self).__init__()
        self.ACROSS_CHANNELS = ACROSS_CHANNELS
        if ACROSS_CHANNELS:
            self.average=nn.AvgPool3d(kernel_size=(local_size, 1, 1),
                                      stride=1,
                                      padding=(int((local_size-1.0)/2), 0, 0))
        else:
            self.average=nn.AvgPool2d(kernel_size=local_size,
                                      stride=1,
                                      padding=int((local_size-1.0)/2))
        self.alpha = alpha
        self.beta = beta

    def forward(self, x):
        if self.ACROSS_CHANNELS:
            div = x.pow(2).unsqueeze(1)
            div = self.average(div).squeeze(1)
            div = div.mul(self.alpha).add(1.0).pow(self.beta)
        else:
            div = x.pow(2)
            div = self.average(div)
            div = div.mul(self.alpha).add(1.0).pow(self.beta)
        x = x.div(div)
        return x


class Flatten(nn.Module):
    def __init__(self):
        super(Flatten, self).__init__()

    def forward(self, x):
        return x.view(x.size(0), -1)


class Conv2d_MultiScale_5(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        super(Conv2d_MultiScale_5, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5)
        self.scale_W = nn.Parameter(torch.zeros(5))

    def forward(self, x):
        x1 = self.Conv2d_Scale1(x)
        x2 = self.Conv2d_Scale2(x)
        x3 = self.Conv2d_Scale3(x)
        x4 = self.Conv2d_Scale4(x)
        x5 = self.Conv2d_Scale5(x)
        weight = F.softmax(self.scale_W, dim=0)
        return x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]



class Conv2d_MultiScale_5_init2(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        super(Conv2d_MultiScale_5_init2, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5)
        self.scale_W = nn.Parameter(torch.FloatTensor([1e12, 0., 0., 0., 0.]))
        #
    def forward(self, x):
        x1 = self.Conv2d_Scale1(x)
        x2 = self.Conv2d_Scale2(x)
        x3 = self.Conv2d_Scale3(x)
        x4 = self.Conv2d_Scale4(x)
        x5 = self.Conv2d_Scale5(x)
        weight = F.softmax(self.scale_W, dim=0)
        return x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]




class Conv2d_MultiScale_Attention_5(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        super(Conv2d_MultiScale_5, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5)
        self.scale_W = nn.Parameter(torch.zeros(5))

    def forward(self, x):
        print('test')
        x1 = self.Conv2d_Scale1(x)
        x2 = self.Conv2d_Scale2(x)
        x3 = self.Conv2d_Scale3(x)
        x4 = self.Conv2d_Scale4(x)
        x5 = self.Conv2d_Scale5(x)
        weight = F.softmax(self.scale_W, dim=0)
        return  x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]



class Conv2d_BigFilter_5(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        super(Conv2d_BigFilter_5, self).__init__()
        assert kernel_size % 2 == 1
        # - formula modified kernel size: D*(K-1)+1 with d: dilation rate, K: kernel size
        self.Conv2d = nn.Conv2d(in_channels, out_channels, int(5 * (kernel_size - 1) + 1),
                                stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=1)

    def forward(self, x):
        x1 = self.Conv2d(x)
        return x1


class Conv2d_MultiScale_5_Const(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        super(Conv2d_MultiScale_5_Const, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5)

    def forward(self, x):
        x1 = self.Conv2d_Scale1(x)
        x2 = self.Conv2d_Scale2(x)
        x3 = self.Conv2d_Scale3(x)
        x4 = self.Conv2d_Scale4(x)
        x5 = self.Conv2d_Scale5(x)
        return  0.2 * (x1 + x2 + x3 + x4 + x5)


class Conv2d_MultiScale_5_Max(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        super(Conv2d_MultiScale_5_Max, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5)

    def forward(self, x):
        x1 = self.Conv2d_Scale1(x)
        x2 = self.Conv2d_Scale2(x)
        x3 = self.Conv2d_Scale3(x)
        x4 = self.Conv2d_Scale4(x)
        x5 = self.Conv2d_Scale5(x)
        return  torch.max(torch.max(torch.max(torch.max(x1, x2), x3), x4), x5)


class Conv2d_MultiScale_5_FC(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        super(Conv2d_MultiScale_5_FC, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5)
        self.FC = nn.Conv2d(out_channels * 5, out_channels, kernel_size=1, stride=1, padding=0, dilation=1)

    def forward(self, x):
        x1 = self.Conv2d_Scale1(x)
        x2 = self.Conv2d_Scale2(x)
        x3 = self.Conv2d_Scale3(x)
        x4 = self.Conv2d_Scale4(x)
        x5 = self.Conv2d_Scale5(x)
        x_cat = torch.cat([x1, x2, x3, x4, x5], dim=1)
        x_out = self.FC(x_cat)
        return  x_out


class GoogleNet_4a(nn.Module):
    def __init__(self):
        super(GoogleNet_4a, self).__init__()
        # conv1
        self.conv1_7x7_s2 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3)
        self.conv1_norm1 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # conv2
        self.conv2_3x3_reduce = nn.Conv2d(64, 64, kernel_size=1, stride=1, padding=0)
        self.conv2_3x3 = nn.Conv2d(64, 192, kernel_size=3, stride=1, padding=1)
        self.conv2_norm2 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # inception_3a
        self.inception_3a_1x1 = nn.Conv2d(192, 64, kernel_size=1, stride=1, padding=0)
        self.inception_3a_3x3_reduce = nn.Conv2d(192, 96, kernel_size=1, stride=1, padding=0)
        self.inception_3a_3x3 = nn.Conv2d(96, 128, kernel_size=3, stride=1, padding=1)
        self.inception_3a_5x5_reduce = nn.Conv2d(192, 16, kernel_size=1, stride=1, padding=0)
        self.inception_3a_5x5 = nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2)
        self.inception_3a_pool_proj = nn.Conv2d(192, 32, kernel_size=1, stride=1, padding=0)
        # inception_3b
        self.inception_3b_1x1 = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0)
        self.inception_3b_3x3_reduce = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0)
        self.inception_3b_3x3 = nn.Conv2d(128, 192, kernel_size=3, stride=1, padding=1)
        self.inception_3b_5x5_reduce = nn.Conv2d(256, 32, kernel_size=1, stride=1, padding=0)
        self.inception_3b_5x5 = nn.Conv2d(32, 96, kernel_size=5, stride=1, padding=2)
        self.inception_3b_pool_proj = nn.Conv2d(256, 64, kernel_size=1, stride=1, padding=0)
        # inception_4a
        self.inception_4a_1x1 = nn.Conv2d(480, 192, kernel_size=1, stride=1, padding=0)
        self.inception_4a_3x3_reduce = nn.Conv2d(480, 96, kernel_size=1, stride=1, padding=0)
        self.inception_4a_3x3 = nn.Conv2d(96, 208, kernel_size=3, stride=1, padding=1)
        self.inception_4a_5x5_reduce = nn.Conv2d(480, 16, kernel_size=1, stride=1, padding=0)
        self.inception_4a_5x5 = nn.Conv2d(16, 48, kernel_size=5, stride=1, padding=2)
        self.inception_4a_pool_proj = nn.Conv2d(480, 64, kernel_size=1, stride=1, padding=0)

    def forward(self, x):
        # 3 x 224 x 224
        x = F.relu(self.conv1_7x7_s2(x), inplace=True)
        # 64 x 112 x 112
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=0, ceil_mode=True)
        # 64 x 56 x 56
        x = self.conv1_norm1(x)
        x = F.relu(self.conv2_3x3_reduce(x), inplace=True)
        x = F.relu(self.conv2_3x3(x), inplace=True)
        # 192 x 56 x 56
        x = self.conv2_norm2(x)
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=0, ceil_mode=True)
        # 192 x 28 x 28
        x1 = F.relu(self.inception_3a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 256 x 28 x 28
        x1 = F.relu(self.inception_3b_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3b_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3b_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3b_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 480 x 28 x 28
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=0, ceil_mode=True)
        # 480 x 14 x 14
        x1 = F.relu(self.inception_4a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_4a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_4a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_4a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 512 x 14 x 14
        return x




class GoogleNet_Dilated(nn.Module):
    def __init__(self, dilation=1):
        super(GoogleNet_Dilated, self).__init__()
        # conv1
        self.conv1_7x7_s2 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3+3*(dilation-1), dilation=dilation)
        self.conv1_norm1 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # conv2
        self.conv2_3x3_reduce = nn.Conv2d(64, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.conv2_3x3 = nn.Conv2d(64, 192, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation)
        self.conv2_norm2 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # inception_3a
        self.inception_3a_1x1 = nn.Conv2d(192, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3_reduce = nn.Conv2d(192, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3 = nn.Conv2d(96, 128, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation)
        self.inception_3a_5x5_reduce = nn.Conv2d(192, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_5x5 = nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2+2*(dilation-1), dilation=dilation)
        self.inception_3a_pool_proj = nn.Conv2d(192, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_3b
        self.inception_3b_1x1 = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3_reduce = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3 = nn.Conv2d(128, 192, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation)
        self.inception_3b_5x5_reduce = nn.Conv2d(256, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_5x5 = nn.Conv2d(32, 96, kernel_size=5, stride=1, padding=2+2*(dilation-1), dilation=dilation)
        self.inception_3b_pool_proj = nn.Conv2d(256, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_4a
        self.inception_4a_1x1 = nn.Conv2d(480, 192, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3_reduce = nn.Conv2d(480, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3 = nn.Conv2d(96, 208, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation)
        self.inception_4a_5x5_reduce = nn.Conv2d(480, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_5x5 = nn.Conv2d(16, 48, kernel_size=5, stride=1, padding=2+2*(dilation-1), dilation=dilation)
        self.inception_4a_pool_proj = nn.Conv2d(480, 64, kernel_size=1, stride=1, padding=0, dilation=1)

    def forward(self, x):
        # 3 x 224 x 224
        x = F.relu(self.conv1_7x7_s2(x), inplace=True)
        # 64 x 112 x 112
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1, ceil_mode=False)    # UCN modified
        # 64 x 56 x 56
        x = self.conv1_norm1(x)
        x = F.relu(self.conv2_3x3_reduce(x), inplace=True)
        x = F.relu(self.conv2_3x3(x), inplace=True)
        # 192 x 56 x 56
        x = self.conv2_norm2(x)
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)    # UCN modified
        # 192 x 28 x 28 / 192 x 56 x 56
        x1 = F.relu(self.inception_3a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 256 x 28 x 28 / 256 x 56 x 56
        x1 = F.relu(self.inception_3b_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3b_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3b_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3b_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 480 x 56 x 56
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)  # UCN modified
        # 480 x 56 x 56
        x1 = F.relu(self.inception_4a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_4a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_4a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_4a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 512 x 56 x 56
        return x


class GoogleNet_MultiScale_5_init2(nn.Module):
    def __init__(self):
        super(GoogleNet_MultiScale_5_init2, self).__init__()
        # conv1
        self.conv1_7x7_s2 = Conv2d_MultiScale_5_init2(3, 64, kernel_size=7, stride=2, padding=3)
        self.conv1_norm1 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # conv2
        self.conv2_3x3_reduce = nn.Conv2d(64, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.conv2_3x3 = Conv2d_MultiScale_5_init2(64, 192, kernel_size=3, stride=1, padding=1)
        self.conv2_norm2 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # inception_3a
        self.inception_3a_1x1 = nn.Conv2d(192, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3_reduce = nn.Conv2d(192, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3 = Conv2d_MultiScale_5_init2(96, 128, kernel_size=3, stride=1, padding=1)
        self.inception_3a_5x5_reduce = nn.Conv2d(192, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_5x5 = Conv2d_MultiScale_5_init2(16, 32, kernel_size=5, stride=1, padding=2)
        self.inception_3a_pool_proj = nn.Conv2d(192, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_3b
        self.inception_3b_1x1 = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3_reduce = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3 = Conv2d_MultiScale_5_init2(128, 192, kernel_size=3, stride=1, padding=1)
        self.inception_3b_5x5_reduce = nn.Conv2d(256, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_5x5 = Conv2d_MultiScale_5_init2(32, 96, kernel_size=5, stride=1, padding=2)
        self.inception_3b_pool_proj = nn.Conv2d(256, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_4a
        self.inception_4a_1x1 = nn.Conv2d(480, 192, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3_reduce = nn.Conv2d(480, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3 = Conv2d_MultiScale_5_init2(96, 208, kernel_size=3, stride=1, padding=1)
        self.inception_4a_5x5_reduce = nn.Conv2d(480, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_5x5 = Conv2d_MultiScale_5_init2(16, 48, kernel_size=5, stride=1, padding=2)
        self.inception_4a_pool_proj = nn.Conv2d(480, 64, kernel_size=1, stride=1, padding=0, dilation=1)

    def forward(self, x):
        # 3 x 224 x 224
        x = F.relu(self.conv1_7x7_s2(x), inplace=True)
        # 64 x 112 x 112
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1, ceil_mode=False)    # UCN modified
        # 64 x 56 x 56
        x = self.conv1_norm1(x)
        x = F.relu(self.conv2_3x3_reduce(x), inplace=True)
        x = F.relu(self.conv2_3x3(x), inplace=True)
        # 192 x 56 x 56
        x = self.conv2_norm2(x)
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)    # UCN modified
        # 192 x 28 x 28 / 192 x 56 x 56
        x1 = F.relu(self.inception_3a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 256 x 28 x 28 / 256 x 56 x 56
        x1 = F.relu(self.inception_3b_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3b_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3b_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3b_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 480 x 56 x 56
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)  # UCN modified
        # 480 x 56 x 56
        x1 = F.relu(self.inception_4a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_4a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_4a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_4a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 512 x 56 x 56
        return x



class GoogleNet_MultiScale_5(nn.Module):
    def __init__(self):
        super(GoogleNet_MultiScale_5, self).__init__()
        # conv1
        self.conv1_7x7_s2 = Conv2d_MultiScale_5(3, 64, kernel_size=7, stride=2, padding=3)
        self.conv1_norm1 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # conv2
        self.conv2_3x3_reduce = nn.Conv2d(64, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.conv2_3x3 = Conv2d_MultiScale_5(64, 192, kernel_size=3, stride=1, padding=1)
        self.conv2_norm2 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # inception_3a
        self.inception_3a_1x1 = nn.Conv2d(192, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3_reduce = nn.Conv2d(192, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3 = Conv2d_MultiScale_5(96, 128, kernel_size=3, stride=1, padding=1)
        self.inception_3a_5x5_reduce = nn.Conv2d(192, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_5x5 = Conv2d_MultiScale_5(16, 32, kernel_size=5, stride=1, padding=2)
        self.inception_3a_pool_proj = nn.Conv2d(192, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_3b
        self.inception_3b_1x1 = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3_reduce = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3 = Conv2d_MultiScale_5(128, 192, kernel_size=3, stride=1, padding=1)
        self.inception_3b_5x5_reduce = nn.Conv2d(256, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_5x5 = Conv2d_MultiScale_5(32, 96, kernel_size=5, stride=1, padding=2)
        self.inception_3b_pool_proj = nn.Conv2d(256, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_4a
        self.inception_4a_1x1 = nn.Conv2d(480, 192, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3_reduce = nn.Conv2d(480, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3 = Conv2d_MultiScale_5(96, 208, kernel_size=3, stride=1, padding=1)
        self.inception_4a_5x5_reduce = nn.Conv2d(480, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_5x5 = Conv2d_MultiScale_5(16, 48, kernel_size=5, stride=1, padding=2)
        self.inception_4a_pool_proj = nn.Conv2d(480, 64, kernel_size=1, stride=1, padding=0, dilation=1)

    def forward(self, x):
        # 3 x 224 x 224
        x = F.relu(self.conv1_7x7_s2(x), inplace=True)
        # 64 x 112 x 112
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1, ceil_mode=False)    # UCN modified
        # 64 x 56 x 56
        x = self.conv1_norm1(x)
        x = F.relu(self.conv2_3x3_reduce(x), inplace=True)
        x = F.relu(self.conv2_3x3(x), inplace=True)
        # 192 x 56 x 56
        x = self.conv2_norm2(x)
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)    # UCN modified
        # 192 x 28 x 28 / 192 x 56 x 56
        x1 = F.relu(self.inception_3a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 256 x 28 x 28 / 256 x 56 x 56
        x1 = F.relu(self.inception_3b_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3b_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3b_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3b_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 480 x 56 x 56
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)  # UCN modified
        # 480 x 56 x 56
        x1 = F.relu(self.inception_4a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_4a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_4a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_4a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 512 x 56 x 56
        return x



class GoogleNet_BigFilter_5(nn.Module):
    def __init__(self):
        super(GoogleNet_BigFilter_5, self).__init__()
        # conv1
        self.conv1_7x7_s2 = Conv2d_BigFilter_5(3, 64, kernel_size=7, stride=2, padding=3)
        #self.conv1_7x7_s2 = Conv2d_MultiScale_5(3, 64, kernel_size=7, stride=2, padding=3)
        self.conv1_norm1 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # conv2
        self.conv2_3x3_reduce = nn.Conv2d(64, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.conv2_3x3 = Conv2d_BigFilter_5(64, 192, kernel_size=3, stride=1, padding=1)
        self.conv2_norm2 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # inception_3a
        self.inception_3a_1x1 = nn.Conv2d(192, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3_reduce = nn.Conv2d(192, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3 = Conv2d_BigFilter_5(96, 128, kernel_size=3, stride=1, padding=1)
        self.inception_3a_5x5_reduce = nn.Conv2d(192, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_5x5 = Conv2d_BigFilter_5(16, 32, kernel_size=5, stride=1, padding=2)
        self.inception_3a_pool_proj = nn.Conv2d(192, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_3b
        self.inception_3b_1x1 = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3_reduce = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3 = Conv2d_BigFilter_5(128, 192, kernel_size=3, stride=1, padding=1)
        self.inception_3b_5x5_reduce = nn.Conv2d(256, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_5x5 = Conv2d_BigFilter_5(32, 96, kernel_size=5, stride=1, padding=2)
        self.inception_3b_pool_proj = nn.Conv2d(256, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_4a
        self.inception_4a_1x1 = nn.Conv2d(480, 192, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3_reduce = nn.Conv2d(480, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3 = Conv2d_BigFilter_5(96, 208, kernel_size=3, stride=1, padding=1)
        self.inception_4a_5x5_reduce = nn.Conv2d(480, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_5x5 = Conv2d_BigFilter_5(16, 48, kernel_size=5, stride=1, padding=2)
        self.inception_4a_pool_proj = nn.Conv2d(480, 64, kernel_size=1, stride=1, padding=0, dilation=1)

    def forward(self, x):
        # 3 x 224 x 224
        x = F.relu(self.conv1_7x7_s2(x), inplace=True)
        # 64 x 112 x 112
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1, ceil_mode=False)    # UCN modified
        # 64 x 56 x 56
        x = self.conv1_norm1(x)
        x = F.relu(self.conv2_3x3_reduce(x), inplace=True)
        x = F.relu(self.conv2_3x3(x), inplace=True)
        # 192 x 56 x 56
        x = self.conv2_norm2(x)
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)    # UCN modified
        # 192 x 28 x 28 / 192 x 56 x 56
        x1 = F.relu(self.inception_3a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 256 x 28 x 28 / 256 x 56 x 56
        x1 = F.relu(self.inception_3b_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3b_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3b_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3b_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 480 x 56 x 56
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)  # UCN modified
        # 480 x 56 x 56
        x1 = F.relu(self.inception_4a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_4a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_4a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_4a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 512 x 56 x 56
        return x


class GoogleNet_MultiScale_5_Const(nn.Module):
    def __init__(self):
        super(GoogleNet_MultiScale_5_Const, self).__init__()
        # conv1
        self.conv1_7x7_s2 = Conv2d_MultiScale_5_Const(3, 64, kernel_size=7, stride=2, padding=3)
        self.conv1_norm1 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # conv2
        self.conv2_3x3_reduce = nn.Conv2d(64, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.conv2_3x3 = Conv2d_MultiScale_5_Const(64, 192, kernel_size=3, stride=1, padding=1)
        self.conv2_norm2 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # inception_3a
        self.inception_3a_1x1 = nn.Conv2d(192, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3_reduce = nn.Conv2d(192, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3 = Conv2d_MultiScale_5_Const(96, 128, kernel_size=3, stride=1, padding=1)
        self.inception_3a_5x5_reduce = nn.Conv2d(192, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_5x5 = Conv2d_MultiScale_5_Const(16, 32, kernel_size=5, stride=1, padding=2)
        self.inception_3a_pool_proj = nn.Conv2d(192, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_3b
        self.inception_3b_1x1 = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3_reduce = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3 = Conv2d_MultiScale_5_Const(128, 192, kernel_size=3, stride=1, padding=1)
        self.inception_3b_5x5_reduce = nn.Conv2d(256, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_5x5 = Conv2d_MultiScale_5_Const(32, 96, kernel_size=5, stride=1, padding=2)
        self.inception_3b_pool_proj = nn.Conv2d(256, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_4a
        self.inception_4a_1x1 = nn.Conv2d(480, 192, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3_reduce = nn.Conv2d(480, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3 = Conv2d_MultiScale_5_Const(96, 208, kernel_size=3, stride=1, padding=1)
        self.inception_4a_5x5_reduce = nn.Conv2d(480, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_5x5 = Conv2d_MultiScale_5_Const(16, 48, kernel_size=5, stride=1, padding=2)
        self.inception_4a_pool_proj = nn.Conv2d(480, 64, kernel_size=1, stride=1, padding=0, dilation=1)

    def forward(self, x):
        # 3 x 224 x 224
        x = F.relu(self.conv1_7x7_s2(x), inplace=True)
        # 64 x 112 x 112
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1, ceil_mode=False)    # UCN modified
        # 64 x 56 x 56
        x = self.conv1_norm1(x)
        x = F.relu(self.conv2_3x3_reduce(x), inplace=True)
        x = F.relu(self.conv2_3x3(x), inplace=True)
        # 192 x 56 x 56
        x = self.conv2_norm2(x)
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)    # UCN modified
        # 192 x 28 x 28 / 192 x 56 x 56
        x1 = F.relu(self.inception_3a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 256 x 28 x 28 / 256 x 56 x 56
        x1 = F.relu(self.inception_3b_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3b_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3b_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3b_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 480 x 56 x 56
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)  # UCN modified
        # 480 x 56 x 56
        x1 = F.relu(self.inception_4a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_4a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_4a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_4a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 512 x 56 x 56
        return x


class GoogleNet_MultiScale_5_Max(nn.Module):
    def __init__(self):
        super(GoogleNet_MultiScale_5_Max, self).__init__()
        # conv1
        self.conv1_7x7_s2 = Conv2d_MultiScale_5_Max(3, 64, kernel_size=7, stride=2, padding=3)
        self.conv1_norm1 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # conv2
        self.conv2_3x3_reduce = nn.Conv2d(64, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.conv2_3x3 = Conv2d_MultiScale_5_Max(64, 192, kernel_size=3, stride=1, padding=1)
        self.conv2_norm2 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # inception_3a
        self.inception_3a_1x1 = nn.Conv2d(192, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3_reduce = nn.Conv2d(192, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3 = Conv2d_MultiScale_5_Max(96, 128, kernel_size=3, stride=1, padding=1)
        self.inception_3a_5x5_reduce = nn.Conv2d(192, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_5x5 = Conv2d_MultiScale_5_Max(16, 32, kernel_size=5, stride=1, padding=2)
        self.inception_3a_pool_proj = nn.Conv2d(192, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_3b
        self.inception_3b_1x1 = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3_reduce = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3 = Conv2d_MultiScale_5_Max(128, 192, kernel_size=3, stride=1, padding=1)
        self.inception_3b_5x5_reduce = nn.Conv2d(256, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_5x5 = Conv2d_MultiScale_5_Max(32, 96, kernel_size=5, stride=1, padding=2)
        self.inception_3b_pool_proj = nn.Conv2d(256, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_4a
        self.inception_4a_1x1 = nn.Conv2d(480, 192, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3_reduce = nn.Conv2d(480, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3 = Conv2d_MultiScale_5_Max(96, 208, kernel_size=3, stride=1, padding=1)
        self.inception_4a_5x5_reduce = nn.Conv2d(480, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_5x5 = Conv2d_MultiScale_5_Max(16, 48, kernel_size=5, stride=1, padding=2)
        self.inception_4a_pool_proj = nn.Conv2d(480, 64, kernel_size=1, stride=1, padding=0, dilation=1)

    def forward(self, x):
        # 3 x 224 x 224
        x = F.relu(self.conv1_7x7_s2(x), inplace=True)
        # 64 x 112 x 112
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1, ceil_mode=False)    # UCN modified
        # 64 x 56 x 56
        x = self.conv1_norm1(x)
        x = F.relu(self.conv2_3x3_reduce(x), inplace=True)
        x = F.relu(self.conv2_3x3(x), inplace=True)
        # 192 x 56 x 56
        x = self.conv2_norm2(x)
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)    # UCN modified
        # 192 x 28 x 28 / 192 x 56 x 56
        x1 = F.relu(self.inception_3a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 256 x 28 x 28 / 256 x 56 x 56
        x1 = F.relu(self.inception_3b_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3b_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3b_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3b_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 480 x 56 x 56
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)  # UCN modified
        # 480 x 56 x 56
        x1 = F.relu(self.inception_4a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_4a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_4a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_4a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 512 x 56 x 56
        return x


class GoogleNet_MultiScale_5_FC(nn.Module):
    def __init__(self):
        super(GoogleNet_MultiScale_5_FC, self).__init__()
        # conv1
        self.conv1_7x7_s2 = Conv2d_MultiScale_5_FC(3, 64, kernel_size=7, stride=2, padding=3)
        self.conv1_norm1 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # conv2
        self.conv2_3x3_reduce = nn.Conv2d(64, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.conv2_3x3 = Conv2d_MultiScale_5_FC(64, 192, kernel_size=3, stride=1, padding=1)
        self.conv2_norm2 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # inception_3a
        self.inception_3a_1x1 = nn.Conv2d(192, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3_reduce = nn.Conv2d(192, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3 = Conv2d_MultiScale_5_FC(96, 128, kernel_size=3, stride=1, padding=1)
        self.inception_3a_5x5_reduce = nn.Conv2d(192, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_5x5 = Conv2d_MultiScale_5_FC(16, 32, kernel_size=5, stride=1, padding=2)
        self.inception_3a_pool_proj = nn.Conv2d(192, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_3b
        self.inception_3b_1x1 = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3_reduce = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3 = Conv2d_MultiScale_5_FC(128, 192, kernel_size=3, stride=1, padding=1)
        self.inception_3b_5x5_reduce = nn.Conv2d(256, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_5x5 = Conv2d_MultiScale_5_FC(32, 96, kernel_size=5, stride=1, padding=2)
        self.inception_3b_pool_proj = nn.Conv2d(256, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_4a
        self.inception_4a_1x1 = nn.Conv2d(480, 192, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3_reduce = nn.Conv2d(480, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3 = Conv2d_MultiScale_5_FC(96, 208, kernel_size=3, stride=1, padding=1)
        self.inception_4a_5x5_reduce = nn.Conv2d(480, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_5x5 = Conv2d_MultiScale_5_FC(16, 48, kernel_size=5, stride=1, padding=2)
        self.inception_4a_pool_proj = nn.Conv2d(480, 64, kernel_size=1, stride=1, padding=0, dilation=1)

    def forward(self, x):
        # 3 x 224 x 224
        x = F.relu(self.conv1_7x7_s2(x), inplace=True)
        # 64 x 112 x 112
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1, ceil_mode=False)    # UCN modified
        # 64 x 56 x 56
        x = self.conv1_norm1(x)
        x = F.relu(self.conv2_3x3_reduce(x), inplace=True)
        x = F.relu(self.conv2_3x3(x), inplace=True)
        # 192 x 56 x 56
        x = self.conv2_norm2(x)
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)    # UCN modified
        # 192 x 28 x 28 / 192 x 56 x 56
        x1 = F.relu(self.inception_3a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 256 x 28 x 28 / 256 x 56 x 56
        x1 = F.relu(self.inception_3b_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3b_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3b_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3b_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 480 x 56 x 56
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)  # UCN modified
        # 480 x 56 x 56
        x1 = F.relu(self.inception_4a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_4a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_4a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_4a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 512 x 56 x 56
        return x



class GoogleNet_MultiScale_Attention(nn.Module):
    def __init__(self):
        super(GoogleNet_MultiScale_Attention, self).__init__()
        # conv1
        self.conv1_7x7_s2 = Conv2d_MultiScale_5(3, 64, kernel_size=7, stride=2, padding=3)
        self.conv1_norm1 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # conv2
        self.conv2_3x3_reduce = nn.Conv2d(64, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.conv2_3x3 = Conv2d_MultiScale_Attention_5(64, 192, kernel_size=3, stride=1, padding=1)
        self.conv2_norm2 = LRN(5, alpha=0.0001, beta=0.75, ACROSS_CHANNELS=True)
        # inception_3a
        self.inception_3a_1x1 = nn.Conv2d(192, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3_reduce = nn.Conv2d(192, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_3x3 = Conv2d_MultiScale_5(96, 128, kernel_size=3, stride=1, padding=1)
        self.inception_3a_5x5_reduce = nn.Conv2d(192, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3a_5x5 = Conv2d_MultiScale_5(16, 32, kernel_size=5, stride=1, padding=2)
        self.inception_3a_pool_proj = nn.Conv2d(192, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_3b
        self.inception_3b_1x1 = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3_reduce = nn.Conv2d(256, 128, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_3x3 = Conv2d_MultiScale_5(128, 192, kernel_size=3, stride=1, padding=1)
        self.inception_3b_5x5_reduce = nn.Conv2d(256, 32, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_3b_5x5 = Conv2d_MultiScale_5(32, 96, kernel_size=5, stride=1, padding=2)
        self.inception_3b_pool_proj = nn.Conv2d(256, 64, kernel_size=1, stride=1, padding=0, dilation=1)
        # inception_4a
        self.inception_4a_1x1 = nn.Conv2d(480, 192, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3_reduce = nn.Conv2d(480, 96, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_3x3 = Conv2d_MultiScale_5(96, 208, kernel_size=3, stride=1, padding=1)
        self.inception_4a_5x5_reduce = nn.Conv2d(480, 16, kernel_size=1, stride=1, padding=0, dilation=1)
        self.inception_4a_5x5 = Conv2d_MultiScale_5(16, 48, kernel_size=5, stride=1, padding=2)
        self.inception_4a_pool_proj = nn.Conv2d(480, 64, kernel_size=1, stride=1, padding=0, dilation=1)

    def forward(self, x):
        # 3 x 224 x 224
        x = F.relu(self.conv1_7x7_s2(x), inplace=True)
        # 64 x 112 x 112
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1, ceil_mode=False)    # UCN modified
        # 64 x 56 x 56
        x = self.conv1_norm1(x)
        x = F.relu(self.conv2_3x3_reduce(x), inplace=True)
        x = F.relu(self.conv2_3x3(x), inplace=True)
        # 192 x 56 x 56
        x = self.conv2_norm2(x)
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)    # UCN modified
        # 192 x 28 x 28 / 192 x 56 x 56
        x1 = F.relu(self.inception_3a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 256 x 28 x 28 / 256 x 56 x 56
        x1 = F.relu(self.inception_3b_1x1(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_3b_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_3b_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_3b_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_3b_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 480 x 56 x 56
        x = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=False)  # UCN modified
        # 480 x 56 x 56
        x1 = F.relu(self.inception_4a_1x1(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3_reduce(x), inplace=True)
        x2 = F.relu(self.inception_4a_3x3(x2), inplace=True)
        x3 = F.relu(self.inception_4a_5x5_reduce(x), inplace=True)
        x3 = F.relu(self.inception_4a_5x5(x3), inplace=True)
        x4 = F.max_pool2d(x, kernel_size=3, stride=1, padding=1, ceil_mode=True)
        x4 = F.relu(self.inception_4a_pool_proj(x4), inplace=True)
        x = torch.cat((x1, x2, x3, x4), dim=1)
        # 512 x 56 x 56
        return x




# ----------------------------------------------------------------------
## START: WEAK Align
# ----------------------------------------------------------------------

def featureL2Norm(feature):
    epsilon = 1e-6
    #        print(feature.size())
    #        print(torch.pow(torch.sum(torch.pow(feature,2),1)+epsilon,0.5).size())
    norm = torch.pow(torch.sum(torch.pow(feature,2),1)+epsilon,0.5).unsqueeze(1).expand_as(feature)
    return torch.div(feature, norm)


class FeatureCorrelation(torch.nn.Module):
    def __init__(self, shape='3D', normalization=True):
        super(FeatureCorrelation, self).__init__()
        self.normalization = normalization
        self.shape = shape
        self.ReLU = nn.ReLU()

    def forward(self, feature_A, feature_B):
        b, c, h, w = feature_A.size()
        if self.shape == '3D':
            # reshape features for matrix multiplication
            feature_A = feature_A.transpose(2, 3).contiguous().view(b, c, h * w)
            feature_B = feature_B.view(b, c, h * w).transpose(1, 2)
            # perform matrix mult.
            feature_mul = torch.bmm(feature_B, feature_A)
            # indexed [batch,idx_A=row_A+h*col_A,row_B,col_B]
            correlation_tensor = feature_mul.view(b, h, w, h * w).transpose(2, 3).transpose(1, 2)
        elif self.shape == '4D':
            # reshape features for matrix multiplication
            feature_A = feature_A.view(b, c, h * w).transpose(1, 2)  # size [b,c,h*w]
            feature_B = feature_B.view(b, c, h * w)  # size [b,c,h*w]
            # perform matrix mult.
            feature_mul = torch.bmm(feature_A, feature_B)
            # indexed [batch,row_A,col_A,row_B,col_B]
            correlation_tensor = feature_mul.view(b, h, w, h, w).unsqueeze(1)

        if self.normalization:
            correlation_tensor = featureL2Norm(self.ReLU(correlation_tensor))
            return correlation_tensor



class FeatureRegression(nn.Module):
    def __init__(self, output_dim=6, use_cuda=True, batch_normalization=True, kernel_sizes=[7,5], channels=[128,64] ,feature_size=15):
        super(FeatureRegression, self).__init__()
        num_layers = len(kernel_sizes)
        nn_modules = list()
        for i in range(num_layers):
            if i==0:
                ch_in = feature_size*feature_size
            else:
                ch_in = channels[i-1]
            ch_out = channels[i]
            k_size = kernel_sizes[i]
            nn_modules.append(nn.Conv2d(ch_in, ch_out, kernel_size=k_size, padding=0))
            if batch_normalization:
                nn_modules.append(nn.BatchNorm2d(ch_out))
            nn_modules.append(nn.ReLU(inplace=True))
        self.conv = nn.Sequential(*nn_modules)
        self.linear = nn.Linear(ch_out * k_size * k_size, output_dim)
        if use_cuda:
            self.conv.cuda()
            self.linear.cuda()

    def forward(self, x):
        x = self.conv(x)
        x = x.view(x.size(0), -1)
        x = self.linear(x)
        return x


class CNNGeometric(nn.Module):
    def __init__(self, output_dim=6,
                 feature_extraction_cnn='vgg',
                 feature_extraction_last_layer='',
                 return_correlation=False,
                 fr_feature_size=15,
                 fr_kernel_sizes=[7, 5],
                 fr_channels=[128, 64],
                 feature_self_matching=False,
                 normalize_features=True, normalize_matches=True,
                 batch_normalization=True,
                 train_fe=False, use_cuda=True):
        #                 regressor_channels_1 = 128,
        #                 regressor_channels_2 = 64):

        super(CNNGeometric, self).__init__()
        self.use_cuda = use_cuda
        self.feature_self_matching = feature_self_matching
        self.normalize_features = normalize_features
        self.normalize_matches = normalize_matches
        self.return_correlation = return_correlation
        self.FeatureExtraction = FeatureExtraction(train_fe=train_fe,
                                                   feature_extraction_cnn=feature_extraction_cnn,
                                                   last_layer=feature_extraction_last_layer,
                                                   normalization=normalize_features,
                                                   use_cuda=self.use_cuda)

        self.FeatureCorrelation = FeatureCorrelation(shape='3D', normalization=normalize_matches)

        self.FeatureRegression = FeatureRegression(output_dim,
                                                   use_cuda=self.use_cuda,
                                                   feature_size=fr_feature_size,
                                                   kernel_sizes=fr_kernel_sizes,
                                                   channels=fr_channels,
                                                   batch_normalization=batch_normalization)

        self.ReLU = nn.ReLU(inplace=True)

    # used only for foward pass at eval and for training with strong supervision
    def forward(self, tnf_batch):
        # feature extraction
        feature_A = self.FeatureExtraction(tnf_batch['source_image'])
        feature_B = self.FeatureExtraction(tnf_batch['target_image'])
        # feature correlation
        correlation = self.FeatureCorrelation(feature_A, feature_B)
        # regression to tnf parameters theta
        theta = self.FeatureRegression(correlation)

        if self.return_correlation:
            return (theta, correlation)
        else:
            return theta

# ----------------------------------------------------------------------
## END: WEAK Align
# ----------------------------------------------------------------------

