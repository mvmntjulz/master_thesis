from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F
import math




class Conv2d_MultiScale_5_Shared(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        super().__init__()
        assert kernel_size % 2 == 1

        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding

        self.conv2d_weights = nn.Parameter(torch.Tensor(out_channels, in_channels, kernel_size, kernel_size))
        self.conv2d_bias = nn.Parameter(torch.Tensor(out_channels))
        self.scale_W = nn.Parameter(torch.zeros(5))

        self.reset_parameters()


    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.conv2d_weights, a=math.sqrt(5))
        fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.conv2d_weights)
        bound = 1 / math.sqrt(fan_in)
        nn.init.uniform_(self.conv2d_bias, -bound, bound)


    def forward(self, x):
        x1 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=self.padding, dilation=1, groups=1
        )
        x2 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2), dilation=2, groups=1
        )
        x3 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=self.padding + self.kernel_size - 1, dilation=3, groups=1
        )
        x4 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2 * 3), dilation=4, groups=1
        )
        x5 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2 * 4), dilation=5, groups=1
        )
        weight = F.softmax(self.scale_W, dim=0)
        return x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]



class Conv2d_MultiScale_5_Shared_Field(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, w_field_size=224):
        super().__init__()
        assert kernel_size % 2 == 1

        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding

        self.conv2d_weights = nn.Parameter(torch.Tensor(out_channels, in_channels, kernel_size, kernel_size))
        self.conv2d_bias = nn.Parameter(torch.Tensor(out_channels))
        self.scale_W = nn.Parameter(torch.zeros(5, 1, w_field_size, w_field_size))
        #self.scale_W = nn.Parameter(torch.zeros(5))
        self.reset_parameters()


    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.conv2d_weights, a=math.sqrt(5))
        fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.conv2d_weights)
        bound = 1 / math.sqrt(fan_in)
        nn.init.uniform_(self.conv2d_bias, -bound, bound)


    def forward(self, x):
        x1 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=self.padding, dilation=1, groups=1
        )
        x2 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2), dilation=2, groups=1
        )
        x3 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=self.padding + self.kernel_size - 1, dilation=3, groups=1
        )
        x4 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2 * 3), dilation=4, groups=1
        )
        x5 = F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2 * 4), dilation=5, groups=1
        )
        weight = F.softmax(self.scale_W, dim=0)

        return x1 * weight[0, :, :, :] + x2 * weight[1, :, :, :] + x3 * weight[2, :, :, :] + x4 * weight[3, :, :, :] + x5 * weight[4, :, :, :]


def VGG16_Conv4_MS5_Shared():
    model = nn.Sequential(
        Conv2d_MultiScale_5_Shared(3, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(64, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5_Shared(64, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(128, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5_Shared(128, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=1, dilation=1),    # modified
        Conv2d_MultiScale_5_Shared(256, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True)
    )
    return model



def VGG16_Conv4_MS5_Shared_correct():
    model = nn.Sequential(
        Conv2d_MultiScale_5_Shared(3, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(64, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5_Shared(64, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(128, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5_Shared(128, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=3, stride=1, dilation=1, padding=1),    # modified v2 (correct)
        Conv2d_MultiScale_5_Shared(256, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True)
    )
    return model


def VGG16_Conv4_MS5_Shared_Field():
    model = nn.Sequential(
        Conv2d_MultiScale_5_Shared_Field(3, 64, kernel_size=3, stride=1, padding=1, w_field_size=224),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared_Field(64, 64, kernel_size=3, stride=1, padding=1, w_field_size=224),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5_Shared_Field(64, 128, kernel_size=3, stride=1, padding=1, w_field_size=112),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared_Field(128, 128, kernel_size=3, stride=1, padding=1, w_field_size=112),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5_Shared_Field(128, 256, kernel_size=3, stride=1, padding=1, w_field_size=56),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared_Field(256, 256, kernel_size=3, stride=1, padding=1, w_field_size=56),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared_Field(256, 256, kernel_size=3, stride=1, padding=1, w_field_size=56),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=3, stride=1, dilation=1, padding=1),    # modified v2 (correct)
        Conv2d_MultiScale_5_Shared_Field(256, 512, kernel_size=3, stride=1, padding=1, w_field_size=56),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared_Field(512, 512, kernel_size=3, stride=1, padding=1, w_field_size=56),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared_Field(512, 512, kernel_size=3, stride=1, padding=1, w_field_size=56),
        nn.ReLU(inplace=True)
    )
    return model



# - not working requires >12GB memory
def VGG16_Conv4_MS5_Shared_stride16():
    model = nn.Sequential(
        Conv2d_MultiScale_5_Shared(3, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(64, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5_Shared(64, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(128, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5_Shared(128, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=1),    # modified v2 (correct)
        Conv2d_MultiScale_5_Shared(256, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True)
    )
    return model



def VGG16_Conv4_Relu_MS5_Shared_correct():
    model = nn.Sequential(
        Conv2d_Relu_MultiScale_5_Shared(3, 64, kernel_size=3, stride=1, padding=1),
        Conv2d_Relu_MultiScale_5_Shared(64, 64, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_Relu_MultiScale_5_Shared(64, 128, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        Conv2d_Relu_MultiScale_5_Shared(128, 128, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_Relu_MultiScale_5_Shared(128, 256, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        Conv2d_Relu_MultiScale_5_Shared(256, 256, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        Conv2d_Relu_MultiScale_5_Shared(256, 256, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=3, stride=1, dilation=1, padding=1),    # modified v2 (correct)
        Conv2d_Relu_MultiScale_5_Shared(256, 512, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        Conv2d_Relu_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        Conv2d_Relu_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1)
        #nn.ReLU(inplace=True)
    )
    return model



# ---------------- SHARED + RELU + POOL --------------------


class Conv2d_Relu_MultiScale_5_Shared(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        super().__init__()
        assert kernel_size % 2 == 1

        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding

        self.conv2d_weights = nn.Parameter(torch.Tensor(out_channels, in_channels, kernel_size, kernel_size))
        self.conv2d_bias = nn.Parameter(torch.Tensor(out_channels))
        self.scale_W = nn.Parameter(torch.zeros(5))

        self.reset_parameters()


    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.conv2d_weights, a=math.sqrt(5))
        fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.conv2d_weights)
        bound = 1 / math.sqrt(fan_in)
        nn.init.uniform_(self.conv2d_bias, -bound, bound)


    def forward(self, x):
        x1 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=self.padding, dilation=1, groups=1
        ), inplace=True)
        x2 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2), dilation=2, groups=1
        ), inplace=True)
        x3 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=self.padding + self.kernel_size - 1, dilation=3, groups=1
        ), inplace=True)
        x4 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2 * 3), dilation=4, groups=1
        ), inplace=True)
        x5 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2 * 4), dilation=5, groups=1
        ), inplace=True)

        weight = F.softmax(self.scale_W, dim=0)
        return x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]


class Conv2d_MultiScale_Pool_5_Shared(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, groups=1, pool_kernel_size=1, pool_stride=1, pool_padding=0):

        super(Conv2d_MultiScale_Pool_5_Shared, self).__init__()
        assert kernel_size % 2 == 1

        self.conv2d_weights = nn.Parameter(torch.Tensor(out_channels, in_channels, kernel_size, kernel_size))
        self.conv2d_bias = nn.Parameter(torch.Tensor(out_channels))

        self.pool_kernel_size =pool_kernel_size
        self.pool_stride =pool_stride
        self.pool_padding =pool_padding
        self.scale_W = nn.Parameter(torch.zeros(5))

        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding



    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.conv2d_weights, a=math.sqrt(5))
        fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.conv2d_weights)
        bound = 1 / math.sqrt(fan_in)
        nn.init.uniform_(self.conv2d_bias, -bound, bound)


    def forward(self, x):

        x1 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=self.padding, dilation=1, groups=1
        ), inplace=True)
        x1 = F.max_pool2d(x1, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)

        x2 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2), dilation=2, groups=1
        ), inplace=True)
        x2 = F.max_pool2d(x2, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)

        x3 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=self.padding + self.kernel_size - 1, dilation=3, groups=1
        ), inplace=True)
        x3 = F.max_pool2d(x3, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)

        x4 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2 * 3), dilation=4, groups=1
        ), inplace=True)
        x4 = F.max_pool2d(x4, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)

        x5 = F.relu(F.conv2d(
            x, self.conv2d_weights,
            bias=self.conv2d_bias, stride=self.stride, padding=int(self.padding + (self.kernel_size - 1) / 2 * 4), dilation=5, groups=1
        ), inplace=True)
        x5 = F.max_pool2d(x5, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)

        weight = F.softmax(self.scale_W, dim=0)

        return x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]


def VGG16_Conv4_MS5_shared_combine8():
    model = nn.Sequential(
        Conv2d_Relu_MultiScale_5_Shared(3, 64, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        #Conv2d_Relu_MultiScale_5(64, 64, kernel_size=3, stride=1, padding=1),
        Conv2d_MultiScale_Pool_5_Shared(64, 64, kernel_size=3, stride=1, padding=1, pool_kernel_size=2, pool_stride=2, pool_padding=0),
        #nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_Relu_MultiScale_5_Shared(64, 128, kernel_size=3, stride=1, padding=1),
        #Conv2d_MultiScale_Pool_5(64, 128, kernel_size=3, stride=1, padding=1), <-- error
        #nn.ReLU(inplace=True),
        #Conv2d_Relu_MultiScale_5(128, 128, kernel_size=3, stride=1, padding=1),
        Conv2d_MultiScale_Pool_5_Shared(128, 128, kernel_size=3, stride=1, padding=1, pool_kernel_size=2, pool_stride=2, pool_padding=0),
        #nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_Relu_MultiScale_5_Shared(128, 256, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        Conv2d_Relu_MultiScale_5_Shared(256, 256, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        #Conv2d_Relu_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        Conv2d_MultiScale_Pool_5_Shared(256, 256, kernel_size=3, stride=1, padding=1, pool_kernel_size=3, pool_stride=2, pool_padding=0),
        #nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=0),
        Conv2d_Relu_MultiScale_5_Shared(256, 512, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        Conv2d_Relu_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        Conv2d_Relu_MultiScale_5_Shared(512, 512, kernel_size=3, stride=1, padding=1)
        #nn.ReLU(inplace=True)
    )
    return model





# class Conv2d_MultiScale_5(nn.Module):
#     def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, groups=1):
#         super(Conv2d_MultiScale_5, self).__init__()
#         assert kernel_size % 2 == 1
#         self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
#                                        stride=stride, padding=padding, dilation=1, groups=groups)
#         self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
#                                        stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2, groups=groups)
#         self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
#                                        stride=stride, padding=padding + (kernel_size - 1), dilation=3, groups=groups)
#         self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
#                                        stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4, groups=groups)
#         self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
#                                        stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5, groups=groups)
#         self.scale_W = nn.Parameter(torch.zeros(5))
#
#     def forward(self, x):
#         x1 = self.Conv2d_Scale1(x)
#         x2 = self.Conv2d_Scale2(x)
#         x3 = self.Conv2d_Scale3(x)
#         x4 = self.Conv2d_Scale4(x)
#         x5 = self.Conv2d_Scale5(x)
#         weight = F.softmax(self.scale_W, dim=0)
#         return  x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]
#
#
#
#
# class vgg16_block5(nn.Module):
#   # ---
#   def __init__(self):
#     super(vgg16_adjust, self).__init__()
#     self.features = nn.Sequential(
#       #nn.Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(3, 64, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(64, 64, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #
#       #nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
#       nn.MaxPool2d(kernel_size=2, stride=2, padding=1, dilation=1, ceil_mode=False),
#       #nn.Conv2d(64, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(64, 128, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(128, 128, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #
#       #nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
#       nn.MaxPool2d(kernel_size=2, stride=2, padding=1, dilation=1, ceil_mode=False),
#       #nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(128, 256, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #
#       #nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
#       nn.MaxPool2d(kernel_size=2, stride=2, padding=1, dilation=1, ceil_mode=False),
#       #nn.Conv2d(256, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(256, 512, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #
#       #nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
#       nn.MaxPool2d(kernel_size=2, stride=2, padding=1, dilation=1, ceil_mode=False),
#       #nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#       Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
#       nn.ReLU(inplace=True),
#       #nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
#     )
#   def forward(self, x):
#     return self.features(x)
#
#
#
#
# # ----------------------------------------------------------------------
# # OLD VERSIONS
# # ----------------------------------------------------------------------
#
# def VGG16_Block4():
#     model = nn.Sequential(
#         nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
#         nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=0),   # NON
#         nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
#         nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=0),  # NON
#         nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
#         #nn.MaxPool2d(kernel_size=3, stride=1, dilation=1, padding=1),  # NON
#         nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True)
#     )
#     return model
#
#
# def VGG16_Dilated(dilation=1):
#     model = nn.Sequential(
#         nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True),
#         nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
#         nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True),
#         nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
#         nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True),
#         #nn.MaxPool2d(kernel_size=2, stride=1, dilation=1),    # modified
#         nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True),
#         nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
#         nn.ReLU(inplace=True)
#     )
#     return model
#
#
# def VGG16_MultiScale_5():
#     model = nn.Sequential(
#         Conv2d_MultiScale_5(3, 64, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         Conv2d_MultiScale_5(64, 64, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
#         Conv2d_MultiScale_5(64, 128, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         Conv2d_MultiScale_5(128, 128, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
#         Conv2d_MultiScale_5(128, 256, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         nn.MaxPool2d(kernel_size=2, stride=1, dilation=1),    # modified
#         Conv2d_MultiScale_5(256, 512, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True),
#         Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
#         nn.ReLU(inplace=True)
#     )
#     return model
#
#
#
#
#
#
#
#
#
#

