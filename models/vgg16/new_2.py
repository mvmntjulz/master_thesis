def VGG16_Conv4_S16_Dilated(dilation=1):
   model = nn.Sequential(
       nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, ceil_mode=False),
       nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, ceil_mode=False),
       nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=0, ceil_mode=False),
       nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
   )
   return model
