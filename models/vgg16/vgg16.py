from __future__ import division
import torch
import torch.nn as nn
import torch.nn.functional as F


class Conv2d_MultiScale_5(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, groups=1):
        super(Conv2d_MultiScale_5, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1, groups=groups)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2, groups=groups)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3, groups=groups)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4, groups=groups)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5, groups=groups)
        self.scale_W = nn.Parameter(torch.zeros(5))

    def forward(self, x):
        x1 = self.Conv2d_Scale1(x)
        x2 = self.Conv2d_Scale2(x)
        x3 = self.Conv2d_Scale3(x)
        x4 = self.Conv2d_Scale4(x)
        x5 = self.Conv2d_Scale5(x)
        weight = F.softmax(self.scale_W, dim=0)
        return  x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]



class Conv2d_Relu_MultiScale_5(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, groups=1):
        super(Conv2d_Relu_MultiScale_5, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1, groups=groups)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2, groups=groups)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3, groups=groups)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4, groups=groups)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5, groups=groups)
        self.scale_W = nn.Parameter(torch.zeros(5))

    def forward(self, x):
        x1 = F.relu(self.Conv2d_Scale1(x), inplace=True)
        x2 = F.relu(self.Conv2d_Scale2(x), inplace=True)
        x3 = F.relu(self.Conv2d_Scale3(x), inplace=True)
        x4 = F.relu(self.Conv2d_Scale4(x), inplace=True)
        x5 = F.relu(self.Conv2d_Scale5(x), inplace=True)
        weight = F.softmax(self.scale_W, dim=0)
        return x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]


class Conv2d_Relu_MultiScale_1(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, groups=1):
        super(Conv2d_Relu_MultiScale_1, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1, groups=groups)
    def forward(self, x):
        return F.relu(self.Conv2d_Scale1(x), inplace=True)


class Conv2d_LeakyRelu_MultiScale_5(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, groups=1):
        super(Conv2d_LeakyRelu_MultiScale_5, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1, groups=groups)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2, groups=groups)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3, groups=groups)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4, groups=groups)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5, groups=groups)
        self.scale_W = nn.Parameter(torch.zeros(5))

    def forward(self, x):
        x1 = F.leaky_relu(self.Conv2d_Scale1(x), inplace=True)
        x2 = F.leaky_relu(self.Conv2d_Scale2(x), inplace=True)
        x3 = F.leaky_relu(self.Conv2d_Scale3(x), inplace=True)
        x4 = F.leaky_relu(self.Conv2d_Scale4(x), inplace=True)
        x5 = F.leaky_relu(self.Conv2d_Scale5(x), inplace=True)
        weight = F.softmax(self.scale_W, dim=0)
        return  x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]



class Conv2d_MultiScale_Pool_5(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, groups=1, pool_kernel_size=1, pool_stride=1, pool_padding=0):
        super(Conv2d_MultiScale_Pool_5, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1, groups=groups)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2, groups=groups)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3, groups=groups)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4, groups=groups)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5, groups=groups)

        self.pool_kernel_size =pool_kernel_size
        self.pool_stride =pool_stride
        self.pool_padding =pool_padding
        self.scale_W = nn.Parameter(torch.zeros(5))

    def forward(self, x):

        x1 = self.Conv2d_Scale1(x)
        x1 = F.max_pool2d(x1, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)
        x2 = self.Conv2d_Scale2(x)
        x2 = F.relu(x2, inplace=True)
        x2 = F.max_pool2d(x2, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)
        x3 = self.Conv2d_Scale3(x)
        x3 = F.relu(x3, inplace=True)
        x3 = F.max_pool2d(x3, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)
        x4 = self.Conv2d_Scale4(x)
        x4 = F.relu(x4, inplace=True)
        x4 = F.max_pool2d(x4, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)
        x5 = self.Conv2d_Scale5(x)
        x5 = F.relu(x5, inplace=True)
        x5 = F.max_pool2d(x5, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True)
        weight = F.softmax(self.scale_W, dim=0)
        return x1 * weight[0] + x2 * weight[1] + x3 * weight[2] + x4 * weight[3] + x5 * weight[4]



class Conv2d_MultiScale_Pool_1(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, groups=1, pool_kernel_size=1, pool_stride=1, pool_padding=0):
        super(Conv2d_MultiScale_Pool_1, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1, groups=groups)
        self.pool_kernel_size =pool_kernel_size
        self.pool_stride =pool_stride
        self.pool_padding =pool_padding
        self.scale_W = nn.Parameter(torch.zeros(5))

    def forward(self, x):
        return self.Conv2d_Scale1(F.max_pool2d(x, kernel_size=self.pool_kernel_size, stride=self.pool_stride, padding=self.pool_padding, dilation=1, ceil_mode=True))




class Conv2d_MultiScale_5_Const(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, groups=1):
        super(Conv2d_MultiScale_5_Const, self).__init__()
        assert kernel_size % 2 == 1
        self.Conv2d_Scale1 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding, dilation=1, groups=groups)
        self.Conv2d_Scale2 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2), dilation=2, groups=groups)
        self.Conv2d_Scale3 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=padding + (kernel_size - 1), dilation=3, groups=groups)
        self.Conv2d_Scale4 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 3), dilation=4, groups=groups)
        self.Conv2d_Scale5 = nn.Conv2d(in_channels, out_channels, kernel_size,
                                       stride=stride, padding=int(padding + (kernel_size - 1) / 2 * 4), dilation=5, groups=groups)

    def forward(self, x):
        x1 = self.Conv2d_Scale1(x)
        x2 = self.Conv2d_Scale2(x)
        x3 = self.Conv2d_Scale3(x)
        x4 = self.Conv2d_Scale4(x)
        x5 = self.Conv2d_Scale5(x)
        return  0.2 * (x1 + x2 + x3 + x4 + x5)


def VGG16_Block4():
    model = nn.Sequential(
        nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=0),   # NON
        nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=0),  # NON
        nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        nn.MaxPool2d(kernel_size=3, stride=1, dilation=1, padding=1),  # NON
        nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True)
    )
    return model


def VGG16_Block4_mod():
    model = nn.Sequential(
        nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=0),   # NON
        nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=0),  # NON
        nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        nn.MaxPool2d(kernel_size=3, stride=2, dilation=1, padding=1),  # NON
        nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True)
    )
    return model



def VGG16_Dilated(dilation=1):
    model = nn.Sequential(
        nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True),
        nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True),
        nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True),
        nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True),
        nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=1, dilation=1),    # modified
        nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True),
        nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True),
        nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
        nn.ReLU(inplace=True)
    )
    return model


def VGG16_Conv4_MultiScale5():
    model = nn.Sequential(
        Conv2d_MultiScale_5(3, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(64, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5(64, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(128, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5(128, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=1, dilation=1),    # modified
        Conv2d_MultiScale_5(256, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True)
    )
    return model

def VGG16_Conv4_Relu_MultiScale5():
    model = nn.Sequential(
        Conv2d_Relu_MultiScale_5(3, 64, kernel_size=3, stride=1, padding=1),
        Conv2d_Relu_MultiScale_5(64, 64, kernel_size=3, stride=1, padding=1),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_Relu_MultiScale_5(64, 128, kernel_size=3, stride=1, padding=1),
        Conv2d_Relu_MultiScale_5(128, 128, kernel_size=3, stride=1, padding=1),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_Relu_MultiScale_5(128, 256, kernel_size=3, stride=1, padding=1),
        Conv2d_Relu_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        Conv2d_Relu_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        nn.MaxPool2d(kernel_size=3, stride=1, dilation=1, padding=1),    # modified v2
        Conv2d_Relu_MultiScale_5(256, 512, kernel_size=3, stride=1, padding=1),
        Conv2d_Relu_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        Conv2d_Relu_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1)
    )
    return model

def VGG16_Conv4_LeakyRelu_MultiScale5():
    model = nn.Sequential(
        Conv2d_LeakyRelu_MultiScale_5(3, 64, kernel_size=3, stride=1, padding=1),
        Conv2d_LeakyRelu_MultiScale_5(64, 64, kernel_size=3, stride=1, padding=1),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_LeakyRelu_MultiScale_5(64, 128, kernel_size=3, stride=1, padding=1),
        Conv2d_LeakyRelu_MultiScale_5(128, 128, kernel_size=3, stride=1, padding=1),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_LeakyRelu_MultiScale_5(128, 256, kernel_size=3, stride=1, padding=1),
        Conv2d_LeakyRelu_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        Conv2d_LeakyRelu_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        nn.MaxPool2d(kernel_size=3, stride=1, dilation=1, padding=1),    # modified v2
        Conv2d_LeakyRelu_MultiScale_5(256, 512, kernel_size=3, stride=1, padding=1),
        Conv2d_LeakyRelu_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        Conv2d_LeakyRelu_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1)
    )
    return model


def VGG16_Conv4_MultiScalePool5():
    model = nn.Sequential(
        Conv2d_MultiScale_5(3, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        #Conv2d_MultiScale_5(64, 64, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_Pool_5(64, 64, kernel_size=3, stride=1, padding=1, pool_kernel_size=2, pool_stride=2, pool_padding=0),
        Conv2d_MultiScale_5(64, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        #Conv2d_MultiScale_5(128, 128, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_Pool_5(128, 128, kernel_size=3, stride=1, padding=1, pool_kernel_size=2, pool_stride=2, pool_padding=0),
        Conv2d_MultiScale_5(128, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        #Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        #nn.ReLU(inplace=True),
        #nn.MaxPool2d(kernel_size=2, stride=1, dilation=1),    # modified
        Conv2d_MultiScale_Pool_5(256, 256, kernel_size=3, stride=1, padding=1, pool_kernel_size=3, pool_stride=1, pool_padding=1),
        Conv2d_MultiScale_5(256, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True)
    )
    return model



def VGG16_Conv5_MultiScale5():
    model = nn.Sequential(
        Conv2d_MultiScale_5(3, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(64, 64, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5(64, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(128, 128, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=2, dilation=1),
        Conv2d_MultiScale_5(128, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(256, 256, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=1, dilation=1),    # modified
        Conv2d_MultiScale_5(256, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=2, stride=1, dilation=1),    # modified  stride 2 -> 1
        Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True),
        Conv2d_MultiScale_5(512, 512, kernel_size=3, stride=1, padding=1),
        nn.ReLU(inplace=True)
    )
    return model


def VGG16_Conv4_S16_Dilated(dilation=1):
   model = nn.Sequential(
       nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, ceil_mode=False),
       nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, ceil_mode=False),
       nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.MaxPool2d(kernel_size=2, stride=2, dilation=1, padding=0, ceil_mode=False),
       nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1+(dilation-1), dilation=dilation),
       nn.ReLU(inplace=True),
       nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
   )
   return model
