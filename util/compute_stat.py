from __future__ import division
import numpy as np

def mean_diagonal(dim):
    ## dim: [im1_h, im1_w, im2_h, im2_w]

    L = 0.5 * np.sqrt(np.sum(dim[:,:2] ** 2, axis=1)) + 0.5 * np.sqrt(np.sum(dim[:,2:] ** 2, axis=1))
    return L


def PCK(pred, target, pred_vis, target_vis, L):
    ## Compute Percentage of Correct Keypoints
    # pred: predicted annotations of Im1 in Im2
    # target: ground true annotations in Im2
    # pred_vis, pred_vis: annotations is visible (1) or not (0)
    # L: mean diagonal of Im1 & Im2

    assert pred.shape==target.shape
    [nb_pairs, nb_anno] = pred.shape
    nb_anno = nb_anno // 2
    distance = np.reshape(((pred - target) ** 2) * pred_vis * target_vis, [nb_pairs, 2, nb_anno])
    distance = np.sqrt(np.sum(distance, axis=1))  # L2 distance
    distance = distance / L[:, None]  # normalized by the mean diagonal

    pck = np.zeros([101, 2])
    pck[:, 0] = np.arange(101) / 100
    visible = pred_vis[:, :nb_anno] * target_vis[:, :nb_anno]
    for ii in range(101):
        alpha = ii / 100
        correct = (distance <= alpha) * visible
        pck[ii, 1] = np.mean(np.sum(correct, 1) / np.sum(visible, 1))
        # pck[ii, 1] = np.average(np.sum(correct, 1) / np.sum(visible, 1), weights=np.sum(visible, 1))

    return pck


def get_LT_ACC_IoU(mask1, mask2, vx, vy):
    # this function copy the label from mask2 to mask 1
    [h1, w1] = mask1.shape[:2]
    [h2, w2] = mask2.shape[:2]
    mask1_pred = np.zeros_like(mask1, dtype='uint8')

    [x1, y1] = np.meshgrid(range(w1), range(h1))
    x2 = x1 + vx.astype(int)
    y2 = y1 + vy.astype(int)
    x1, y1 = x1.flatten(), y1.flatten()
    x2, y2 = x2.flatten(), y2.flatten()
    in_bound = (x2 >= 0) & (y2 >= 0) & (x2 < w2) & (y2 < h2)

    mask1_pred[y1[in_bound], x1[in_bound]] = mask2[y2[in_bound], x2[in_bound]]

    LT_ACC = np.mean(mask1==mask1_pred)
    I = np.sum((mask1==1) & (mask1_pred==1))
    U = np.sum((mask1==1) | (mask1_pred==1))
    IoU = I / U
    return LT_ACC, IoU


def localization_error(source_mask_np, target_mask_np, flow_np):
    h_tgt, w_tgt = target_mask_np.shape[0], target_mask_np.shape[1]
    h_src, w_src = source_mask_np.shape[0], source_mask_np.shape[1]

    # initial pixel positions x1,y1 in target image
    x1, y1 = np.meshgrid(range(1, w_tgt + 1), range(1, h_tgt + 1))
    # sampling pixel positions x2,y2
    x2 = x1 + flow_np[:, :, 0]
    y2 = y1 + flow_np[:, :, 1]

    # compute in-bound coords for each image
    in_bound = (x2 >= 1) & (x2 <= w_src) & (y2 >= 1) & (y2 <= h_src)
    row, col = np.where(in_bound)
    row_1 = y1[row, col].flatten().astype(np.int) - 1
    col_1 = x1[row, col].flatten().astype(np.int) - 1
    row_2 = y2[row, col].flatten().astype(np.int) - 1
    col_2 = x2[row, col].flatten().astype(np.int) - 1

    # compute relative positions
    target_loc_x, target_loc_y = obj_ptr(target_mask_np)
    source_loc_x, source_loc_y = obj_ptr(source_mask_np)
    x1_rel = target_loc_x[row_1, col_1]
    y1_rel = target_loc_y[row_1, col_1]
    x2_rel = source_loc_x[row_2, col_2]
    y2_rel = source_loc_y[row_2, col_2]

    # compute localization error
    loc_err = np.mean(np.abs(x1_rel - x2_rel) + np.abs(y1_rel - y2_rel))

    return loc_err


def obj_ptr(mask):
    # computes images of normalized coordinates around bounding box
    # kept function name from DSP code
    h, w = mask.shape[0], mask.shape[1]
    y, x = np.where(mask > 0.5)
    left = np.min(x);
    right = np.max(x);
    top = np.min(y);
    bottom = np.max(y);
    fg_width = right - left + 1;
    fg_height = bottom - top + 1;
    x_image, y_image = np.meshgrid(range(1, w + 1), range(1, h + 1));
    x_image = (x_image - left) / fg_width;
    y_image = (y_image - top) / fg_height;
    return (x_image, y_image)
