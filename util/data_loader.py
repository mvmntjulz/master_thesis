from __future__ import division
import numpy as np
from scipy.misc import imread
import os
import scipy.io as sio
from PIL import Image, ImageDraw
from bs4 import BeautifulSoup


def load_pascal_parts(dir_data):
    data = {'images':[], 'part_x': [], 'part_y': [], 'keypts_status': [], 'dim': [], 'img_cl': [], 'name': [],
            'mask': [], 'part_name': [], 'part_mask': []}
    for curr_folder in np.sort(os.listdir(dir_data)):
        for curr_file in np.sort(os.listdir('%s/%s' % (dir_data, curr_folder))):
            if curr_file.endswith('.jpg'):
                image = imread('%s/%s/%s' % (dir_data, curr_folder, curr_file), mode='RGB')
                keypts = sio.loadmat('%s/%s/%s' % (dir_data, curr_folder, curr_file.replace('.jpg', '.mat')))
                if len(keypts['keypts_status'])==0:
                    continue
                data['dim'].append(image.shape[:2])
                data['img_cl'].append(curr_folder)
                data['name'].append('%s/%s' % (curr_folder, curr_file))
                data['images'].append(image)
                data['part_x'].append(keypts['keypts'][0, :])
                data['part_y'].append(keypts['keypts'][1, :])
                data['keypts_status'].append(keypts['keypts_status'].squeeze())
                data['mask'].append(keypts['mask'])
                nb_part = keypts['parts']['part_name'].size
                data['part_name'].append(np.array([keypts['parts']['part_name'][0, ii] for ii in range(nb_part)], dtype='<U64'))
                data['part_mask'].append(np.array([keypts['parts']['mask'][0, ii] for ii in range(nb_part)]))
    data['dim'] = np.asarray(data['dim'])
    data['img_cl'] = np.asarray(data['img_cl'], dtype='<U32')
    data['name'] = np.asarray(data['name'], dtype='<U64')
    return data


def contour2mask(contour, height, width):
    # contour = [(x1,y1),(x2,y2),...] or [x1,y1,x2,y2,...]
    # contour must be a list, not ndarray
    img = Image.new('1', (width, height), color=0)
    ImageDraw.Draw(img).polygon(contour, outline=1, fill=1)
    return np.array(img, dtype='uint8')


def load_TSS(dir_data, debug=0):
    data = {'images': [], 'flow': [], 'img_cl': [], 'name': [], 'mask':[], 'flip':[]}
    for curr_class in os.listdir(dir_data):
        if (curr_class.endswith('.txt') or curr_class.endswith('.csv')):
            continue
        else:
            for ii, curr_pair in enumerate(os.listdir('%s/%s' % (dir_data, curr_class))):
                if debug & (ii > 1):
                    continue
                image1 = imread('%s/%s/%s/image1.png' % (dir_data, curr_class, curr_pair), mode='RGB')
                image2 = imread('%s/%s/%s/image2.png' % (dir_data, curr_class, curr_pair), mode='RGB')
                flip = np.genfromtxt('%s/%s/%s/flip_gt.txt' % (dir_data, curr_class, curr_pair), dtype=int)
                mask1 = imread('%s/%s/%s/mask1.png' % (dir_data, curr_class, curr_pair), mode='L')
                mask2 = imread('%s/%s/%s/mask2.png' % (dir_data, curr_class, curr_pair), mode='L')
                flow1 = read_flo('%s/%s/%s/flow1.flo' % (dir_data, curr_class, curr_pair))
                flow2 = read_flo('%s/%s/%s/flow2.flo' % (dir_data, curr_class, curr_pair))
                data['images'].append([image1, image2])
                data['mask'].append([mask1, mask2])
                data['flow'].append([flow1, flow2])
                data['img_cl'].append(curr_class)
                # if curr_class=='JODS':
                #     data['img_cl'].append(curr_pair.split('_')[0])
                # else:
                #     data['img_cl'].append(curr_class)
                data['name'].append(curr_pair)
                data['flip'].append(flip)
    data['flip'] = np.asarray(data['flip'])
    data['img_cl'] = np.asarray(data['img_cl'], dtype='<U64')
    data['name'] = np.asarray(data['name'], dtype='<U64')
    return data


def load_TSS_jpg(dir_data, debug=0):
    data = {'images': [], 'flow': [], 'img_cl': [], 'name': [], 'mask':[], 'flip':[]}
    for curr_class in os.listdir(dir_data):
        if (curr_class.endswith('.txt') or curr_class.endswith('.csv')):
            continue
        else:
            for ii, curr_pair in enumerate(os.listdir('%s/%s' % (dir_data, curr_class))):
                if debug & (ii > 1):
                    continue
                image1 = imread('%s/%s/%s/image1.jpg' % (dir_data, curr_class, curr_pair), mode='RGB')
                image2 = imread('%s/%s/%s/image2.jpg' % (dir_data, curr_class, curr_pair), mode='RGB')
                flip = np.genfromtxt('%s/%s/%s/flip_gt.txt' % (dir_data, curr_class, curr_pair), dtype=int)
                mask1 = imread('%s/%s/%s/mask1.png' % (dir_data, curr_class, curr_pair), mode='L')
                mask2 = imread('%s/%s/%s/mask2.png' % (dir_data, curr_class, curr_pair), mode='L')
                flow1 = read_flo('%s/%s/%s/flow1.flo' % (dir_data, curr_class, curr_pair))
                flow2 = read_flo('%s/%s/%s/flow2.flo' % (dir_data, curr_class, curr_pair))
                data['images'].append([image1, image2])
                data['mask'].append([mask1, mask2])
                data['flow'].append([flow1, flow2])
                data['img_cl'].append(curr_class)
                data['name'].append(curr_pair)
                data['flip'].append(flip)
    data['flip'] = np.asarray(data['flip'])
    data['img_cl'] = np.asarray(data['img_cl'], dtype='<U64')
    data['name'] = np.asarray(data['name'], dtype='<U64')
    return data


def load_VOC2007(path):
    data = {'name': [], 'images': [], 'img_cl': [], 'bbox': [], 'nb_obj': []}
    cl_name = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle',
                  'bus', 'car', 'cat', 'chair', 'cow',
                  'diningtable', 'dog', 'horse', 'motorbike', 'person',
                  'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']

    for curr_cl in cl_name:
        trainval = np.genfromtxt('%s/ImageSets/Main/%s_trainval.txt' % (path, curr_cl), dtype=int)
        trainval = trainval[np.isin(trainval[:, 1], 1), 0]
        for curr_file in trainval:
            with open('%s/Annotations/%06d.xml' % (path, curr_file)) as f:
                xml = f.readlines()
            xml = ''.join([line.strip('\t') for line in xml])
            soup = BeautifulSoup(xml, 'html.parser').find_all('object')
            bbox = []
            for curr_obj in soup:
                if not curr_obj.find('name').string==curr_cl:
                    continue
                if (curr_obj.find('truncated').string=='1') or (curr_obj.find('difficult').string=='1'):
                    continue
                bbox.append([int(curr_obj.find('xmin').string), int(curr_obj.find('ymin').string),
                             int(curr_obj.find('xmax').string), int(curr_obj.find('ymax').string)])
            if len(bbox)>0:
                data['images'].append(imread('%s/JPEGImages/%06d.jpg' % (path, curr_file), mode='RGB'))
                data['img_cl'].append(curr_cl)
                data['nb_obj'].append(len(bbox))
                data['bbox'].append(np.array(bbox, dtype=int))
                data['name'].append(curr_file)
        print('Class %s loaded!' % curr_cl)

    data['img_cl'] = np.array(data['img_cl'], dtype='<U16')
    data['nb_obj'] = np.array(data['nb_obj'])
    data['name'] = np.array(data['name'])
    return data



def read_flo(flo_path):
    with open(flo_path, 'rb') as f:
        magic = np.fromfile(f, np.float32, count=1)
        if 202021.25 != magic:
            print('Magic number incorrect. Invalid .flo file')
        else:
            w = np.fromfile(f, np.int32, count=1)
            h = np.fromfile(f, np.int32, count=1)
            try:
                data = np.fromfile(f, np.float32, count=2 * w * h)
            except:
                data = np.fromfile(f, np.float32, count=2 * w[0] * h[0])
            data2D = np.resize(data, (h[0], w[0], 2))
    return data2D


def pts2mask(pts, size, radii):
    mask = np.zeros((size,size), dtype=int)
    for ii in range(-radii,radii+1):
        for jj in range(-radii,radii+1):
            x = np.maximum(np.minimum(pts[:,0] + ii, size-1), 0).astype(int)
            y = np.maximum(np.minimum(pts[:,1] + jj, size-1), 0).astype(int)
            mask[y, x] = 1
    return mask



def superpixel2mask(label, corr, radii, threshold = 5):
    h, w = label.shape
    label_idx = np.zeros(np.max(label) + 1)
    for ii in range(len(corr)):
        x, y = corr[ii].astype(int)
        overlap_idx = np.unique(label[max(y-radii, 0):min(y+radii+1, h-1), max(x-radii, 0):min(x+radii+1, h-1)])
        label_idx[overlap_idx] += 1
    label_idx = np.argwhere(label_idx>=threshold).flatten()
    mask = np.isin(label, label_idx) * 1.
    return mask

def load_TSS_weakalign(dir_data, dir_weakalign):
    data = {'images': [], 'flow': [], 'img_cl': [], 'name': [], 'mask':[], 'flip':[], 'flow_weakalign': []}
    for curr_class in os.listdir(dir_data):
        if curr_class.endswith('.txt'):
            continue
        else:
            for curr_pair in os.listdir('%s/%s' % (dir_data, curr_class)):
                image1 = imread('%s/%s/%s/image1.png' % (dir_data, curr_class, curr_pair), mode='RGB')
                image2 = imread('%s/%s/%s/image2.png' % (dir_data, curr_class, curr_pair), mode='RGB')
                flip = np.genfromtxt('%s/%s/%s/flip_gt.txt' % (dir_data, curr_class, curr_pair), dtype=int)
                mask1 = imread('%s/%s/%s/mask1.png' % (dir_data, curr_class, curr_pair), mode='L')
                mask2 = imread('%s/%s/%s/mask2.png' % (dir_data, curr_class, curr_pair), mode='L')
                flow1 = read_flo('%s/%s/%s/flow1.flo' % (dir_data, curr_class, curr_pair))
                flow2 = read_flo('%s/%s/%s/flow2.flo' % (dir_data, curr_class, curr_pair))
                flow1_weakalign = read_flo('%s/%s/%s/flow1.flo' % (dir_weakalign, curr_class, curr_pair))
                flow2_weakalign = read_flo('%s/%s/%s/flow2.flo' % (dir_weakalign, curr_class, curr_pair))
                data['images'].append([image1, image2])
                data['mask'].append([mask1, mask2])
                data['flow'].append([flow1, flow2])
                data['flow_weakalign'].append([flow1_weakalign, flow2_weakalign])
                data['img_cl'].append(curr_class)
                # if curr_class=='JODS':
                #     data['img_cl'].append(curr_pair.split('_')[0])
                # else:
                #     data['img_cl'].append(curr_class)
                data['name'].append(curr_pair)
                data['flip'].append(flip)
    data['flip'] = np.asarray(data['flip'])
    data['img_cl'] = np.asarray(data['img_cl'], dtype='<U64')
    data['name'] = np.asarray(data['name'], dtype='<U64')
    return data
