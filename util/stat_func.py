from __future__ import division
import numpy as np


def get_LT_ACC_IoU(mask1, mask2, vx, vy):
    # this function copy the label from mask2 to mask 1
    [h1, w1] = mask1.shape[:2]
    [h2, w2] = mask2.shape[:2]
    mask1_pred = np.zeros_like(mask1, dtype='uint8')

    [x1, y1] = np.meshgrid(range(w1), range(h1))
    x2 = x1 + vx.astype(int)
    y2 = y1 + vy.astype(int)
    x1, y1 = x1.flatten(), y1.flatten()
    x2, y2 = x2.flatten(), y2.flatten()
    in_bound = (x2 >= 0) & (y2 >= 0) & (x2 < w2) & (y2 < h2)

    mask1_pred[y1[in_bound], x1[in_bound]] = mask2[y2[in_bound], x2[in_bound]]

    LT_ACC = np.mean(mask1==mask1_pred)
    I = np.sum((mask1==1) & (mask1_pred==1))
    U = np.sum((mask1==1) | (mask1_pred==1))
    IoU = I / U

    return LT_ACC, IoU


def get_part_IoU(part_mask1, part_mask2, pairs, vx, vy):
    # this function copy the label from mask2 to mask 1 per part
    [h1, w1] = part_mask1.shape[1:3]
    [h2, w2] = part_mask2.shape[1:3]

    [x1, y1] = np.meshgrid(range(w1), range(h1))
    x2 = x1 + vx.astype(int)
    y2 = y1 + vy.astype(int)
    x1, y1 = x1.flatten(), y1.flatten()
    x2, y2 = x2.flatten(), y2.flatten()
    in_bound = (x2 >= 0) & (y2 >= 0) & (x2 < w2) & (y2 < h2)

    IoU = []
    Union = []
    for ii in range(len(pairs)):
        mask1 = part_mask1[pairs[ii][0]]
        mask2 = part_mask2[pairs[ii][1]]
        mask1_pred = np.zeros_like(mask1, dtype='uint8')
        mask1_pred[y1[in_bound], x1[in_bound]] = mask2[y2[in_bound], x2[in_bound]]
        I = np.sum((mask1==1) & (mask1_pred==1))
        U = np.sum((mask1==1) | (mask1_pred==1))
        # print I, U, I/U
        IoU.append(I / (U + 1e-16))
        Union.append(U)
    IoU = np.asarray(IoU)
    Union = np.asarray(Union)

    return np.sum(IoU * Union / np.sum(Union))


def localization_error(source_mask_np, target_mask_np, flow_np):
    h_tgt, w_tgt = target_mask_np.shape[0], target_mask_np.shape[1]
    h_src, w_src = source_mask_np.shape[0], source_mask_np.shape[1]

    # initial pixel positions x1,y1 in target image
    x1, y1 = np.meshgrid(range(1, w_tgt + 1), range(1, h_tgt + 1))
    # sampling pixel positions x2,y2
    x2 = x1 + flow_np[:, :, 0]
    y2 = y1 + flow_np[:, :, 1]

    # compute in-bound coords for each image
    in_bound = (x2 >= 1) & (x2 <= w_src) & (y2 >= 1) & (y2 <= h_src)
    row, col = np.where(in_bound)
    row_1 = y1[row, col].flatten().astype(np.int) - 1
    col_1 = x1[row, col].flatten().astype(np.int) - 1
    row_2 = y2[row, col].flatten().astype(np.int) - 1
    col_2 = x2[row, col].flatten().astype(np.int) - 1

    # compute relative positions
    target_loc_x, target_loc_y = obj_ptr(target_mask_np)
    source_loc_x, source_loc_y = obj_ptr(source_mask_np)
    x1_rel = target_loc_x[row_1, col_1]
    y1_rel = target_loc_y[row_1, col_1]
    x2_rel = source_loc_x[row_2, col_2]
    y2_rel = source_loc_y[row_2, col_2]

    # compute localization error
    loc_err = np.mean(np.abs(x1_rel - x2_rel) + np.abs(y1_rel - y2_rel))

    return loc_err


def obj_ptr(mask):
    # computes images of normalized coordinates around bounding box
    # kept function name from DSP code
    h, w = mask.shape[0], mask.shape[1]
    y, x = np.where(mask > 0.5)
    left = np.min(x);
    right = np.max(x);
    top = np.min(y);
    bottom = np.max(y);
    fg_width = right - left + 1;
    fg_height = bottom - top + 1;
    x_image, y_image = np.meshgrid(range(1, w + 1), range(1, h + 1));
    x_image = (x_image - left) / fg_width;
    y_image = (y_image - top) / fg_height;
    return (x_image, y_image)


def get_voc_IoU(h1, w1, bbox_gt, bbox_pred):
    correct_bbox = 0

    mask_pred = np.zeros([h1, w1], dtype='uint8')
    mask_pred[bbox_pred[1]:bbox_pred[3], bbox_pred[0]:bbox_pred[2]] = 1

    for ii in range(len(bbox_gt)):
        mask_gt = np.zeros([h1, w1], dtype='uint8')
        mask_gt[bbox_gt[ii,1]-1:bbox_gt[ii,3]-1, bbox_gt[ii,0]-1:bbox_gt[ii,2]-1] = 1
        I = np.sum((mask_gt==1) & (mask_pred==1))
        U = np.sum((mask_gt==1) | (mask_pred==1))
        # print I, U, I/U
        if I / (U + 1e-16)>=0.5:
            correct_bbox += 1
    if correct_bbox > 0:
        return 1.
    else:
        return 0.
