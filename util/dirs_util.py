import os

def create_dir(directory, silent=True):
    """Check if dir exist if not create, return the directory name """
    if not os.path.exists(directory):
        os.makedirs(directory)
    elif not silent:
        print('{} already exists ...'.format(directory))
    return directory

def check_dir(directory):
    """Check if dir exist, return the directory name"""
    assert(os.path.exists(directory)), 'Path %s is not available' % (directory)
    return directory
