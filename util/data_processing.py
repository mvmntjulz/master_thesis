from __future__ import division
import numpy as np
from scipy.misc import imresize, imread
import os
import scipy.io as sio
import gzip
from scipy.interpolate import interp2d
from PIL import Image, ImageDraw


def read_txt(txt_file, start = 0, length = 0, dtype = 'float'):
    ## read the txt file and split the data

    with open(txt_file) as f:
        lines = f.readlines()
        if length==0:
            end = len(lines[0].split())
        else:
            end = start + length
        for ii in range(len(lines)):
            lines[ii] = lines[ii].split()[start:end]

        if dtype=='str':
            pass
        elif (dtype=='float') | (dtype=='float32'):
            lines = np.asarray(lines, dtype=dtype)
        elif (dtype=='int') | (dtype=='int32'):
            lines = np.asarray(lines, dtype='float')
            lines = lines.astype(dtype=dtype)

    return lines


def preprocess(im, bbox, pad = 32, min_dim = 224):
    ## crop and resize the image

    if len(im.shape)==3:
        out_im = np.pad(im, [(pad, pad), (pad, pad), (0, 0)], mode='edge')
    elif len(im.shape)==2:
        out_im = np.pad(im, [(pad, pad), (pad, pad)], mode='edge')
    out_im = out_im[bbox[1]:bbox[1]+bbox[3]+pad*2, bbox[0]:bbox[0]+bbox[2]+pad*2]
    [h, w] = out_im.shape[:2]
    if h > w:
        out_im = imresize(out_im, [int(min_dim * h / w), min_dim], 'bilinear')
    elif h < w:
        out_im = imresize(out_im, [min_dim, int(min_dim * w / h)], 'bilinear')
    elif h==w:
        out_im = imresize(out_im, [min_dim, min_dim], 'bilinear')

    return out_im


def preprocess_sq(im, bbox, pad = 32, min_dim = 224):
    ## crop and resize to a square image

    if len(im.shape)==3:
        out_im = np.pad(im, [(pad, pad), (pad, pad), (0, 0)], mode='edge')
    elif len(im.shape)==2:
        out_im = np.pad(im, [(pad, pad), (pad, pad)], mode='edge')
    out_im = out_im[bbox[1]:bbox[1]+bbox[3]+pad*2, bbox[0]:bbox[0]+bbox[2]+pad*2]
    out_im = imresize(out_im, [min_dim, min_dim], 'bilinear')

    return out_im


def load_data(dir, files, train_id, load_mask=False, resize_sq=False, debug_flag=False):

    if debug_flag:
        nb_im = 500
    else:
        nb_im = len(files)

    data = {}
    data['bbox'] = read_txt(dir + '/bounding_boxes.txt', 1, 4, 'int')  # <image_id> <x> <y> <width> <height>
    data['imgid'] = read_txt(dir + '/bounding_boxes.txt', 0, 1, 'int').reshape(-1)
    if debug_flag:
        data['bbox'] = data['bbox'][data['imgid'] <= nb_im, :]
    else:
        assert data['bbox'].shape[0] == nb_im

    data['images'] = []
    if load_mask:
        data['masks'] = []
    data['dim'] = np.ones([nb_im, 2], dtype='int')
    data['dim_old'] = np.ones([nb_im, 2], dtype='int')

    for ii in range(nb_im):
        if train_id[ii] == 1:  # reduce memory usage
            data['images'].append([])
            if load_mask:
                data['masks'].append([])
        else:
            tmp_image = imread(dir + '/images/' + files[ii][0], mode='RGB')
            data['dim_old'][ii] = tmp_image.shape[:2]
            if resize_sq:
                tmp_image = preprocess_sq(tmp_image, data['bbox'][ii], pad=32, min_dim=224)
            else:
                tmp_image = preprocess(tmp_image, data['bbox'][ii], pad=32, min_dim=224)
            if load_mask:
                tmp_mask = imread(dir + '/segmentations/' + files[ii][0].replace('jpg', 'png'), mode='L')    # black & white
                if resize_sq:
                    tmp_mask = preprocess_sq(tmp_mask, data['bbox'][ii], pad=32, min_dim=224)
                else:
                    tmp_mask = preprocess(tmp_mask, data['bbox'][ii], pad=32, min_dim=224)
                assert tmp_image.shape[:2]==tmp_mask.shape[:2]
                data['masks'].append(tmp_mask)
            data['images'].append(tmp_image)
            data['dim'][ii] = tmp_image.shape[:2]
    print(str(nb_im) + ' images loaded!')

    # annotations
    parts = read_txt(dir + '/parts/part_locs.txt', 0, 5, 'int')  # <image_id> <part_id> <x> <y> <visible>
    if debug_flag:
        parts = parts[parts[:, 0] <= nb_im, :]
    else:
        assert np.max(parts[:, 0]) == nb_im
    nb_anno = np.max(parts[:, 1])
    data['anno_x'] = np.zeros([nb_im, nb_anno], dtype='int')  # <x>
    data['anno_y'] = np.zeros([nb_im, nb_anno], dtype='int')  # <y>
    data['anno_vis'] = np.zeros([nb_im, nb_anno], dtype='int')
    parts[:, 0] = parts[:, 0] - 1
    parts[:, 1] = parts[:, 1] - 1
    data['anno_x'][parts[:, 0], parts[:, 1]] = parts[:, 2]
    data['anno_y'][parts[:, 0], parts[:, 1]] = parts[:, 3]
    data['anno_vis'][parts[:, 0], parts[:, 1]] = parts[:, 4]

    # adjust annotations
    data['anno_x'] = data['anno_x'] - data['bbox'][:, [0]] + 32
    data['anno_y'] = data['anno_y'] - data['bbox'][:, [1]] + 32
    data['anno_x'] = data['anno_x'] * (data['dim'][:, [1]] - 1) // (data['bbox'][:, [2]] + 64 - 1) * data['anno_vis']
    data['anno_y'] = data['anno_y'] * (data['dim'][:, [0]] - 1) // (data['bbox'][:, [3]] + 64 - 1) * data['anno_vis']

    return data


def load_vx_vy(file, nb_bytes = -1, dtype = 'float'):
    fid = gzip.open(file, 'rb')
    if nb_bytes==-1:
        lines = fid.readlines()
    else:
        lines = fid.readlines(nb_bytes)
    data = []
    tmp_matrix = []
    for ii in range(len(lines)):
        tmp = lines[ii].split()
        if tmp[0] == '#':
            if int(tmp[2]) > 0:
                data.append(np.asarray(tmp_matrix, dtype=dtype))
            tmp_matrix = []
        else:
            tmp_matrix.append(tmp)
    data.append(np.asarray(tmp_matrix, dtype=dtype))
    fid.close()

    return data


def warp_image(im, vx, vy):
    # function to warp images with different dimensions

    [height2, width2, nchannels] = im.shape
    [height1, width1] = vx.shape

    # [xx, yy] = np.meshgrid(range(width2), range(height2))
    [XX, YY] = np.meshgrid(range(width1), range(height1))
    XX = XX + vx
    YY = YY + vy
    mask = (XX < 0) | (XX > width2 - 1) | (YY < 0) | (YY > height2 - 1)
    XX = np.minimum(np.maximum(XX, 0), width2 - 1)
    YY = np.minimum(np.maximum(YY, 0), height2 - 1)

    warpI2 = np.zeros([height1, width1, nchannels])
    for i in range(nchannels):
        f = interp2d(range(width2), range(height2), im[:, :, i], kind='cubic')
        foo = np.zeros([height1, width1])
        for j in range(height1):
            for k in range(width1):
                foo[j, k] = f(XX[j, k], YY[j, k])
        foo[mask] = 0.6
        warpI2[:, :, i] = foo

    return warpI2


#def load_willow(dir_data, debug=False):
#    data = {'images': [], 'part_x': [], 'part_y': [], 'dim': [], 'img_cl': []}
#    for curr_folder in sorted(os.listdir(dir_data)):
#        count = 0
#        for curr_file in sorted(os.listdir('%s/%s' % (dir_data, curr_folder))):
#            if curr_file.endswith('.png'):
#                print(curr_folder, curr_file)
#                data['img_cl'].append(curr_folder)
#                image = imread('%s/%s/%s' % (dir_data, curr_folder, curr_file), mode='RGB')
#                data['dim'].append(image.shape[:2])
#                pts_coord = sio.loadmat('%s/%s/%s' % (dir_data, curr_folder, curr_file.replace('.png', '.mat')))['pts_coord']
#                data['images'].append(image)
#                data['part_x'].append(pts_coord[0, :])
#                data['part_y'].append(pts_coord[1, :])
#    data['part_x'] = np.asarray(data['part_x'])
#    data['part_y'] = np.asarray(data['part_y'])
#    data['dim'] = np.asarray(data['dim'])
#    data['img_cl'] = np.asarray(data['img_cl'], dtype='<U16')
#    data['bbox'] = np.concatenate([data['part_x'].min(axis=1, keepdims=True),
#                                   data['part_y'].min(axis=1, keepdims=True),
#                                   data['part_x'].max(axis=1, keepdims=True),
#                                   data['part_y'].max(axis=1, keepdims=True)], axis=1)
#    return data


def load_willow(dir_data, debug=False):
    data = {'images':[], 'part_x': [], 'part_y': [], 'dim': [], 'img_cl': []}
    for curr_folder in sorted(os.listdir(dir_data)):
        for curr_file in sorted(os.listdir('%s/%s' % (dir_data, curr_folder))):
            if curr_file.endswith('.png'):
                print(curr_folder, curr_file)
                data['img_cl'].append(curr_folder)
                image = imread('%s/%s/%s' % (dir_data, curr_folder, curr_file), mode='RGB')
                data['dim'].append(image.shape[:2])
                pts_coord = sio.loadmat('%s/%s/%s' % (dir_data, curr_folder, curr_file.replace('.png', '.mat')))['pts_coord']
                data['images'].append(image)
                data['part_x'].append(pts_coord[0, :])
                data['part_y'].append(pts_coord[1, :])
    data['part_x'] = np.asarray(data['part_x'])
    data['part_y'] = np.asarray(data['part_y'])
    data['dim'] = np.asarray(data['dim'])
    data['img_cl'] = np.asarray(data['img_cl'], dtype='<U16')
    data['bbox'] = np.concatenate([data['part_x'].min(axis=1, keepdims=True),
                                   data['part_y'].min(axis=1, keepdims=True),
                                   data['part_x'].max(axis=1, keepdims=True),
                                   data['part_y'].max(axis=1, keepdims=True)], axis=1)
    return data




def load_pascal_parts(dir_data):
    data = {'images':[], 'part_x': [], 'part_y': [], 'keypts_status': [], 'dim': [], 'img_cl': [], 'name': []}
    for curr_folder in os.listdir(dir_data):
        for curr_file in os.listdir('%s/%s' % (dir_data, curr_folder)):
            if curr_file.endswith('.jpg'):
                image = imread('%s/%s/%s' % (dir_data, curr_folder, curr_file), mode='RGB')
                keypts = sio.loadmat('%s/%s/%s' % (dir_data, curr_folder, curr_file.replace('.jpg', '.mat')))
                if len(keypts['keypts_status'])==0:
                    continue
                data['dim'].append(image.shape[:2])
                data['img_cl'].append(curr_folder)
                data['name'].append('%s/%s' % (curr_folder, curr_file))
                data['images'].append(image)
                data['part_x'].append(keypts['keypts'][0, :])
                data['part_y'].append(keypts['keypts'][1, :])
                data['keypts_status'].append(keypts['keypts_status'].squeeze())
    # data['part_x'] = np.asarray(data['part_x'])
    # data['part_y'] = np.asarray(data['part_y'])
    # data['keypts_status'] = np.asarray(data['keypts_status'])
    data['dim'] = np.asarray(data['dim'])
    data['img_cl'] = np.asarray(data['img_cl'], dtype='<U32')
    data['name'] = np.asarray(data['name'], dtype='<U64')
    return data


def contour2mask(contour, height, width):
    # contour = [(x1,y1),(x2,y2),...] or [x1,y1,x2,y2,...]
    # contour must be a list, not ndarray
    img = Image.new('1', (width, height), color=0)
    ImageDraw.Draw(img).polygon(contour, outline=1, fill=1)
    return np.array(img, dtype='uint8')


def load_TSS(dir_data):
    data = {'images': [], 'flow': [], 'img_cl': [], 'name': [], 'mask':[], 'flip':[]}
    for curr_class in os.listdir(dir_data):
        if curr_class.endswith('.txt'):
            continue
        else:
            for curr_pair in os.listdir('%s/%s' % (dir_data, curr_class)):
                image1 = imread('%s/%s/%s/image1.png' % (dir_data, curr_class, curr_pair), mode='RGB')
                image2 = imread('%s/%s/%s/image2.png' % (dir_data, curr_class, curr_pair), mode='RGB')
                flip = np.genfromtxt('%s/%s/%s/flip_gt.txt' % (dir_data, curr_class, curr_pair), dtype=int)
                mask1 = imread('%s/%s/%s/mask1.png' % (dir_data, curr_class, curr_pair), mode='L')
                mask2 = imread('%s/%s/%s/mask2.png' % (dir_data, curr_class, curr_pair), mode='L')
                flow1 = read_flo('%s/%s/%s/flow1.flo' % (dir_data, curr_class, curr_pair))
                flow2 = read_flo('%s/%s/%s/flow2.flo' % (dir_data, curr_class, curr_pair))
                data['images'].append([image1, image2])
                data['mask'].append([mask1, mask2])
                data['flow'].append([flow1, flow2])
                data['img_cl'].append(curr_class)
                data['name'].append(curr_pair)
                data['flip'].append(flip)
    data['flip'] = np.asarray(data['flip'])
    data['img_cl'] = np.asarray(data['img_cl'], dtype='<U64')
    data['name'] = np.asarray(data['name'], dtype='<U64')
    return data


def read_flo(flo_path):
    with open(flo_path, 'rb') as f:
        magic = np.fromfile(f, np.float32, count=1)
        if 202021.25 != magic:
            print('Magic number incorrect. Invalid .flo file')
        else:
            w = np.fromfile(f, np.int32, count=1)
            h = np.fromfile(f, np.int32, count=1)
            data = np.fromfile(f, np.float32, count=2 * w * h)
            data2D = np.resize(data, (h[0], w[0], 2))
    return data2D


def pts2mask(pts, size, radii):
    mask = np.zeros((size,size), dtype=int)
    for ii in range(-radii,radii+1):
        for jj in range(-radii,radii+1):
            x = np.maximum(np.minimum(pts[:,0] + ii, size-1), 0).astype(int)
            y = np.maximum(np.minimum(pts[:,1] + jj, size-1), 0).astype(int)
            mask[y, x] = 1
    return mask
