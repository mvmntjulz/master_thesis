import numpy as np



def random_photometric(img, *, noise_stddev=0.0, min_contrast=0.0, max_contrast=0.0,
					   brightness_stddev=0.0, min_colour=1.0, max_colour=1.0, min_gamma=1.0, max_gamma=1.0):
	"""Applies photometric augmentations to a list of image batches.
	Each image in the list is augmented in the same way.
	For all elements, num_batch must be equal while height and width may differ.
	Args:
		ims: list of 3-channel image batches normalized to [0, 1].
		channel_mean: tensor of shape [3] which was used to normalize the pixel
			values ranging from 0 ... 255.
	Returns:
		Batch of normalized images with photometric augmentations. Has the same
		shape as the input batch.
	"""

	#num_batch = tf.shape(ims[0])[0]
	contrast = np.random.uniform(min_contrast, max_contrast)
	gamma = np.random.uniform(min_gamma, max_gamma)
	gamma_inv = 1.0 / gamma
	colour = np.random.uniform(min_colour, max_colour)

	if noise_stddev > 0.0:
		noise = np.random.normal(0,noise_stddev)
	else:
		noise = 0.0
	if brightness_stddev > 0.0:
		brightness = np.random.normal(0,brightness_stddev)
	else:
		brightness = 0#tf.zeros([num_batch, 1])

	img = (img * (contrast + 1.0) + brightness) * colour
	img = np.maximum(0.0, np.minimum(1.0, img))
	img = np.power(img, gamma_inv)

	img = img + noise

	return img

